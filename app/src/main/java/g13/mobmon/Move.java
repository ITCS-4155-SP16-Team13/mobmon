package g13.mobmon;

import android.util.Log;
import java.util.ArrayList;
import java.util.Random;

public class Move {
    private int critical = 0, hit, times, damage, type, power, maxpp, pp, accuracy;
    private float modifier;
    private String category, name, description;
    private Monster owner, enemy;
    private ArrayList<String> updateText = new ArrayList();

    Move(String move, Monster owner) {
        name = move;
        this.owner = owner;
        //the reason types are ints is because I use them as an index in a 2d array to determine damage modifiers
        //types: 0:normal, 1:fire, 2:water, 3:electric, 4:grass, 5:ice, 6:fighting, 7:poison,
        //8:ground, 9:flying, 10:psychic, 11:bug, 12:rock, 13:ghost, 14:dragon, 15:dark, 16:steel
        //-1:N/A
        switch (move) {
            case "Absorb" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 20;
                category = "Special";
                type = 4;
                description = "User recovers half the HP inflicted on opponent";
                break;
            case "Acid" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Special";
                type = 7;
                description = "May lower opponent's Special Defense";
                break;
            case "Acid Armor" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 76;
                description = "Sharply raises user's Defense";
                break;
            case "Agility" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "Sharply raises user's Speed";
                break;
            case "Amnesia" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "Sharply raises user's Special Defense";
                break;
            case "Aurora Beam" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 65;
                category = "Special";
                type = 5;
                description = "May lower opponent's Attack";
                break;
            case "Barrage" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 85;
                power = 15;
                category = "Physical";
                type = 0;
                description = "Hits 2-5 times in one turn";
                break;
            case "Barrier" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "Sharply raises user's Defense";
                break;
            case "Bide" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Physical";
                type = 0;
                description = "User takes damage for two or three turns then strikes back double";
                break;
            case "Bind" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 85;
                power = 15;
                category = "Physical";
                type = 0;
                description = "Traps opponent, damaging them for 4-5 turns";
                break;
            case "Bite" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 60;
                category = "Physical";
                type = 15;
                description = "May cause flinching";
                break;
            case "Blizzard" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 70;
                power = 110;
                category = "Special";
                type = 5;
                description = "May freeze opponent";
                break;
            case "Body Slam" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 85;
                category = "Physical";
                type = 0;
                description = "May paralyze opponent";
                break;
            case "Bone Club" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 85;
                power = 65;
                category = "Physical";
                type = 8;
                description = "May cause flinching";
                break;
            case "Bonemerang" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 90;
                power = 50;
                category = "Physical";
                type = 8;
                description = "Hits twice in one turn";
                break;
            case "Bubble" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Special";
                type = 2;
                description = "May lower opponent's Speed";
                break;
            case "Bubble Beam" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 65;
                category = "Special";
                type = 2;
                description = "May lower opponent's Speed";
                break;
            case "Clamp" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 85;
                power = 35;
                category = "Physical";
                type = 2;
                description = "Traps opponent, damaging them for 4-5 turns";
                break;
            case "Comet Punch" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 85;
                power = 18;
                category = "Physical";
                type = 0;
                description = "Hits 2-5 times in one turn";
                break;
            case "Confuse Ray" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 13;
                description = "Confuses opponent";
                break;
            case "Confusion" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 50;
                category = "Special";
                type = 10;
                description = "May confuse opponent";
                break;
            case "Constrict" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 10;
                category = "Physical";
                type = 0;
                description = "May lower opponent's Speed by one stage";
                break;
            case "Conversion" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Changes user's type to that of its first move";
                break;
            case "Counter" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Physical";
                type = 6;
                description = "When hit by a Physical Attack, user strikes back with 2x power";
                break;
            case "Crabhammer" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 90;
                power = 100;
                category = "Physical";
                type = 2;
                description = "High critical hit ratio";
                critical += 1;
                break;
            case "Cut" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 95;
                power = 50;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Defense Curl" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Raises user's Defense";
                break;
            case "Dig" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 80;
                category = "Physical";
                type = 8;
                description = "Digs underground on first turn, attacks on second";
                break;
            case "Disable" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 0;
                description = "Opponent can't use its last attack for a few turns";
                break;
            case "Dizzy Punch" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 70;
                category = "Physical";
                type = 0;
                description = "May confuse opponent";
                break;
            case "Double Kick" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 30;
                category = "Physical";
                type = 6;
                description = "Hits twice in one turn";
                break;
            case "Double Slap" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 85;
                power = 15;
                category = "Physical";
                type = 0;
                description = "Hits 2-5 times in one turn";
                break;
            case "Double Team" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Raises user's Evaisiveness";
                break;
            case "Double Edge" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 120;
                category = "Special";
                type = 0;
                description = "User receives recoil damage";
                break;
            case "Dragon Rage" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Special";
                type = 14;
                description = "Always inflicts 40 HP";
                break;
            case "Dream Eater" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 100;
                category = "Special";
                type = 10;
                description = "User recovers half the HP inflicted on a sleeping opponent";
                break;
            case "Drill Peck" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 80;
                category = "Physical";
                type = 9;
                description = "";
                break;
            case "Earthquake" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 100;
                category = "Physical";
                type = 8;
                description = "Power is doubled if opponent is underground from using Dig";
                break;
            case "Egg Bomb" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 75;
                power = 100;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Ember" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Special";
                type = 1;
                description = "May burn opponent";
                break;
            case "Explosion" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 100;
                power = 500;
                category = "Physical";
                type = 0;
                description = "User faints";
                break;
            case "Fire Blast" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 85;
                power = 110;
                category = "Special";
                type = 1;
                description = "May burn opponent";
                break;
            case "Fire Punch" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 75;
                category = "Physical";
                type = 1;
                description = "May burn opponent";
                break;
            case "Fire Spin" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 85;
                power = 35;
                category = "Special";
                type = 1;
                description = "Traps opponent, damaging them for 4-5 turns";
                break;
            case "Fissure" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Physical";
                type = 8;
                description = "One-Hit-KO, if it hits";
                break;
            case "Flamethrower" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 90;
                category = "Special";
                type = 1;
                description = "May burn opponent";
                break;
            case "Flash" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 0;
                description = "Lowers opponent's Accuracy";
                break;
            case "Fly" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 95;
                power = 90;
                category = "Physical";
                type = 9;
                description = "Flies up on first turn, attacks on second turn";
                break;
            case "Focus Energy" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Increases critical hit ratio";
                break;
            case "Fury Attack" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 85;
                power = 15;
                category = "Physical";
                type = 0;
                description = "Hits 2-5 times in one turn";
                break;
            case "Fury Swipes" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 80;
                power = 18;
                category = "Physical";
                type = 0;
                description = "Hits 2-5 times in one turn";
                break;
            case "Glare" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 0;
                description = "Paralyzes opponent";
                break;
            case "Growl" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 0;
                description = "Lowers opponent's Attack";
                break;
            case "Growth" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Raises user's Attack and Special Attack";
                break;
            case "Guillotine" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Physical";
                type = 0;
                description = "One-Hit-KO, if it hits";
                break;
            case "Gust" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Special";
                type = 9;
                description = "Hits Monster using Fly with double power";
                break;
            case "Harden" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Raises user's Defense";
                break;
            case "Haze" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 5;
                description = "Resets all state changes";
                break;
            case "Headbutt" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 70;
                category = "Physical";
                type = 0;
                description = "May cause flinching";
                break;
            case "High Jump Kick" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 90;
                power = 130;
                category = "Physical";
                type = 6;
                description = "If it misses, the user loses half their HP";
                break;
            case "Horn Attack" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 65;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Horn Drill" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Physical";
                type = 0;
                description = "One-Hit-KO, if it hits";
                break;
            case "Hydro Pump" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 80;
                power = 110;
                category = "Special";
                type = 2;
                description = "";
                break;
            case "Hyper Beam" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 90;
                power = 150;
                category = "Special";
                type = 0;
                description = "User must recharge next turn";
                break;
            case "Hyper Fang" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 90;
                power = 80;
                category = "Physical";
                type = 0;
                description = "May cause flinching";
                break;
            case "Hypnosis" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 60;
                power = 0;
                category = "Status";
                type = 10;
                description = "Puts opponent to sleep";
                break;
            case "Ice Beam" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 90;
                category = "Special";
                type = 5;
                description = "May freeze opponent";
                break;
            case "Ice Punch" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 75;
                category = "Physical";
                type = 5;
                description = "May freeze opponent";
                break;
            case "Jump Kick" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 95;
                power = 100;
                category = "Physical";
                type = 6;
                description = "If it misses, the user loses half their HP";
                break;
            case "Karate Chop" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 50;
                category = "Physical";
                type = 6;
                description = "High critical hit ratio";
                critical += 1;
                break;
            case "Kinesis" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 80;
                power = 0;
                category = "Status";
                type = 10;
                description = "Lowers opponent's Accuracy";
                break;
            case "Leech Life" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 20;
                category = "Physical";
                type = 11;
                description = "User recovers half the HP inflicted on opponent";
                break;
            case "Leech Seed" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 90;
                power = 0;
                category = "Status";
                type = 4;
                description = "User steals HP from opponent each turn";
                break;
            case "Leer" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 0;
                description = "Lowers opponent's Defense";
                break;
            case "Lick" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 30;
                category = "Physical";
                type = 13;
                description = "May paralyze opponent";
                break;
            case "Light Screen" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "Halves damage from Special attacks for 5 turns";
                break;
            case "Lovely Kiss" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 75;
                power = 0;
                category = "Status";
                type = 0;
                description = "Puts opponent to sleep";
                break;
            case "Low Kick" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Physical";
                type = 6;
                description = "The heavier the opponent, the stronger the attack";
                break;
            case "Meditate" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "Raises user's Attack";
                break;
            case "Mega Drain" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Special";
                type = 4;
                description = "User recovers half the HP inflicted on opponent";
                break;
            case "Mega Kick" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 75;
                power = 120;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Mega Punch" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 85;
                power = 80;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Metronome" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "User performs any move in the game at random";
                break;
            case "Mimic" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Copies the opponent's last move";
                break;
            case "Minimize" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Sharply raises user's Evasiveness";
                break;
            case "Mirror Move" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 9;
                description = "User performs the opponent's last move";
                break;
            case "Mist" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 5;
                description = "User's stats cannot be changed for a period of time";
                break;
            case "Night Shade" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Special";
                type = 13;
                description = "Inflicts damage equal to user's level";
                break;
            case "Pay Day" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Physical";
                type = 0;
                description = "A small amount of money is gained after the battle resolves";
                break;
            case "Peck" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 35;
                category = "Physical";
                type = 9;
                description = "";
                break;
            case "Petal Dance" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 120;
                category = "Special";
                type = 4;
                description = "User attacks for 2-3 turns but then becomes confused";
                break;
            case "Pin Missile" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 95;
                power = 25;
                category = "Physical";
                type = 11;
                description = "Hits 2-5 times in one turn";
                break;
            case "Poison Gas" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 90;
                power = 0;
                category = "Status";
                type = 7;
                description = "Poisons opponent";
                break;
            case "Poison Powder" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 75;
                power = 0;
                category = "Status";
                type = 7;
                description = "Poisons opponent";
                break;
            case "Poison Sting" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 15;
                category = "Physical";
                type = 7;
                description = "May poison the opponent";
                break;
            case "Pound" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Psybeam" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 65;
                category = "Special";
                type = 10;
                description = "May confuse opponent";
                break;
            case "Psychic" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 90;
                category = "Special";
                type = 10;
                description = "May lower opponent's Special Defense";
                break;
            case "Psywave" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 80;
                power = 0;
                category = "Special";
                type = 10;
                description = "Inflicts damage 50-150% of the user's level";
                break;
            case "Quick Attack" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Physical";
                type = 0;
                description = "User attacks first";
                break;
            case "Rage" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 20;
                category = "Physical";
                type = 0;
                description = "Raises user's Attack when hit";
                break;
            case "Razor Leaf" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 95;
                power = 55;
                category = "Physical";
                type = 4;
                description = "High critical hit ratio";
                critical += 1;
                break;
            case "Razor Wind" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 80;
                category = "Special";
                type = 0;
                description = "Charges on first turn, attacks on second. High critical hit ratio";
                break;
            case "Recover" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "User recovers half its max HP";
                break;
            case "Reflect" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "Halves damage from Physical attacks for 5 turns";
                break;
            case "Rest" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "User sleeps for 2 turns, but user is fully healed";
                break;
            case "Roar" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "In battles the opponent switches, in the wild, the monster runs";
                break;
            case "Rock Slide" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 90;
                power = 75;
                category = "Physical";
                type = 12;
                description = "May cause flinching";
                break;
            case "Rock Throw" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 90;
                power = 50;
                category = "Physical";
                type = 12;
                description = "";
                break;
            case "Rolling Kick" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 85;
                power = 60;
                category = "Physical";
                type = 6;
                description = "May cause flinching";
                break;
            case "Sand Attack" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 8;
                description = "Lowers opponent's Accuracy";
                break;
            case "Scratch" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Screech" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 85;
                power = 0;
                category = "Status";
                type = 0;
                description = "Sharply lowers opponent's Defense";
                break;
            case "Seismic Toss" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Physical";
                type = 6;
                description = "Inflicts damage equal to the user's level";
                break;
            case "Self-Destruct" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 100;
                power = 400;
                category = "Physical";
                type = 0;
                description = "User faints";
                break;
            case "Sharpen" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Raises user's Attack";
                break;
            case "Sing" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 55;
                power = 0;
                category = "Status";
                type = 0;
                description = "Puts opponent to sleep";
                break;
            case "Skull Bash" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 130;
                category = "Physical";
                type = 0;
                description = "Raises Defense on first turn, attacks on second";
                break;
            case "Sky Attack" :
                maxpp = 5;
                pp = maxpp;
                accuracy = 90;
                power = 140;
                category = "Physical";
                type = 9;
                description = "Charges on the first turn, attacks on second. May cause flinching";
                break;
            case "Slam" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 75;
                power = 80;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Slash" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 70;
                category = "Physical";
                type = 0;
                description = "High critical hit ratio";
                critical += 1;
                break;
            case "Sleep Powder" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 75;
                power = 0;
                category = "Status";
                type = 4;
                description = "Puts opponent to sleep";
                break;
            case "Sludge" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 65;
                category = "Special";
                type = 7;
                description = "May poison opponent";
                break;
            case "Smog" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 70;
                power = 30;
                category = "Special";
                type = 7;
                description = "May poison opponent";
                break;
            case "Smokescreen" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 0;
                description = "Lowers opponent's Accruacy";
                break;
            case "Soft Boiled" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "User recovers half its max HP";
                break;
            case "Solar Beam" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 120;
                category = "Special";
                type = 4;
                description = "Charges on first turn, attacks on second";
                break;
            case "Sonic Boom" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 90;
                power = 0;
                category = "Special";
                type = 0;
                description = "Always inflicts 20 HP";
                break;
            case "Spike Cannon" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 20;
                category = "Physical";
                type = 0;
                description = "Hits 2-5 times in one turn";
                break;
            case "Splash" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Doesn't do ANYTHING";
                break;
            case "Spore" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 4;
                description = "Puts opponent to sleep";
                break;
            case "Stomp" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 65;
                category = "Physical";
                type = 0;
                description = "May cause flinching";
                break;
            case "Strength" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 80;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "String Shot" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 95;
                power = 0;
                category = "Status";
                type = 11;
                description = "Sharply lowers opponent's Speed";
                break;
            case "Struggle" :
                maxpp = 0;
                pp = maxpp;
                accuracy = 100;
                power = 50;
                category = "Physical";
                type = 0;
                description = "Only usable when all PP are gone. Hurts the user";
                break;
            case "Stun Spore" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 75;
                power = 0;
                category = "Status";
                type = 4;
                description = "Paralyzes opponent";
                break;
            case "Submission" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 80;
                power = 80;
                category = "Physical";
                type = 6;
                description = "User receives recoil damage";
                break;
            case "Substitute" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Uses HP to create a decoy that takes hits";
                break;
            case "Super Fang" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 90;
                power = 0;
                category = "Physical";
                type = 0;
                description = "Always takes off half of the opponent's HP";
                break;
            case "Supersonic" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 55;
                power = 0;
                category = "Status";
                type = 0;
                description = "Confuses opponent";
                break;
            case "Surf" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 90;
                category = "Special";
                type = 2;
                description = "Hits all adjacent monsters";
                break;
            case "Swift" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 60;
                category = "Special";
                type = 0;
                description = "Ignores Accuracy and Evasiveness";
                break;
            case "Swords Dance" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "Sharply raises user's Attack";
                break;
            case "Tackle" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 50;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Tail Whip" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 0;
                description = "Lowers opponent's Defense";
                break;
            case "Take Down" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 85;
                power = 90;
                category = "Physical";
                type = 0;
                description = "User receives recoil damage";
                break;
            case "Teleport" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 10;
                description = "Allows user to flee wild battles";
                break;
            case "Thrash" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 120;
                category = "Physical";
                type = 0;
                description = "User attacks for 2-3 turns but then becomes confused";
                break;
            case "Thunder" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 70;
                power = 110;
                category = "Special";
                type = 3;
                description = "May paralyze opponent";
                break;
            case "Thunder Punch" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 75;
                category = "Physical";
                type = 3;
                description = "May paralyze opponent";
                break;
            case "Thunder Shock" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Special";
                type = 3;
                description = "May paralyze opponent";
                break;
            case "Thunder Wave" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 0;
                category = "Status";
                type = 3;
                description = "Paralyzes opponent";
                break;
            case "Thunderbolt" :
                maxpp = 15;
                pp = maxpp;
                accuracy = 100;
                power = 90;
                category = "Special";
                type = 3;
                description = "May paralyze opponent";
                break;
            case "Toxic" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 90;
                power = 0;
                category = "Status";
                type = 7;
                description = "Badly poisons opponent";
                break;
            case "Transform" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "User takes on the form and attacks of the opponent";
                break;
            case "Tri Attack" :
                maxpp = 10;
                pp = maxpp;
                accuracy = 100;
                power = 80;
                category = "Special";
                type = 0;
                description = "May paralyze, burn or freeze opponent";
                break;
            case "Twineedle" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 100;
                power = 25;
                category = "Physical";
                type = 11;
                description = "Hits twice in one turn. May poison opponent";
                break;
            case "Vice Grip" :
                maxpp = 30;
                pp = maxpp;
                accuracy = 100;
                power = 55;
                category = "Physical";
                type = 0;
                description = "";
                break;
            case "Vine Whip" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 45;
                category = "Physical";
                type = 4;
                description = "";
                break;
            case "Water Gun" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 40;
                category = "Special";
                type = 2;
                description = "";
                break;
            case "Waterfall" :
                maxpp = 25;
                pp = maxpp;
                accuracy = 100;
                power = 80;
                category = "Physical";
                type = 2;
                description = "May cause flinching";
                break;
            case "Whirlwind" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 0;
                description = "In battles, the opponent switches. In the wild, the monster runs";
                break;
            case "Wing Attack" :
                maxpp = 35;
                pp = maxpp;
                accuracy = 100;
                power = 60;
                category = "Physical";
                type = 9;
                description = "";
                break;
            case "Withdraw" :
                maxpp = 40;
                pp = maxpp;
                accuracy = 0;
                power = 0;
                category = "Status";
                type = 2;
                description = "Raises user's Defense";
                break;
            case "Wrap" :
                maxpp = 20;
                pp = maxpp;
                accuracy = 90;
                power = 15;
                category = "Physical";
                type = 0;
                description = "Traps opponent, damaging them for 4-5 turns";
                break;
            default :
                break;
        }
    }

    public ArrayList<String> performMove(Monster enemy) {
        this.enemy = enemy;
        updateText = new ArrayList();
        updateText.add(owner.getName() + " used " + name);
        Random rand = new Random();
        boolean confusedSkip = false;

        if (owner.getConfused()) {
            int randomNum = rand.nextInt(2);
            if (randomNum < 1) {
                confusedSkip = true;
            }
        }

        if (confusedSkip) {
            updateText.add("In it's confusion, it hurt istelf instead!");
            damage = (int) (((float)((float)owner.getAttack()/(float)owner.getDefense())*40)*modifier/10);;

            owner.setHp(owner.getHp() - damage);
            if (owner.getHp() < 0) {
                owner.setHp(0);
            }
        }

        if (owner.getAsleep()) {
            updateText.add(owner.getName() + " is asleep and unable to make a move!");
        } else if (owner.getFrozen()) {
            updateText.add(owner.getName() + " is frozen and unable to make a move!");
        } else if (owner.getParalyzed()) {
            int randomNum = rand.nextInt(100);
            if (randomNum < 25) {
                updateText.add(owner.getName() + " is paralyzed and unable to make a move!");
            }
        } else {
            //need to account for accuracy/evasion
            if (!category.equals("Status")) {
                hit = (accuracy) * (owner.getAccuracy())/(enemy.getEvasion());
                Log.d("g13.mobmon","accuracy: " + accuracy);
                Log.d("g13.mobmon","owner.getAccuracy())/(enemy.getEvasion(): " + (owner.getAccuracy())/(enemy.getEvasion()));
                Log.d("g13.mobmon","hit: " + hit);

                int randomNum = rand.nextInt(100);
                Log.d("g13.mobmon","randomNum: " + randomNum);

                if (randomNum > hit) {
                    updateText.add(owner.getName() + "'s attack missed!");
                } else {
                    //same-type attack bonus: if your move is your type (ie pikachu using thunder), it does more damage
                    if (type == owner.getType1() || type == owner.getType2()) {
                        modifier = 1.5f;
                    } else {
                        modifier = 1;
                    }

                    //check for super effective, not very effective, not effective, critical
                    //modifier *= 0, 0.25, 0.5, 1, 2, or 4 depending on defender type
                    float[][]typeChart = { //x (vertical) == attacker, y (horizontal) == defender
                            //nor, fir, wat, ele, gra, ice, fig, poi, gro, fly, psy, bug, roc, gho, dra, dar, ste
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,0.5f,   0,   1,   1,0.5f},//Normal
                            {   1,0.5f,0.5f,   1,   2,   2,   1,   1,   1,   1,   1,   2,0.5f,   1,0.5f,   1,   2},//Fire
                            {   1,   2,0.5f,   1,0.5f,   1,   1,   1,   2,   1,   1,   1,   2,   1,0.5f,   1,   1},//Water
                            {   1,   1,   2,0.5f,0.5f,   1,   1,   1,   0,   2,   1,   1,   1,   1,0.5f,   1,   1},//Electric
                            {   1,0.5f,   2,   1,0.5f,   1,   1,0.5f,   2,0.5f,   1,0.5f,   2,   1,0.5f,   1,0.5f},//Grass
                            {   1,0.5f,0.5f,   1,   2,0.5f,   1,   1,   2,   2,   1,   1,   1,   1,   2,   1,0.5f},//Ice
                            {   2,   1,   1,   1,   1,   2,   1,0.5f,   1,0.5f,0.5f,0.5f,   2,   0,   1,   2,   2},//Fighting
                            {   1,   1,   1,   1,   2,   1,   1,0.5f,0.5f,   1,   1,   1,0.5f,0.5f,   1,   1,   0},//Poison
                            {   1,   2,   1,   2,0.5f,   1,   1,   2,   1,   0,   1,0.5f,   2,   1,   1,   1,   2},//Ground
                            {   1,   1,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,   2,0.5f,   1,   1,   1,0.5f},//Flying
                            {   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,0.5f,   1,   1,   1,   1,   0,0.5f},//Psychic
                            {   1,0.5f,   1,   1,   2,   1,0.5f,0.5f,   1,0.5f,   2,   1,   1,0.5f,   1,   2,0.5f},//Bug
                            {   1,   2,   1,   1,   1,   2,0.5f,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,0.5f},//Rock
                            {   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Ghost
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,0.5f},//Dragon
                            {   1,   1,   1,   1,   1,   1,0.5f,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Dark
                            {   1,0.5f,0.5f,0.5f,   1,   2,   1,   1,   1,   1,   1,   1,   2,   0,   1,   1,0.5f}//Steel
                    };

                    modifier *= typeChart[type][enemy.getType1()];

                    //if they have a second type, calculate that too
                    if (enemy.getType2() > 0) {
                        modifier *= typeChart[type][enemy.getType2()];
                    }

                    //if critical, modifier *= 2
                    //add modifiers to detect whether to increase "critical", such as buffs from moves, etc.
                    randomNum = 0;
                    if (critical == 0) { //1/16(6.25%)
                        randomNum = rand.nextInt(16);
                    } else if (critical == 1) { //1/8(12.5%)
                        randomNum = rand.nextInt(8);
                    } else if (critical == 2) { //1/4(25%)
                        randomNum = rand.nextInt(4);
                    } else if (critical == 3) { //1/3(33.3%)
                        randomNum = rand.nextInt(3);
                    } else if (critical >= 4) { //1/2(50%)
                        randomNum = rand.nextInt(2);
                    }

                    if (randomNum < 1) {//critical hit!
                        //PRINT CRITICAL HIT!
                        updateText.add("CRITICAL HIT!");
                        modifier *= 2;
                    }

                    if (enemy.getType2() > 0) {
                        if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    } else {
                        if (typeChart[type][enemy.getType1()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    }

                    //other accounts for held items, abilities, field advantages, double or triple battle modifiers

                    // nextInt is normally exclusive of the top value,
                    // so add 1 to make it inclusive
                    float r = (float)(rand.nextInt(16) + 85)/100;
                    modifier *= r;
                    //and then modifier *= random [0.85, 1]

                    //if category is Physical, use attack/defense, if Special use special attack/special defense.
                    damage  = 0;
                    if (category.equals("Physical")) {
                        if (power > 0) {
                            damage = (int) (((float)((float)owner.getAttack()/(float)enemy.getDefense())*power)*modifier/10);
                            if (owner.getBurned()) {
                                damage/=2;
                            }
                        }
                    } else  if (category.equals("Special")) {
                        if (power > 0) {
                            damage = (int) (((float)((float)owner.getSpecialAttack()/(float)enemy.getSpecialDefense())*power)*modifier/10);
                        }
                    }

                    //apply damage
                    if (damage > 0) {
                        updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                    }

                    if (type == 1) {
                        if (enemy.getFrozen()) {
                            enemy.setFrozen(false);
                            updateText.add(enemy.getName() + " has thawed!");
                        }
                    }

                    enemy.setHp(enemy.getHp() - damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                }
            }

            doExtraEffects();
            owner.setExperienceGained(owner.getExperienceGained()+1);
            owner.setExperience(owner.getExperience()+1);
            Log.d("g13.mobmon","xp: " + owner.getExperience());
            if (owner.getExperience() > owner.getLevel()*5) {
                owner.setLevel(owner.getLevel() + 1);
                owner.setBaseHp(owner.getBaseHp());
                owner.setBaseSpeed(owner.getBaseSpeed());
                owner.setBaseAttack(owner.getBaseAttack());
                owner.setBaseDefense(owner.getBaseDefense());
                owner.setBaseSpecialAttack(owner.getBaseSpecialAttack());
                owner.setBaseSpecialDefense(owner.getBaseSpecialDefense());
                updateText.add(owner.getName() + " leveled up!");
            }
        }

        return updateText;
    }

    public void doExtraEffects() {
        Random rand = new Random();
        int randomNum;
        switch (name) {
            case "Absorb" :
                owner.setHp(owner.getHp()+(damage/2));
                if (owner.getHp() > owner.getBaseHp()) {
                    owner.setHp(owner.getBaseHp());
                }
                updateText.add(owner.getName() + " absorbed " + damage/2 + " health!");
                break;
            case "Acid" :
                randomNum = rand.nextInt(10);
                if (randomNum == 0) {
                    if (enemy.getSpecialDefense() > 0) {
                        enemy.setSpecialDefense(enemy.getSpecialDefense() - 1);
                    }
                    updateText.add(enemy.getName() + "'s Special Defense was lowered!");
                }
                break;
            case "Acid Armor" :
                owner.setDefense(owner.getDefense() + 2);
                updateText.add(owner.getName() + "'s Defense was sharply raised!");
                break;
            case "Agility" :
                owner.setSpeed(owner.getSpeed() + 2);
                updateText.add(owner.getName() + "'s Speed was sharply raised!");
                break;
            case "Amnesia" :
                owner.setSpecialDefense(owner.getSpecialDefense() + 2);
                updateText.add(owner.getName() + "'s Special Defense was sharply raised!");
                break;
            case "Aurora Beam" :
                randomNum = rand.nextInt(10);
                if (randomNum == 0) {
                    if (enemy.getAttack() > 0) {
                        enemy.setAttack(enemy.getAttack() - 1);
                    }
                    updateText.add(enemy.getName() + "'s Attack was lowered!");
                }
                break;
            case "Barrage" :
                hit2to5moreTimes();
                break;
            case "Barrier" :
                owner.setDefense(owner.getDefense() + 2);
                updateText.add(owner.getName() + "'s Defense was sharply raised!");
                break;
            case "Bide" :
                break;
            case "Bind" :
                break;
            case "Bite" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "Blizzard" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //freeze
                    if (enemy.getType1() != 5 && enemy.getType2() != 5) {
                        enemy.setFrozen(true);
                        updateText.add("It caused " + enemy.getName() + " to Freeze");
                    }
                }
                break;
            case "Body Slam" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //paralyze
                    enemy.setParalyzed(true);
                    updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                }
                break;
            case "Bone Club" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "Bonemerang" :
                updateText.add("It's coming back around again!");
                hit = (accuracy) * (owner.getAccuracy())/(enemy.getEvasion());
                randomNum = rand.nextInt(100);
                modifier = 1;
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s second attack missed!");
                } else {
                    //same-type attack bonus: if your move is your type (ie pikachu using thunder), it does more damage
                    if (type == owner.getType1() || type == owner.getType2()) {
                        modifier = 1.5f;
                    } else {
                        modifier = 1;
                    }

                    //check for super effective, not very effective, not effective, critical
                    //modifier *= 0, 0.25, 0.5, 1, 2, or 4 depending on defender type
                    float[][]typeChart = { //x (vertical) == attacker, y (horizontal) == defender
                            //nor, fir, wat, ele, gra, ice, fig, poi, gro, fly, psy, bug, roc, gho, dra, dar, ste
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,0.5f,   0,   1,   1,0.5f},//Normal
                            {   1,0.5f,0.5f,   1,   2,   2,   1,   1,   1,   1,   1,   2,0.5f,   1,0.5f,   1,   2},//Fire
                            {   1,   2,0.5f,   1,0.5f,   1,   1,   1,   2,   1,   1,   1,   2,   1,0.5f,   1,   1},//Water
                            {   1,   1,   2,0.5f,0.5f,   1,   1,   1,   0,   2,   1,   1,   1,   1,0.5f,   1,   1},//Electric
                            {   1,0.5f,   2,   1,0.5f,   1,   1,0.5f,   2,0.5f,   1,0.5f,   2,   1,0.5f,   1,0.5f},//Grass
                            {   1,0.5f,0.5f,   1,   2,0.5f,   1,   1,   2,   2,   1,   1,   1,   1,   2,   1,0.5f},//Ice
                            {   2,   1,   1,   1,   1,   2,   1,0.5f,   1,0.5f,0.5f,0.5f,   2,   0,   1,   2,   2},//Fighting
                            {   1,   1,   1,   1,   2,   1,   1,0.5f,0.5f,   1,   1,   1,0.5f,0.5f,   1,   1,   0},//Poison
                            {   1,   2,   1,   2,0.5f,   1,   1,   2,   1,   0,   1,0.5f,   2,   1,   1,   1,   2},//Ground
                            {   1,   1,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,   2,0.5f,   1,   1,   1,0.5f},//Flying
                            {   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,0.5f,   1,   1,   1,   1,   0,0.5f},//Psychic
                            {   1,0.5f,   1,   1,   2,   1,0.5f,0.5f,   1,0.5f,   2,   1,   1,0.5f,   1,   2,0.5f},//Bug
                            {   1,   2,   1,   1,   1,   2,0.5f,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,0.5f},//Rock
                            {   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Ghost
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,0.5f},//Dragon
                            {   1,   1,   1,   1,   1,   1,0.5f,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Dark
                            {   1,0.5f,0.5f,0.5f,   1,   2,   1,   1,   1,   1,   1,   1,   2,   0,   1,   1,0.5f}//Steel
                    };

                    modifier *= typeChart[type][enemy.getType1()];

                    //if they have a second type, calculate that too
                    if (enemy.getType2() > 0) {
                        modifier *= typeChart[type][enemy.getType2()];
                    }

                    //if critical, modifier *= 2
                    //add modifiers to detect whether to increase "critical", such as buffs from moves, etc.
                    randomNum = 0;
                    if (critical == 0) { //1/16(6.25%)
                        randomNum = rand.nextInt(16);
                    } else if (critical == 1) { //1/8(12.5%)
                        randomNum = rand.nextInt(8);
                    } else if (critical == 2) { //1/4(25%)
                        randomNum = rand.nextInt(4);
                    } else if (critical == 3) { //1/3(33.3%)
                        randomNum = rand.nextInt(3);
                    } else if (critical >= 4) { //1/2(50%)
                        randomNum = rand.nextInt(2);
                    }

                    if (randomNum < 1) {//critical hit!
                        //PRINT CRITICAL HIT!
                        updateText.add("CRITICAL HIT!");
                        modifier *= 2;
                    }

                    if (enemy.getType2() > 0) {
                        if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    } else {
                        if (typeChart[type][enemy.getType1()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    }

                    //other accounts for held items, abilities, field advantages, double or triple battle modifiers

                    // nextInt is normally exclusive of the top value,
                    // so add 1 to make it inclusive
                    float r = (float)(rand.nextInt(16) + 85)/100;
                    modifier *= r;
                    //and then modifier *= random [0.85, 1]

                    //if category is Physical, use attack/defense, if Special use special attack/special defense.
                    damage  = 0;
                    if (category.equals("Physical")) {
                        if (power > 0) {
                            damage = (int) (((float)((float)owner.getAttack()/(float)enemy.getDefense())*power)*modifier/10);
                        }
                    } else  if (category.equals("Special")) {
                        if (power > 0) {
                            damage = (int) (((float)((float)owner.getSpecialAttack()/(float)enemy.getSpecialDefense())*power)*modifier/10);
                        }
                    }

                    //apply damage
                    if (damage > 0) {
                        updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                    }
                    enemy.setHp(enemy.getHp()-damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                }
                break;
            case "Bubble" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    enemy.setSpeed(enemy.getSpeed() - 1);
                    updateText.add(enemy.getName() + "'s Speed was lowered!");
                }
                break;
            case "Bubble Beam" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    enemy.setSpeed(enemy.getSpeed() - 1);
                    updateText.add(enemy.getName() + "'s Speed was lowered!");
                }
                break;
            case "Clamp" :
                break;
            case "Comet Punch" :
                hit2to5moreTimes();
                updateText.add("Hit " + times + " times!");
                break;
            case "Confuse Ray" :
                //cause target to become confused. fails if the target has substitute or is already confused.
                randomNum = rand.nextInt(4) + 1;
                enemy.setConfused(true);
                enemy.startConfusedCounter(randomNum);
                updateText.add("It caused " + enemy.getName() + " to become Confused");
                break;
            case "Confusion" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //confused
                    randomNum = rand.nextInt(4) + 1;
                    enemy.setConfused(true);
                    enemy.startConfusedCounter(randomNum);
                    updateText.add("It caused " + enemy.getName() + " to become Confused");
                }
                break;
            case "Constrict" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    enemy.setSpeed(enemy.getSpeed() - 1);
                    updateText.add(enemy.getName() + "'s Speed was lowered!");
                }
                break;
            case "Conversion" :
                owner.setType1(enemy.getType1());
                String typeName1 = "";
                updateText.add(enemy.getName() + "'s primary type is now " + typeName1);
                if (enemy.getType2() > 0) {
                    owner.setType2(enemy.getType2());
                    String typeName2 = "";
                    updateText.add(enemy.getName() + "'s secondary type is now " + typeName2);
                }
                break;
            case "Counter" :
                break;
            case "Crabhammer" :
                break;
            case "Cut" :
                break;
            case "Defense Curl" :
                owner.setDefense(owner.getDefense() + 1);
                updateText.add(owner.getName() + "'s Defense was raised!");
                break;
            case "Dig" :
                break;
            case "Disable" :
                break;
            case "Dizzy Punch" :
                randomNum = rand.nextInt(10);
                if (randomNum < 2) {
                    //confused
                    randomNum = rand.nextInt(4) + 1;
                    enemy.setConfused(true);
                    enemy.startConfusedCounter(randomNum);
                    updateText.add("It caused " + enemy.getName() + " to become Confused");
                }
                break;
            case "Double Kick" :
                updateText.add("It's coming back around again!");
                hit = (accuracy) * (owner.getAccuracy())/(enemy.getEvasion());
                randomNum = rand.nextInt(100);
                modifier = 1;
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s second attack missed!");
                } else {
                    //same-type attack bonus: if your move is your type (ie pikachu using thunder), it does more damage
                    if (type == owner.getType1() || type == owner.getType2()) {
                        modifier = 1.5f;
                    } else {
                        modifier = 1;
                    }

                    //check for super effective, not very effective, not effective, critical
                    //modifier *= 0, 0.25, 0.5, 1, 2, or 4 depending on defender type
                    float[][]typeChart = { //x (vertical) == attacker, y (horizontal) == defender
                            //nor, fir, wat, ele, gra, ice, fig, poi, gro, fly, psy, bug, roc, gho, dra, dar, ste
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,0.5f,   0,   1,   1,0.5f},//Normal
                            {   1,0.5f,0.5f,   1,   2,   2,   1,   1,   1,   1,   1,   2,0.5f,   1,0.5f,   1,   2},//Fire
                            {   1,   2,0.5f,   1,0.5f,   1,   1,   1,   2,   1,   1,   1,   2,   1,0.5f,   1,   1},//Water
                            {   1,   1,   2,0.5f,0.5f,   1,   1,   1,   0,   2,   1,   1,   1,   1,0.5f,   1,   1},//Electric
                            {   1,0.5f,   2,   1,0.5f,   1,   1,0.5f,   2,0.5f,   1,0.5f,   2,   1,0.5f,   1,0.5f},//Grass
                            {   1,0.5f,0.5f,   1,   2,0.5f,   1,   1,   2,   2,   1,   1,   1,   1,   2,   1,0.5f},//Ice
                            {   2,   1,   1,   1,   1,   2,   1,0.5f,   1,0.5f,0.5f,0.5f,   2,   0,   1,   2,   2},//Fighting
                            {   1,   1,   1,   1,   2,   1,   1,0.5f,0.5f,   1,   1,   1,0.5f,0.5f,   1,   1,   0},//Poison
                            {   1,   2,   1,   2,0.5f,   1,   1,   2,   1,   0,   1,0.5f,   2,   1,   1,   1,   2},//Ground
                            {   1,   1,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,   2,0.5f,   1,   1,   1,0.5f},//Flying
                            {   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,0.5f,   1,   1,   1,   1,   0,0.5f},//Psychic
                            {   1,0.5f,   1,   1,   2,   1,0.5f,0.5f,   1,0.5f,   2,   1,   1,0.5f,   1,   2,0.5f},//Bug
                            {   1,   2,   1,   1,   1,   2,0.5f,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,0.5f},//Rock
                            {   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Ghost
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,0.5f},//Dragon
                            {   1,   1,   1,   1,   1,   1,0.5f,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Dark
                            {   1,0.5f,0.5f,0.5f,   1,   2,   1,   1,   1,   1,   1,   1,   2,   0,   1,   1,0.5f}//Steel
                    };

                    modifier *= typeChart[type][enemy.getType1()];

                    //if they have a second type, calculate that too
                    if (enemy.getType2() > 0) {
                        modifier *= typeChart[type][enemy.getType2()];
                    }

                    //if critical, modifier *= 2
                    //add modifiers to detect whether to increase "critical", such as buffs from moves, etc.
                    randomNum = 0;
                    if (critical == 0) { //1/16(6.25%)
                        randomNum = rand.nextInt(16);
                    } else if (critical == 1) { //1/8(12.5%)
                        randomNum = rand.nextInt(8);
                    } else if (critical == 2) { //1/4(25%)
                        randomNum = rand.nextInt(4);
                    } else if (critical == 3) { //1/3(33.3%)
                        randomNum = rand.nextInt(3);
                    } else if (critical >= 4) { //1/2(50%)
                        randomNum = rand.nextInt(2);
                    }

                    if (randomNum < 1) {//critical hit!
                        //PRINT CRITICAL HIT!
                        updateText.add("CRITICAL HIT!");
                        modifier *= 2;
                    }

                    if (enemy.getType2() > 0) {
                        if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    } else {
                        if (typeChart[type][enemy.getType1()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    }

                    //other accounts for held items, abilities, field advantages, double or triple battle modifiers

                    // nextInt is normally exclusive of the top value,
                    // so add 1 to make it inclusive
                    float r = (float)(rand.nextInt(16) + 85)/100;
                    modifier *= r;
                    //and then modifier *= random [0.85, 1]

                    //if category is Physical, use attack/defense, if Special use special attack/special defense.
                    damage  = 0;
                    if (category.equals("Physical")) {
                        if (power > 0) {
                            damage = (int) (((float)((float)owner.getAttack()/(float)enemy.getDefense())*power)*modifier/10);
                        }
                    } else  if (category.equals("Special")) {
                        if (power > 0) {
                            damage = (int) (((float)((float)owner.getSpecialAttack()/(float)enemy.getSpecialDefense())*power)*modifier/10);
                        }
                    }

                    //apply damage
                    if (damage > 0) {
                        updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                    }
                    enemy.setHp(enemy.getHp()-damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                }
                break;
            case "Double Slap" :
                hit2to5moreTimes();
                updateText.add("Hit " + times + " times!");
                break;
            case "Double Team" :
                owner.setEvasion(owner.getEvasion() + 1);
                updateText.add(owner.getName() + "'s Evasion was raised!");
                break;
            case "Double Edge" :
                int recoilDamage = damage/4;
                owner.setHp(owner.getHp() - recoilDamage);
                if (owner.getHp() < 0) {
                    owner.setHp(0);
                }
                updateText.add(owner.getName() + "took " + recoilDamage + " recoil damage!");
                break;
            case "Dragon Rage" :
                damage = 40;
                hit = (accuracy) * (owner.getAccuracy())/(enemy.getEvasion());
                randomNum = rand.nextInt(100);
                modifier = 1;
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s attack missed!");
                } else {
                    enemy.setHp(enemy.getHp() - damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                    updateText.add(enemy.getName() + "took " + damage + " damage!");
                }
                break;
            case "Dream Eater" :
                break;
            case "Drill Peck" :
                break;
            case "Earthquake" :
                break;
            case "Egg Bomb" :
                break;
            case "Ember" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //burn
                    if (enemy.getType1() != 1 && enemy.getType2() != 1) {
                        enemy.setBurned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Burned");
                    }
                }
                break;
            case "Explosion" :
                owner.setHp(0);
                updateText.add(owner.getName() + " has fainted!");
                break;
            case "Fire Blast" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //burn
                    if (enemy.getType1() != 1 && enemy.getType2() != 1) {
                        enemy.setBurned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Burned");
                    }
                }
                break;
            case "Fire Punch" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //burn
                    if (enemy.getType1() != 1 && enemy.getType2() != 1) {
                        enemy.setBurned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Burned");
                    }
                }
                break;
            case "Fire Spin" :
                break;
            case "Fissure" :
                hit = (owner.getLevel() - enemy.getLevel() + 30);
                randomNum = rand.nextInt(100);
                modifier = 1;
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s attack missed!");
                } else {
                    //same-type attack bonus: if your move is your type (ie pikachu using thunder), it does more damage
                    if (type == owner.getType1() || type == owner.getType2()) {
                        modifier = 1.5f;
                    } else {
                        modifier = 1;
                    }

                    //check for super effective, not very effective, not effective, critical
                    //modifier *= 0, 0.25, 0.5, 1, 2, or 4 depending on defender type
                    float[][]typeChart = { //x (vertical) == attacker, y (horizontal) == defender
                            //nor, fir, wat, ele, gra, ice, fig, poi, gro, fly, psy, bug, roc, gho, dra, dar, ste
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,0.5f,   0,   1,   1,0.5f},//Normal
                            {   1,0.5f,0.5f,   1,   2,   2,   1,   1,   1,   1,   1,   2,0.5f,   1,0.5f,   1,   2},//Fire
                            {   1,   2,0.5f,   1,0.5f,   1,   1,   1,   2,   1,   1,   1,   2,   1,0.5f,   1,   1},//Water
                            {   1,   1,   2,0.5f,0.5f,   1,   1,   1,   0,   2,   1,   1,   1,   1,0.5f,   1,   1},//Electric
                            {   1,0.5f,   2,   1,0.5f,   1,   1,0.5f,   2,0.5f,   1,0.5f,   2,   1,0.5f,   1,0.5f},//Grass
                            {   1,0.5f,0.5f,   1,   2,0.5f,   1,   1,   2,   2,   1,   1,   1,   1,   2,   1,0.5f},//Ice
                            {   2,   1,   1,   1,   1,   2,   1,0.5f,   1,0.5f,0.5f,0.5f,   2,   0,   1,   2,   2},//Fighting
                            {   1,   1,   1,   1,   2,   1,   1,0.5f,0.5f,   1,   1,   1,0.5f,0.5f,   1,   1,   0},//Poison
                            {   1,   2,   1,   2,0.5f,   1,   1,   2,   1,   0,   1,0.5f,   2,   1,   1,   1,   2},//Ground
                            {   1,   1,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,   2,0.5f,   1,   1,   1,0.5f},//Flying
                            {   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,0.5f,   1,   1,   1,   1,   0,0.5f},//Psychic
                            {   1,0.5f,   1,   1,   2,   1,0.5f,0.5f,   1,0.5f,   2,   1,   1,0.5f,   1,   2,0.5f},//Bug
                            {   1,   2,   1,   1,   1,   2,0.5f,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,0.5f},//Rock
                            {   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Ghost
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,0.5f},//Dragon
                            {   1,   1,   1,   1,   1,   1,0.5f,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Dark
                            {   1,0.5f,0.5f,0.5f,   1,   2,   1,   1,   1,   1,   1,   1,   2,   0,   1,   1,0.5f}//Steel
                    };

                    modifier *= typeChart[type][enemy.getType1()];

                    //if they have a second type, calculate that too
                    if (enemy.getType2() > 0) {
                        modifier *= typeChart[type][enemy.getType2()];
                    }

                    //if critical, modifier *= 2
                    //add modifiers to detect whether to increase "critical", such as buffs from moves, etc.
                    randomNum = 0;
                    if (critical == 0) { //1/16(6.25%)
                        randomNum = rand.nextInt(16);
                    } else if (critical == 1) { //1/8(12.5%)
                        randomNum = rand.nextInt(8);
                    } else if (critical == 2) { //1/4(25%)
                        randomNum = rand.nextInt(4);
                    } else if (critical == 3) { //1/3(33.3%)
                        randomNum = rand.nextInt(3);
                    } else if (critical >= 4) { //1/2(50%)
                        randomNum = rand.nextInt(2);
                    }

                    if (randomNum < 1) {//critical hit!
                        //PRINT CRITICAL HIT!
                        updateText.add("CRITICAL HIT!");
                        modifier *= 2;
                    }

                    if (enemy.getType2() > 0) {
                        if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    } else {
                        if (typeChart[type][enemy.getType1()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    }

                    //other accounts for held items, abilities, field advantages, double or triple battle modifiers

                    // nextInt is normally exclusive of the top value,
                    // so add 1 to make it inclusive
                    float r = (float)(rand.nextInt(16) + 85)/100;
                    modifier *= r;
                    //and then modifier *= random [0.85, 1]

                    //if category is Physical, use attack/defense, if Special use special attack/special defense.
                    damage  = (int) (enemy.getHp()*modifier);

                    //apply damage
                    if (damage > 0) {
                        updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                    }
                    enemy.setHp(enemy.getHp()-damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                }
                break;
            case "Flamethrower" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //burn
                    if (enemy.getType1() != 1 && enemy.getType2() != 1) {
                        enemy.setBurned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Burned");
                    }
                }
                break;
            case "Flash" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    enemy.setAccuracy(enemy.getAccuracy() - 1);
                    updateText.add(enemy.getName() + "'s Accuracy was lowered!");
                }
                break;
            case "Fly" :
                break;
            case "Focus Energy" :
                break;
            case "Fury Attack" :
                hit2to5moreTimes();
                break;
            case "Fury Swipes" :
                hit2to5moreTimes();
                break;
            case "Glare" :
                break;
            case "Growl" :
                enemy.setAttack(enemy.getAttack() - 1);
                updateText.add(enemy.getName() + "'s Attack was lowered!");
                break;
            case "Growth" :
                owner.setSpecialAttack(owner.getSpecialAttack() + 1);
                updateText.add(owner.getName() + "'s Special Attack was raised!");
                break;
            case "Guillotine" :
                hit = 3;
                randomNum = rand.nextInt(10);
                modifier = 1;
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s attack missed!");
                } else {
                    //same-type attack bonus: if your move is your type (ie pikachu using thunder), it does more damage
                    if (type == owner.getType1() || type == owner.getType2()) {
                        modifier = 1.5f;
                    } else {
                        modifier = 1;
                    }

                    //check for super effective, not very effective, not effective, critical
                    //modifier *= 0, 0.25, 0.5, 1, 2, or 4 depending on defender type
                    float[][]typeChart = { //x (vertical) == attacker, y (horizontal) == defender
                            //nor, fir, wat, ele, gra, ice, fig, poi, gro, fly, psy, bug, roc, gho, dra, dar, ste
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,0.5f,   0,   1,   1,0.5f},//Normal
                            {   1,0.5f,0.5f,   1,   2,   2,   1,   1,   1,   1,   1,   2,0.5f,   1,0.5f,   1,   2},//Fire
                            {   1,   2,0.5f,   1,0.5f,   1,   1,   1,   2,   1,   1,   1,   2,   1,0.5f,   1,   1},//Water
                            {   1,   1,   2,0.5f,0.5f,   1,   1,   1,   0,   2,   1,   1,   1,   1,0.5f,   1,   1},//Electric
                            {   1,0.5f,   2,   1,0.5f,   1,   1,0.5f,   2,0.5f,   1,0.5f,   2,   1,0.5f,   1,0.5f},//Grass
                            {   1,0.5f,0.5f,   1,   2,0.5f,   1,   1,   2,   2,   1,   1,   1,   1,   2,   1,0.5f},//Ice
                            {   2,   1,   1,   1,   1,   2,   1,0.5f,   1,0.5f,0.5f,0.5f,   2,   0,   1,   2,   2},//Fighting
                            {   1,   1,   1,   1,   2,   1,   1,0.5f,0.5f,   1,   1,   1,0.5f,0.5f,   1,   1,   0},//Poison
                            {   1,   2,   1,   2,0.5f,   1,   1,   2,   1,   0,   1,0.5f,   2,   1,   1,   1,   2},//Ground
                            {   1,   1,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,   2,0.5f,   1,   1,   1,0.5f},//Flying
                            {   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,0.5f,   1,   1,   1,   1,   0,0.5f},//Psychic
                            {   1,0.5f,   1,   1,   2,   1,0.5f,0.5f,   1,0.5f,   2,   1,   1,0.5f,   1,   2,0.5f},//Bug
                            {   1,   2,   1,   1,   1,   2,0.5f,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,0.5f},//Rock
                            {   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Ghost
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,0.5f},//Dragon
                            {   1,   1,   1,   1,   1,   1,0.5f,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Dark
                            {   1,0.5f,0.5f,0.5f,   1,   2,   1,   1,   1,   1,   1,   1,   2,   0,   1,   1,0.5f}//Steel
                    };

                    modifier *= typeChart[type][enemy.getType1()];

                    //if they have a second type, calculate that too
                    if (enemy.getType2() > 0) {
                        modifier *= typeChart[type][enemy.getType2()];
                    }

                    //if critical, modifier *= 2
                    //add modifiers to detect whether to increase "critical", such as buffs from moves, etc.
                    randomNum = 0;
                    if (critical == 0) { //1/16(6.25%)
                        randomNum = rand.nextInt(16);
                    } else if (critical == 1) { //1/8(12.5%)
                        randomNum = rand.nextInt(8);
                    } else if (critical == 2) { //1/4(25%)
                        randomNum = rand.nextInt(4);
                    } else if (critical == 3) { //1/3(33.3%)
                        randomNum = rand.nextInt(3);
                    } else if (critical >= 4) { //1/2(50%)
                        randomNum = rand.nextInt(2);
                    }

                    if (randomNum < 1) {//critical hit!
                        //PRINT CRITICAL HIT!
                        updateText.add("CRITICAL HIT!");
                        modifier *= 2;
                    }

                    if (enemy.getType2() > 0) {
                        if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    } else {
                        if (typeChart[type][enemy.getType1()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    }

                    //other accounts for held items, abilities, field advantages, double or triple battle modifiers

                    // nextInt is normally exclusive of the top value,
                    // so add 1 to make it inclusive
                    float r = (float)(rand.nextInt(16) + 85)/100;
                    modifier *= r;
                    //and then modifier *= random [0.85, 1]

                    //if category is Physical, use attack/defense, if Special use special attack/special defense.
                    damage  = (int) (enemy.getHp()*modifier);

                    //apply damage
                    if (damage > 0) {
                        updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                    }
                    enemy.setHp(enemy.getHp()-damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                }
                break;
            case "Gust" :
                break;
            case "Harden" :
                owner.setDefense(owner.getDefense() + 1);
                updateText.add(owner.getName() + "'s Defense was raised!");
                break;
            case "Haze" :
                //speed, attack, defense, special attack, special defense, accuracy, evasion
                owner.setSpeed(owner.getBaseSpeed());
                owner.setAttack(owner.getBaseAttack());
                owner.setDefense(owner.getBaseDefense());
                owner.setSpecialAttack(owner.getBaseSpecialAttack());
                owner.setSpecialDefense(owner.getBaseSpecialDefense());
                owner.setAccuracy(100);
                owner.setEvasion(100);

                enemy.setSpeed(enemy.getBaseSpeed());
                enemy.setAttack(enemy.getBaseAttack());
                enemy.setDefense(enemy.getBaseDefense());
                enemy.setSpecialAttack(enemy.getBaseSpecialAttack());
                enemy.setSpecialDefense(enemy.getBaseSpecialDefense());
                enemy.setAccuracy(100);
                enemy.setEvasion(100);
                break;
            case "Headbutt" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "High Jump Kick" :
                break;
            case "Horn Attack" :
                break;
            case "Horn Drill" :
                hit = 3;
                randomNum = rand.nextInt(10);
                modifier = 1;
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s attack missed!");
                } else {
                    //same-type attack bonus: if your move is your type (ie pikachu using thunder), it does more damage
                    if (type == owner.getType1() || type == owner.getType2()) {
                        modifier = 1.5f;
                    } else {
                        modifier = 1;
                    }

                    //check for super effective, not very effective, not effective, critical
                    //modifier *= 0, 0.25, 0.5, 1, 2, or 4 depending on defender type
                    float[][]typeChart = { //x (vertical) == attacker, y (horizontal) == defender
                            //nor, fir, wat, ele, gra, ice, fig, poi, gro, fly, psy, bug, roc, gho, dra, dar, ste
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,0.5f,   0,   1,   1,0.5f},//Normal
                            {   1,0.5f,0.5f,   1,   2,   2,   1,   1,   1,   1,   1,   2,0.5f,   1,0.5f,   1,   2},//Fire
                            {   1,   2,0.5f,   1,0.5f,   1,   1,   1,   2,   1,   1,   1,   2,   1,0.5f,   1,   1},//Water
                            {   1,   1,   2,0.5f,0.5f,   1,   1,   1,   0,   2,   1,   1,   1,   1,0.5f,   1,   1},//Electric
                            {   1,0.5f,   2,   1,0.5f,   1,   1,0.5f,   2,0.5f,   1,0.5f,   2,   1,0.5f,   1,0.5f},//Grass
                            {   1,0.5f,0.5f,   1,   2,0.5f,   1,   1,   2,   2,   1,   1,   1,   1,   2,   1,0.5f},//Ice
                            {   2,   1,   1,   1,   1,   2,   1,0.5f,   1,0.5f,0.5f,0.5f,   2,   0,   1,   2,   2},//Fighting
                            {   1,   1,   1,   1,   2,   1,   1,0.5f,0.5f,   1,   1,   1,0.5f,0.5f,   1,   1,   0},//Poison
                            {   1,   2,   1,   2,0.5f,   1,   1,   2,   1,   0,   1,0.5f,   2,   1,   1,   1,   2},//Ground
                            {   1,   1,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,   2,0.5f,   1,   1,   1,0.5f},//Flying
                            {   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,0.5f,   1,   1,   1,   1,   0,0.5f},//Psychic
                            {   1,0.5f,   1,   1,   2,   1,0.5f,0.5f,   1,0.5f,   2,   1,   1,0.5f,   1,   2,0.5f},//Bug
                            {   1,   2,   1,   1,   1,   2,0.5f,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,0.5f},//Rock
                            {   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Ghost
                            {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,0.5f},//Dragon
                            {   1,   1,   1,   1,   1,   1,0.5f,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Dark
                            {   1,0.5f,0.5f,0.5f,   1,   2,   1,   1,   1,   1,   1,   1,   2,   0,   1,   1,0.5f}//Steel
                    };

                    modifier *= typeChart[type][enemy.getType1()];

                    //if they have a second type, calculate that too
                    if (enemy.getType2() > 0) {
                        modifier *= typeChart[type][enemy.getType2()];
                    }

                    //if critical, modifier *= 2
                    //add modifiers to detect whether to increase "critical", such as buffs from moves, etc.
                    randomNum = 0;
                    if (critical == 0) { //1/16(6.25%)
                        randomNum = rand.nextInt(16);
                    } else if (critical == 1) { //1/8(12.5%)
                        randomNum = rand.nextInt(8);
                    } else if (critical == 2) { //1/4(25%)
                        randomNum = rand.nextInt(4);
                    } else if (critical == 3) { //1/3(33.3%)
                        randomNum = rand.nextInt(3);
                    } else if (critical >= 4) { //1/2(50%)
                        randomNum = rand.nextInt(2);
                    }

                    if (randomNum < 1) {//critical hit!
                        //PRINT CRITICAL HIT!
                        updateText.add("CRITICAL HIT!");
                        modifier *= 2;
                    }

                    if (enemy.getType2() > 0) {
                        if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    } else {
                        if (typeChart[type][enemy.getType1()] > 1) {
                            updateText.add("It's super effective!");
                        } else if (typeChart[type][enemy.getType1()] == 0){
                            updateText.add("It's not effective...");
                        } else if (typeChart[type][enemy.getType1()] < 1) {
                            updateText.add("It's not very effective.");
                        }
                    }

                    //other accounts for held items, abilities, field advantages, double or triple battle modifiers

                    // nextInt is normally exclusive of the top value,
                    // so add 1 to make it inclusive
                    float r = (float)(rand.nextInt(16) + 85)/100;
                    modifier *= r;
                    //and then modifier *= random [0.85, 1]

                    //if category is Physical, use attack/defense, if Special use special attack/special defense.
                    damage  = (int) (enemy.getHp()*modifier);

                    //apply damage
                    if (damage > 0) {
                        updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                    }
                    enemy.setHp(enemy.getHp()-damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                }
                break;
            case "Hydro Pump" :
                break;
            case "Hyper Beam" :
                break;
            case "Hyper Fang" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "Hypnosis" :
                randomNum = rand.nextInt(10);
                if (randomNum < 6) {
                    //sleep
                    enemy.setAsleep(true);
                    updateText.add("It caused " + enemy.getName() + " to fall Asleep");
                }
                break;
            case "Ice Beam" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //freeze
                    if (enemy.getType1() != 5 && enemy.getType2() != 5) {
                        enemy.setFrozen(true);
                        updateText.add("It caused " + enemy.getName() + " to Freeze");
                    }
                }
                break;
            case "Ice Punch" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //freeze
                    if (enemy.getType1() != 5 && enemy.getType2() != 5) {
                        enemy.setFrozen(true);
                        updateText.add("It caused " + enemy.getName() + " to Freeze");
                    }
                }
                break;
            case "Jump Kick" :
                break;
            case "Karate Chop" :
                break;
            case "Kinesis" :
                enemy.setAccuracy(enemy.getAccuracy() - 1);
                updateText.add(enemy.getName() + "'s Accuracy was lowered!");
                break;
            case "Leech Life" :
                owner.setHp(owner.getHp()+(damage/2));
                if (owner.getHp() > owner.getBaseHp()) {
                    owner.setHp(owner.getBaseHp());
                }
                updateText.add(owner.getName() + " absorbed " + damage/2 + " health!");
                break;
            case "Leech Seed" :
                break;
            case "Leer" :
                enemy.setDefense(enemy.getDefense() - 1);
                updateText.add(enemy.getName() + "'s Defense was lowered!");
                break;
            case "Lick" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //paralyze
                    enemy.setParalyzed(true);
                    updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                }
                break;
            case "Light Screen" :
                break;
            case "Lovely Kiss" :
                //sleep
                enemy.setAsleep(true);
                updateText.add("It caused " + enemy.getName() + " to fall Asleep");
                break;
            case "Low Kick" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "Meditate" :
                owner.setAttack(owner.getAttack() + 1);
                updateText.add(owner.getName() + "'s Attack was raised!");
                break;
            case "Mega Drain" :
                owner.setHp(owner.getHp()+(damage/2));
                if (owner.getHp() > owner.getBaseHp()) {
                    owner.setHp(owner.getBaseHp());
                }
                updateText.add(owner.getName() + " absorbed " + damage/2 + " health!");
                break;
            case "Mega Kick" :
                break;
            case "Mega Punch" :
                break;
            case "Metronome" :
                break;
            case "Mimic" :
                break;
            case "Minimize" :
                owner.setEvasion(owner.getEvasion() + 1);
                updateText.add(owner.getName() + "'s Evasion was raised!");
                break;
            case "Mirror Move" :
                break;
            case "Mist" :
                break;
            case "Night Shade" :
                //need to account for accuracy/evasion
                hit = (accuracy) * (owner.getAccuracy())/(enemy.getEvasion());
                randomNum = rand.nextInt(100);
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s attack missed!");
                } else {
                    //if category is Physical, use attack/defense, if Special use special attack/special defense.
                    damage  = owner.getLevel();

                    //apply damage
                    if (damage > 0) {
                        updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                    }
                    enemy.setHp(enemy.getHp()-damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                }
                break;
            case "Pay Day" :
                break;
            case "Peck" :
                break;
            case "Petal Dance" :
                break;
            case "Pin Missile" :
                hit2to5moreTimes();
                break;
            case "Poison Gas" :
                randomNum = rand.nextInt(10);
                if (randomNum < 8) {
                    //poison
                    if (enemy.getType1() != 7 && enemy.getType2() != 7) {
                        enemy.setPoisoned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Poisoned");
                    }
                }
                break;
            case "Poison Powder" :
                //poison
                if (enemy.getType1() != 7 && enemy.getType2() != 7) {
                    enemy.setPoisoned(true);
                    updateText.add("It caused " + enemy.getName() + " to become Poisoned");
                }
                break;
            case "Poison Sting" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //poison
                    if (enemy.getType1() != 7 && enemy.getType2() != 7) {
                        enemy.setPoisoned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Poisoned");
                    }
                }
                break;
            case "Pound" :
                break;
            case "Psybeam" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //confused
                    randomNum = rand.nextInt(4) + 1;
                    enemy.setConfused(true);
                    enemy.startConfusedCounter(randomNum);
                    updateText.add("It caused " + enemy.getName() + " to become Confused");
                }
                break;
            case "Psychic" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    enemy.setSpecialDefense(enemy.getSpecialDefense() - 1);
                    updateText.add(enemy.getName() + "'s Special Defense was lowered!");
                }
                break;
            case "Psywave" :
                randomNum = rand.nextInt(100) + 50;
                damage = (randomNum/100) * owner.getLevel();
                //apply damage
                if (damage > 0) {
                    updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                }
                enemy.setHp(enemy.getHp()-damage);
                if (enemy.getHp() < 0) {
                    enemy.setHp(0);
                }
                break;
            case "Quick Attack" :
                break;
            case "Rage" :
                break;
            case "Razor Leaf" :
                break;
            case "Razor Wind" :
                break;
            case "Recover" :
                owner.setHp(owner.getHp() + owner.getBaseHp() / 2);
                if (owner.getHp() > owner.getBaseHp()) {
                    owner.setHp(owner.getBaseHp());
                }
                updateText.add(owner.getName() + " recovered" + owner.getBaseHp()/2 + " health");
                break;
            case "Reflect" :
                break;
            case "Rest" :
                //sleep
                enemy.setAsleep(true);
                updateText.add("It caused " + owner.getName() + " to fall Asleep");
                break;
            case "Roar" :
                break;
            case "Rock Slide" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "Rock Throw" :
                break;
            case "Rolling Kick" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "Sand Attack" :
                enemy.setAccuracy(enemy.getAccuracy() - 1);
                updateText.add(enemy.getName() + "'s Accuracy was lowered!");
                break;
            case "Scratch" :
                break;
            case "Screech" :
                enemy.setDefense(enemy.getDefense() - 2);
                updateText.add(enemy.getName() + "'s Defense was sharply lowered!");
                break;
            case "Seismic Toss" :
                damage = owner.getLevel();
                //apply damage
                if (damage > 0) {
                    updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                }
                enemy.setHp(enemy.getHp()-damage);
                if (enemy.getHp() < 0) {
                    enemy.setHp(0);
                }
                break;
            case "Self-Destruct" :
                owner.setHp(0);
                updateText.add(owner.getName() + " has fainted!");
                break;
            case "Sharpen" :
                owner.setAttack(owner.getAttack() + 1);
                updateText.add(owner.getName() + "'s Attack was raised!");
                break;
            case "Sing" :
                //sleep
                enemy.setAsleep(true);
                updateText.add("It caused " + enemy.getName() + " to fall Asleep");
                break;
            case "Skull Bash" :
                break;
            case "Sky Attack" :
                break;
            case "Slam" :
                break;
            case "Slash" :
                break;
            case "Sleep Powder" :
                //sleep
                enemy.setAsleep(true);
                updateText.add("It caused " + enemy.getName() + " to fall Asleep");
                break;
            case "Sludge" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //poison
                    if (enemy.getType1() != 7 && enemy.getType2() != 7) {
                        enemy.setPoisoned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Poisoned");
                    }
                }
                break;
            case "Smog" :
                randomNum = rand.nextInt(10);
                if (randomNum < 4) {
                    //poison
                    if (enemy.getType1() != 7 && enemy.getType2() != 7) {
                        enemy.setPoisoned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Poisoned");
                    }
                }
                break;
            case "Smokescreen" :
                enemy.setAccuracy(enemy.getAccuracy() - 1);
                updateText.add(enemy.getName() + "'s Accuracy was lowered!");
                break;
            case "Soft Boiled" :
                owner.setHp(owner.getHp() + owner.getBaseHp() / 2);
                if (owner.getHp() > owner.getBaseHp()) {
                    owner.setHp(owner.getBaseHp());
                }
                updateText.add(owner.getName() + " recovered" + owner.getBaseHp()/2 + " health");
                break;
            case "Solar Beam" :
                break;
            case "Sonic Boom" :
                damage = 20;
                hit = (accuracy) * (owner.getAccuracy())/(enemy.getEvasion());
                randomNum = rand.nextInt(100);
                modifier = 1;
                if (randomNum < hit) {
                    updateText.add(owner.getName() + "'s attack missed!");
                } else {
                    enemy.setHp(enemy.getHp() - damage);
                    if (enemy.getHp() < 0) {
                        enemy.setHp(0);
                    }
                    updateText.add(enemy.getName() + "took " + damage + " damage!");
                }
                break;
            case "Spike Cannon" :
                hit2to5moreTimes();
                break;
            case "Splash" :
                updateText.add("But nothing happened!");
                break;
            case "Spore" :
                //sleep
                enemy.setAsleep(true);
                updateText.add("It caused " + enemy.getName() + " to fall Asleep");
                break;
            case "Stomp" :
                randomNum = rand.nextInt(10);
                if (randomNum < 3) {
                    //flinch
                    if (enemy.getSpeed() < owner.getSpeed()) {
                        enemy.setFlinched(true);
                        updateText.add("It caused " + enemy.getName() + " to Flinch");
                    }
                }
                break;
            case "Strength" :
                break;
            case "String Shot" :
                enemy.setSpeed(enemy.getSpeed() - 1);
                updateText.add(enemy.getName() + "'s Speed was lowered!");
                break;
            case "Struggle" :
                break;
            case "Stun Spore" :
                //paralyze
                enemy.setParalyzed(true);
                updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                break;
            case "Submission" :
                owner.setHp(owner.getHp() - damage/4);
                updateText.add(owner.getName() + "took damage " + damage/4 + " from the recoil!");
                break;
            case "Substitute" :
                break;
            case "Super Fang" :
                damage = enemy.getHp()/2;
                //apply damage
                if (damage > 0) {
                    updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                }
                enemy.setHp(enemy.getHp()-damage);
                if (enemy.getHp() < 0) {
                    enemy.setHp(0);
                }
                break;
            case "Supersonic" :
                //confusion
                randomNum = rand.nextInt(4) + 1;
                enemy.setConfused(true);
                enemy.startConfusedCounter(randomNum);
                updateText.add("It caused " + enemy.getName() + " to become Confused");
                break;
            case "Surf" :
                break;
            case "Swift" :
                break;
            case "Swords Dance" :
                owner.setAttack(owner.getAttack() + 2);
                updateText.add(owner.getName() + "'s Attack was sharply raised!");
                break;
            case "Tackle" :
                break;
            case "Tail Whip" :
                enemy.setDefense(enemy.getDefense() - 1);
                updateText.add(enemy.getName() + "'s Defense was lowered!");
                break;
            case "Take Down" :
                owner.setHp(owner.getHp() - damage/4);
                updateText.add(owner.getName() + "took damage " + damage/4 + " from the recoil!");
                break;
            case "Teleport" :
                break;
            case "Thrash" :
                break;
            case "Thunder" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //paralyze
                    if (enemy.getType1() != 8 && enemy.getType2() != 8 &&
                            enemy.getType1() != 12 && enemy.getType2() != 12)  {
                        enemy.setParalyzed(true);
                        updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                    }
                }
                break;
            case "Thunder Punch" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //paralyze
                    if (enemy.getType1() != 8 && enemy.getType2() != 8 &&
                            enemy.getType1() != 12 && enemy.getType2() != 12)  {
                        enemy.setParalyzed(true);
                        updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                    }
                }
                break;
            case "Thunder Shock" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //paralyze
                    if (enemy.getType1() != 8 && enemy.getType2() != 8 &&
                            enemy.getType1() != 12 && enemy.getType2() != 12)  {
                        enemy.setParalyzed(true);
                        updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                    }
                }
                break;
            case "Thunder Wave" :
                //paralyze
                if (enemy.getType1() != 8 && enemy.getType2() != 8 &&
                        enemy.getType1() != 12 && enemy.getType2() != 12)  {
                    enemy.setParalyzed(true);
                    updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                }
                break;
            case "Thunderbolt" :
                randomNum = rand.nextInt(10);
                if (randomNum < 1) {
                    //paralyze
                    if (enemy.getType1() != 8 && enemy.getType2() != 8 &&
                            enemy.getType1() != 12 && enemy.getType2() != 12)  {
                        enemy.setParalyzed(true);
                        updateText.add("It caused " + enemy.getName() + " to become Paralyzed");
                    }
                }
                break;
            case "Toxic" :
                break;
            case "Transform" :
                break;
            case "Tri Attack" :
                break;
            case "Twineedle" :
                randomNum = rand.nextInt(10);
                if (randomNum < 2) {
                    //poison
                    if (enemy.getType1() != 7 && enemy.getType2() != 7) {
                        enemy.setPoisoned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Poisoned");
                    }
                }
                //apply damage
                if (damage > 0) {
                    updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
                }
                enemy.setHp(enemy.getHp()-damage);
                if (enemy.getHp() < 0) {
                    enemy.setHp(0);
                }
                randomNum = rand.nextInt(10);
                if (randomNum < 2) {
                    //poison
                    if (enemy.getType1() != 7 && enemy.getType2() != 7) {
                        enemy.setPoisoned(true);
                        updateText.add("It caused " + enemy.getName() + " to become Poisoned");
                    }
                }
                break;
            case "Vice Grip" :
                break;
            case "Vine Whip" :
                break;
            case "Water Gun" :
                break;
            case "Waterfall" :
                break;
            case "Whirlwind" :
                owner.setDefense(owner.getDefense() + 1);
                updateText.add(owner.getName() + "'s Defense was raised!");
                break;
            case "Wing Attack" :
                break;
            case "Withdraw" :
                break;
            case "Wrap" :
                break;
            default :
                break;
        }
    }

    public void setPp(int pp) { this.pp = pp; }
    public void setAccuracy(int accuracy) { this.accuracy = accuracy; }

    public int getPower() { return power; }
    public int getMaxpp() { return maxpp; }
    public int getPp() { return pp; }
    public int getAccuracy() { return accuracy; }
    public String getName() { return name; }
    public int getType() { return type; }
    public String getCategory() { return category; }
    public String getDescription() { return description; }

    private void hit2to5moreTimes() {
        Random rand = new Random();
        int randomNum = rand.nextInt(1000);
        times = 0;
        if (randomNum > 875) {
            //hit 5 times (+4)
            times = 4;
        } else if (randomNum > 750) {
            //hit 4 times (+3)
            times = 3;
        } else if (randomNum > 350) {
            //hit 3 times (+2)
            times = 2;
        } else {
            //hit 2 times (+1)
            times = 1;
        }
        for (int i=0; i<times; i++) {
            //same-type attack bonus: if your move is your type (ie pikachu using thunder), it does more damage
            if (type == owner.getType1() || type == owner.getType2()) {
                modifier = 1.5f;
            } else {
                modifier = 1;
            }

            //check for super effective, not very effective, not effective, critical
            //modifier *= 0, 0.25, 0.5, 1, 2, or 4 depending on defender type
            float[][]typeChart = { //x (vertical) == attacker, y (horizontal) == defender
                    //nor, fir, wat, ele, gra, ice, fig, poi, gro, fly, psy, bug, roc, gho, dra, dar, ste
                    {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,0.5f,   0,   1,   1,0.5f},//Normal
                    {   1,0.5f,0.5f,   1,   2,   2,   1,   1,   1,   1,   1,   2,0.5f,   1,0.5f,   1,   2},//Fire
                    {   1,   2,0.5f,   1,0.5f,   1,   1,   1,   2,   1,   1,   1,   2,   1,0.5f,   1,   1},//Water
                    {   1,   1,   2,0.5f,0.5f,   1,   1,   1,   0,   2,   1,   1,   1,   1,0.5f,   1,   1},//Electric
                    {   1,0.5f,   2,   1,0.5f,   1,   1,0.5f,   2,0.5f,   1,0.5f,   2,   1,0.5f,   1,0.5f},//Grass
                    {   1,0.5f,0.5f,   1,   2,0.5f,   1,   1,   2,   2,   1,   1,   1,   1,   2,   1,0.5f},//Ice
                    {   2,   1,   1,   1,   1,   2,   1,0.5f,   1,0.5f,0.5f,0.5f,   2,   0,   1,   2,   2},//Fighting
                    {   1,   1,   1,   1,   2,   1,   1,0.5f,0.5f,   1,   1,   1,0.5f,0.5f,   1,   1,   0},//Poison
                    {   1,   2,   1,   2,0.5f,   1,   1,   2,   1,   0,   1,0.5f,   2,   1,   1,   1,   2},//Ground
                    {   1,   1,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,   2,0.5f,   1,   1,   1,0.5f},//Flying
                    {   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,0.5f,   1,   1,   1,   1,   0,0.5f},//Psychic
                    {   1,0.5f,   1,   1,   2,   1,0.5f,0.5f,   1,0.5f,   2,   1,   1,0.5f,   1,   2,0.5f},//Bug
                    {   1,   2,   1,   1,   1,   2,0.5f,   1,0.5f,   2,   1,   2,   1,   1,   1,   1,0.5f},//Rock
                    {   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Ghost
                    {   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   1,0.5f},//Dragon
                    {   1,   1,   1,   1,   1,   1,0.5f,   1,   1,   1,   2,   1,   1,   2,   1,0.5f,0.5f},//Dark
                    {   1,0.5f,0.5f,0.5f,   1,   2,   1,   1,   1,   1,   1,   1,   2,   0,   1,   1,0.5f}//Steel
            };

            modifier *= typeChart[type][enemy.getType1()];

            //if they have a second type, calculate that too
            if (enemy.getType2() > 0) {
                modifier *= typeChart[type][enemy.getType2()];
            }

            //if critical, modifier *= 2
            //add modifiers to detect whether to increase "critical", such as buffs from moves, etc.
            randomNum = 0;
            if (critical == 0) { //1/16(6.25%)
                randomNum = rand.nextInt(16);
            } else if (critical == 1) { //1/8(12.5%)
                randomNum = rand.nextInt(8);
            } else if (critical == 2) { //1/4(25%)
                randomNum = rand.nextInt(4);
            } else if (critical == 3) { //1/3(33.3%)
                randomNum = rand.nextInt(3);
            } else if (critical >= 4) { //1/2(50%)
                randomNum = rand.nextInt(2);
            }

            if (randomNum < 1) {//critical hit!
                //PRINT CRITICAL HIT!
                updateText.add("CRITICAL HIT!");
                modifier *= 2;
            }

            if (enemy.getType2() > 0) {
                if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] > 1) {
                    updateText.add("It's super effective!");
                } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] == 0){
                    updateText.add("It's not effective...");
                } else if (typeChart[type][enemy.getType1()] * typeChart[type][enemy.getType2()] < 1) {
                    updateText.add("It's not very effective.");
                }
            } else {
                if (typeChart[type][enemy.getType1()] > 1) {
                    updateText.add("It's super effective!");
                } else if (typeChart[type][enemy.getType1()] == 0){
                    updateText.add("It's not effective...");
                } else if (typeChart[type][enemy.getType1()] < 1) {
                    updateText.add("It's not very effective.");
                }
            }

            //other accounts for held items, abilities, field advantages, double or triple battle modifiers

            // nextInt is normally exclusive of the top value,
            // so add 1 to make it inclusive
            float r = (float)(rand.nextInt(16) + 85)/100;
            modifier *= r;
            //and then modifier *= random [0.85, 1]

            //if category is Physical, use attack/defense, if Special use special attack/special defense.
            damage  = 0;
            if (category.equals("Physical")) {
                if (power > 0) {
                    damage = (int) (((float)((float)owner.getAttack()/(float)enemy.getDefense())*power)*modifier/10);
                }
            } else  if (category.equals("Special")) {
                if (power > 0) {
                    damage = (int) (((float)((float)owner.getSpecialAttack()/(float)enemy.getSpecialDefense())*power)*modifier/10);
                }
            }

            //apply damage
            if (damage > 0) {
                updateText.add(owner.getName() + " dealt " + damage + " damage to " + enemy.getName() + "!");
            }
            enemy.setHp(enemy.getHp()-damage);
            if (enemy.getHp() < 0) {
                enemy.setHp(0);
            }
        }
        updateText.add("Hit " + times + " times!");
    }
}

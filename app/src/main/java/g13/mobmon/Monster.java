package g13.mobmon;

public class Monster {
    String name;
    private int sleepCounter, confusedCounter, monsterNumber, level = 0, experience = 0, experienceGained = 0, type1, type2,
            hp, speed, attack, defense, specialAttack, specialDefense, accuracy, evasion,
            baseHp, baseSpeed, baseAttack, baseDefense, baseSpecialAttack, baseSpecialDefense;
    private Move move1, move2, move3, move4;
    private boolean flinched = false, frozen = false, paralyzed = false, confused = false,
            burned = false, asleep = false, poisoned = false;
    //make an arraylist of statuses

    public Monster () { }

    public void startSleepCounter(int sleepCounter) { this.sleepCounter = sleepCounter; }
    public boolean runSleepCounter () {
        sleepCounter--;
        if (sleepCounter == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void startConfusedCounter(int confusedCounter) { this.confusedCounter = confusedCounter; }
    public boolean runConfusedCounter () {
        confusedCounter--;
        if (confusedCounter == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void setFlinched(boolean flinched) { this.flinched = flinched; }
    public void setFrozen(boolean frozen) { this.frozen = frozen; }
    public void setParalyzed(boolean paralyzed) { this.paralyzed = paralyzed; }
    public void setConfused(boolean confused) { this.confused = confused; }
    public void setBurned(boolean burned) { this.burned = burned; }
    public void setAsleep(boolean asleep) { this.asleep = asleep; }
    public void setPoisoned(boolean poisoned) { this.poisoned = poisoned; }

    public void setHp(int hp) { this.hp = hp; }
    public void setSpeed(int speed) { this.speed = speed; }
    public void setAttack(int attack) { this.attack = attack; }
    public void setDefense(int defense) { this.defense = defense; }
    public void setSpecialAttack(int specialAttack) { this.specialAttack = specialAttack; }
    public void setSpecialDefense(int specialDefense) { this.specialDefense = specialDefense; }
    public void setAccuracy(int accuracy) { this.accuracy = accuracy; }
    public void setEvasion(int evasion) { this.evasion = evasion; }

    public void setBaseHp(int baseHp) { this.baseHp = (int)Math.floor(baseHp*2*level/100)+level+10; }
    public void setBaseSpeed(int baseSpeed) { this.baseSpeed = (int)Math.floor(baseSpeed*2*level/100)+5; }
    public void setBaseAttack(int baseAttack) { this.baseAttack = (int)Math.floor(baseAttack*2*level/100)+5; }
    public void setBaseDefense(int baseDefense) { this.baseDefense = (int)Math.floor(baseDefense*2*level/100)+5; }
    public void setBaseSpecialAttack(int baseSpecialAttack) { this.baseSpecialAttack = (int)Math.floor(baseSpecialAttack*2*level/100)+5; }
    public void setBaseSpecialDefense(int baseSpecialDefense) { this.baseSpecialDefense = (int)Math.floor(baseSpecialDefense*2*level/100)+5; }

    public void setName(String name) { this.name = name; }
    public void setLevel(int level) { this.level = level; }
    public void setExperience(int experience) { this.experience = experience; }
    public void setExperienceGained(int experienceGained) { this.experienceGained = experienceGained; }
    public void setMonsterNumber(int monsterNumber) { this.monsterNumber = monsterNumber; }
    public void setType1(int type1) { this.type1 = type1; }
    public void setType2(int type2) { this.type2 = type2; }

    public int getHp() { return hp; }
    public int getSpeed() { return speed; }
    public int getAttack() { return attack; }
    public int getDefense() { return defense; }
    public int getSpecialAttack() { return specialAttack; }
    public int getSpecialDefense() { return specialDefense; }
    public int getEvasion() { return evasion; }
    public int getAccuracy() { return accuracy; }
    public int getBaseHp() { return baseHp; }
    public int getBaseSpeed() { return baseSpeed; }
    public int getBaseAttack() { return baseAttack; }
    public int getBaseDefense() { return baseDefense; }
    public int getBaseSpecialAttack() { return baseSpecialAttack; }
    public int getBaseSpecialDefense() { return baseSpecialDefense; }
    public String getName() { return name; }
    public int getLevel() { return level; }
    public int getExperience() { return experience; }
    public int getExperienceGained() { return experienceGained; }
    public int getMonsterNumber() { return monsterNumber; }
    public int getType1() { return type1; }
    public int getType2() { return type2; }
    public Move getMove1() { return move1; }
    public Move getMove2() { return move2; }
    public Move getMove3() { return move3; }
    public Move getMove4() { return move4; }
    public boolean getFlinched() { return flinched; }
    public boolean getFrozen() { return frozen; }
    public boolean getParalyzed() { return paralyzed; }
    public boolean getConfused() { return confused; }
    public boolean getBurned() { return burned; }
    public boolean getAsleep() { return asleep; }
    public boolean getPoisoned() { return poisoned; }

    public void setMove(String move, int slot) {
        if (slot == 1) {
            move1 = new Move(move, this);
        } else if (slot == 2) {
            move2 = new Move(move, this);
        } else if (slot == 3) {
            move3 = new Move(move, this);
        } else if (slot == 4) {
            move4 = new Move(move, this);
        }
    }
}

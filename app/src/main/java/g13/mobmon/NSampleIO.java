package g13.mobmon;

public interface NSampleIO {
    //call to log in the app
    void log(String info);
    //to call when connected to nuggeta
    void connected();
}

package g13.mobmon;

import com.nuggeta.NuggetaGamePlug;
import com.nuggeta.api.NuggetaGameApi;
import com.nuggeta.game.core.ngdl.nobjects.NGame;

public abstract class NSample {
    public NGame game;
    public String gameId;
    protected NuggetaGamePlug nuggetaPlug;
    protected NuggetaGameApi gameApi;
    protected NSampleIO sampleIO;

    public NSample(String url) {
        nuggetaPlug = new NuggetaGamePlug(url);
    }

    public void onUpdate() {
        nuggetaPlug.pump();
        /*
        boolean gameRunning = true;
        while (gameRunning) {
            nuggetaPlug.signUp();
            List<Message> freshEvents = nuggetaPlug.pump();
            for (int i = 0; i < freshEvents.size(); i++) {
                Message event = freshEvents.get(i);
                if (event instanceof SignUpResponse){
                    SignUpResponse SignUpResponse= (SignUpResponse) event;
                    SignUpStatus signUpStatus = SignUpResponse.getSignUpStatus();
                    if(signUpStatus==SignUpStatus.ACCOUNT_CREATED){
                        log.info("Place your code here when ACCOUNT_CREATED");
                        return;
                    }
                    if(signUpStatus==SignUpStatus.INTERNAL_ERROR){
                        log.info("Place your code here when INTERNAL_ERROR");
                        return;
                    }
                    if(signUpStatus==SignUpStatus.CANCELED){
                        log.info("Place your code here when CANCELED");
                        return;
                    }
                }
            }
        }
        */
    }

    public void onPaused() {
        if (nuggetaPlug != null) {
            // nuggetaPlug.pauseSessionMonitoring();
        }
    }

    public void onResume() {
        if (nuggetaPlug != null) {
            // nuggetaPlug.resumeSessionMonitoring();
        }
    }

    public void onExit() {
        if (nuggetaPlug != null) {
            nuggetaPlug.stop();
        }
    }

    public abstract void run();

    public void setIo(NSampleIO sampleIO) {
        this.sampleIO = sampleIO;
    }

    protected void createGame(int minPlayers, int maxPlayers, String name) {
    }

    protected void findGames() {
    }

    protected void joinGame() {
    }

    protected void resumeGame() {
    }

    protected void sendGameMessage(String message) {
    }

    /*protected void sendMessage() {
        sampleIO.log("sendMessage");
        NRawGameMessage rawMessage = new NRawGameMessage();
        rawMessage.setContent("blabla");
        gameApi.sendMessage(rawMessage);
    }*/

    protected void sendPlayerMessage(String playerId, String message) {
    }

    protected void matchAndJoinGame() {
    }

    public void saveMultiplayerGame() {
    }

    public void loadMultiplayerGame(String id) {
    }

    public void customMatchmaking() {
    }

    public void updateGame() {
    }
}

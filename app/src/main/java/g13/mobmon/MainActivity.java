package g13.mobmon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.nuggeta.NuggetaAndroid;
import com.nuggeta.game.core.api.handlers.CreateGameResponseHandler;
import com.nuggeta.game.core.api.handlers.GetGamesResponseHandler;
import com.nuggeta.game.core.api.handlers.JoinGameResponseHandler;
import com.nuggeta.game.core.api.handlers.MatchAndJoinGameResponseHandler;
import com.nuggeta.game.core.api.handlers.NDisconnectedNotificationHandler;
import com.nuggeta.game.core.api.handlers.NMatchAndJoinGameExpiredNotificationHandler;
import com.nuggeta.game.core.api.handlers.NRawGameMessageHandler;
import com.nuggeta.game.core.api.handlers.NextPlayerTurnResponseHandler;
import com.nuggeta.game.core.api.handlers.PlayerEnterGameHandler;
import com.nuggeta.game.core.api.handlers.PlayerTurnNotificationHandler;
import com.nuggeta.game.core.api.handlers.PlayerUnjoinGameHandler;
import com.nuggeta.game.core.api.handlers.SendMessageResponseHandler;
import com.nuggeta.game.core.api.handlers.SendMessageToGameResponseHandler;
import com.nuggeta.game.core.api.handlers.SendMessageToPlayerResponseHandler;
import com.nuggeta.game.core.api.handlers.StartResponseHandler;
import com.nuggeta.game.core.ngdl.nobjects.CreateGameResponse;
import com.nuggeta.game.core.ngdl.nobjects.CreateGameStatus;
import com.nuggeta.game.core.ngdl.nobjects.GetGamesResponse;
import com.nuggeta.game.core.ngdl.nobjects.JoinGameResponse;
import com.nuggeta.game.core.ngdl.nobjects.JoinGameStatus;
import com.nuggeta.game.core.ngdl.nobjects.MatchAndJoinGameResponse;
import com.nuggeta.game.core.ngdl.nobjects.MatchAndJoinGameStatus;
import com.nuggeta.game.core.ngdl.nobjects.NDisconnectedNotification;
import com.nuggeta.game.core.ngdl.nobjects.NGame;
import com.nuggeta.game.core.ngdl.nobjects.NGameCharacteristics;
import com.nuggeta.game.core.ngdl.nobjects.NMatchAndJoinGameExpiredNotification;
import com.nuggeta.game.core.ngdl.nobjects.NRawGameMessage;
import com.nuggeta.game.core.ngdl.nobjects.NextPlayerTurnResponse;
import com.nuggeta.game.core.ngdl.nobjects.PlayerEnterGame;
import com.nuggeta.game.core.ngdl.nobjects.PlayerTurnNotification;
import com.nuggeta.game.core.ngdl.nobjects.PlayerUnjoinGame;
import com.nuggeta.game.core.ngdl.nobjects.SendMessageResponse;
import com.nuggeta.game.core.ngdl.nobjects.SendMessageToGameResponse;
import com.nuggeta.game.core.ngdl.nobjects.SendMessageToPlayerResponse;
import com.nuggeta.game.core.ngdl.nobjects.StartResponse;
import com.nuggeta.game.core.ngdl.nobjects.StartStatus;
import com.nuggeta.network.plug.ConnectionInterruptedListener;
import com.nuggeta.network.plug.ConnectionLostListener;
import com.nuggeta.ngdl.nobjects.NuggetaQuery;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity implements NSampleIO{
    private GameView theView;
    private NSample sample;
    private final static String GAMESERVER_URL = "104.196.0.162:5010";
    private Monster[] myTeam = new Monster[3], theirTeam = new Monster[3];
    private int munny = 0, xpLength, myHpLength, theirHpLength, detailsIterator = 0, currentMove = 1, heightOffset, widthOffset, tileSize, screenWidth, screenHeight, currentMenu, myCurrentMonster = 0, theirCurrentMonster = 0;
    private Bitmap fitmonCenter, heal, gym, wilderness, mart, martMonsterPotion, martMonsterCharm, dollar, connectedSign, disconnectedSign, myHpGreen, theirHpGreen, hpRed, xpBlue, xpYellow, electrosaurFront, electrosaurBack, beaverFront, beaverBack, dragonFront, dragonBack, frosqueakFront, frosqueakBack, ghostlyFront, ghostlyBack, medupsyFront, medupsyBack, redipsFront, redipsBack, rockyFront, rockyBack, sandslashFront, sandslashBack, shovelknightFront, shovelknightBack, subatFront, subatBack, venocatFront, venocatBack, battleInfo, battleBackground, moveEmber, moveVineWhip, moveCut, moveGrowl, moveLeer, movePound, moveScratch, moveSmokeScreen, moveTackle, moveTailWhip, moveBubble, moveSurf, moveBite, moveScreech, moveFlash, moveGrowth, moveRockSlide, moveRockThrow, moveBodySlam, moveDefenseCurl, moveConfuseRay, moveLick, moveNightShade, moveSing, moveAcid, movePoisonGas, moveSmog, moveSludge, moveLeechLife, movePinMissle, moveStringShot, moveTwineedle, moveAuroraBeam, moveBlizzard, moveIceBeam, moveHeadButt, charmouseFront, charmouseBack, beetishFront, beetishBack, pengyFront, pengyBack, selector, buttonConfirm, buttonCancel, myCurrentMonsterBitmap, theirCurrentMonsterBitmap, buttonFight, buttonItem, buttonSwap, buttonRun, upperA, upperB, upperC, upperD, upperE, upperF, upperG, upperH, upperI, upperJ, upperK, upperL, upperM, upperN, upperO, upperP, upperQ, upperR, upperS, upperT, upperU, upperV, upperW, upperX, upperY, upperZ, lowerA, lowerB, lowerC, lowerD, lowerE, lowerF, lowerG, lowerH, lowerI, lowerJ, lowerK, lowerL, lowerM, lowerN, lowerO, lowerP, lowerQ, lowerR, lowerS, lowerT, lowerU, lowerV, lowerW, lowerX, lowerY, lowerZ, zero, one, two, three, four, five, six, seven, eight, nine, mainMenu, buttonBack, buttonBattle, buttonMyTeam, buttonQuests, buttonVillage, buttonCenter, buttonMart, buttonGym, buttonWilderness, buttonSearch;
    private String details = "", healStat = "";
    private long curTime = 2000000, prevTime = 1, deltaTime = 0, deltaSum = 0;
    private boolean connected = false, aiMatch = true;
    private ArrayList<String> updateText, stepCount, money, healStatus;
    private ArrayList<Monster> savedMonsters;
    private ArrayList<Item> myItems;
    private Item monsterCharm;
    private Item monsterPotion;
    private Money monies;

    private String monsterFile = "monsters.data", itemFile = "items.data", stepsFile = "steps.data", moneyfile = "money.data";
    private SaveArrayList stepsArray, moneyArray;

    @Override
    public void log(final String info) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("g.13", "" + info);
            }
        });
    }

    @Override
    public void connected() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                log("connected()");
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        theView = new GameView(this);
        setContentView(theView);
        View decorView = getWindow().getDecorView();
        getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        stepsArray = new SaveArrayList();
        stepCount = (ArrayList<String>) stepsArray.retrieveFile(stepsFile, this.getApplicationContext());
        moneyArray = new SaveArrayList();
        money = moneyArray.retrieveFile(moneyfile,this.getApplicationContext());
        savedMonsters = new ArrayList();
        myItems = new ArrayList();

        startService(new Intent(this, PedometerService.class));
        monies = new Money();
        NuggetaAndroid.register(MainActivity.this);
        sample = new Nuggeta(GAMESERVER_URL);
        sample.setIo(this);
        sample.run();

        heightOffset = 0;
        widthOffset = 0;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        updateText = new ArrayList();
        healStatus = new ArrayList();

        //this detects if the aspect ratio is not 16x9. If it does then it determines which dimension is shorter and how big the borders will be.
        if (screenWidth /16 < screenHeight /9) {
            tileSize = screenWidth /16;
            heightOffset = (screenHeight - (tileSize*9))/2;
        } else if (screenWidth /16 > screenHeight /9) {
            tileSize = screenHeight /9;
            widthOffset = -(screenWidth - (tileSize*16))/2;
        } else if (screenWidth /16 == screenHeight /9) {
            tileSize = screenHeight /9;
        }

        log("screenHeight: " + screenHeight);
        log("screenWidth: " + screenWidth);
        log("widthOffset: " + widthOffset);
        log("heightOffset: " + heightOffset);
        log("tileSize: " + tileSize);

        loadImages();
        monsterCharm = new Item("Monster Charm","Allows you to capture a weakened Monster",50,0);
        monsterPotion = new Item("Monster Potion", "Heals an injured monster", 50, 20);
        myItems.add(monsterCharm);
        myItems.add(monsterCharm);
        myItems.add(monsterCharm);
        myItems.add(monsterPotion);

        String[] files = fileList();
        log("Files: " + files.length);

        //load normally
        currentMenu = 0;
        boolean hasMonster = false;
        for (String s : files) {
            //check for save file
            log("File: " + s);
            try {
                FileInputStream fis = openFileInput(s);
                if (s.equals("TEAM")) {
                    hasMonster = true;
                    String fileContent = "";
                    if (fis != null) {
                        InputStreamReader inputStreamReader = new InputStreamReader(fis);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        String receiveString = "";
                        StringBuilder stringBuilder = new StringBuilder();

                        while ((receiveString = bufferedReader.readLine()) != null) {
                            stringBuilder.append(receiveString);
                        }

                        fis.close();
                        fileContent = stringBuilder.toString();
                        log("fileContent: " + fileContent);
                        myTeam[0] = generateMonster(Character.getNumericValue(fileContent.charAt(0)), Character.getNumericValue(fileContent.charAt(1)));
                    }
                } else {
                    //Print a log about what the files name is, it's content, and then delete it.
                    log("s: " + s);
                    String ret = "";
                    if (fis != null) {
                        InputStreamReader inputStreamReader = new InputStreamReader(fis);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        String receiveString = "";
                        StringBuilder stringBuilder = new StringBuilder();

                        while ((receiveString = bufferedReader.readLine()) != null) {
                            stringBuilder.append(receiveString);
                        }

                        fis.close();
                        ret = stringBuilder.toString();
                        Log.d("g13.mobmon", "ret: " + ret);
                    }
                    //this.deleteFile(s);
                }
            } catch (Exception e) {
            }
        }
        if (!hasMonster) {
            currentMenu = 9;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        int X = (int) event.getX();
        int Y = (int) event.getY();
        int eventAction = event.getAction();

        if (currentMenu == 0) { //Main Menu
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (X > tileSize*3+widthOffset && X < tileSize*3+widthOffset+buttonBattle.getWidth()) {
                        if (Y > tileSize*3+heightOffset && Y < tileSize*3+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 1; // Battle
                            myCurrentMonster = 0;
                            theirCurrentMonster = 0;
                            detailsIterator = 0;
                            Random rand = new Random();
                            int randomNum = rand.nextInt(14);
                            theirTeam[0] = generateMonster(randomNum, myTeam[0].getLevel());

                            for (int i=0; i<myTeam.length; i++) {
                                if (myTeam[i] != null) {
                                    myHpGreen = Bitmap.createScaledBitmap(myHpGreen, upperA.getWidth() / 2 * 10, upperA.getWidth() / 2, false);
                                    theirHpGreen = Bitmap.createScaledBitmap(theirHpGreen, upperA.getWidth() / 2 * 10, upperA.getWidth() / 2, false);
                                    xpBlue = Bitmap.createScaledBitmap(xpBlue, upperA.getWidth() / 2 , upperA.getWidth() / 2, false);
                                    myTeam[i].setHp(myTeam[i].getHp());
                                    myTeam[i].setSpeed(myTeam[i].getBaseSpeed());
                                    myTeam[i].setAttack(myTeam[i].getBaseAttack());
                                    myTeam[i].setDefense(myTeam[i].getBaseDefense());
                                    myTeam[i].setSpecialAttack(myTeam[i].getBaseSpecialAttack());
                                    myTeam[i].setSpecialDefense(myTeam[i].getBaseSpecialDefense());
                                    myTeam[i].setAccuracy(100);
                                    myTeam[i].setEvasion(100);
                                    myTeam[i].setFlinched(false);
                                    myTeam[i].setFrozen(false);
                                    myTeam[i].setBurned(false);
                                    myTeam[i].setParalyzed(false);
                                    myTeam[i].setConfused(false);
                                    myTeam[i].setAsleep(false);
                                    myTeam[i].setPoisoned(false);
                                }
                            }
                            for (int i=0; i<theirTeam.length; i++) {
                                if (theirTeam[i] != null) {
                                    theirTeam[i].setHp(theirTeam[i].getBaseHp());
                                    theirTeam[i].setSpeed(theirTeam[i].getBaseSpeed());
                                    theirTeam[i].setAttack(theirTeam[i].getBaseAttack());
                                    theirTeam[i].setDefense(theirTeam[i].getBaseDefense());
                                    theirTeam[i].setSpecialAttack(theirTeam[i].getBaseSpecialAttack());
                                    theirTeam[i].setSpecialDefense(theirTeam[i].getBaseSpecialDefense());
                                    theirTeam[i].setAccuracy(100);
                                    theirTeam[i].setEvasion(100);
                                    theirTeam[i].setFlinched(false);
                                    theirTeam[i].setFrozen(false);
                                    theirTeam[i].setBurned(false);
                                    theirTeam[i].setParalyzed(false);
                                    theirTeam[i].setConfused(false);
                                    theirTeam[i].setAsleep(false);
                                    theirTeam[i].setPoisoned(false);
                                }
                            }
                            details = "A wild " + theirTeam[theirCurrentMonster].getName() + " has appeared";
                        } else if (Y > tileSize*6+heightOffset && Y < tileSize*6+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 2; //Quests
                        }
                    } else if (X > tileSize*9+widthOffset && X < tileSize*9+widthOffset+buttonBattle.getWidth()) {
                        if (Y > tileSize*3+heightOffset && Y < tileSize*3+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 3; //My Team
                        } else if (Y > tileSize*6+heightOffset && Y < tileSize*6+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 4; //Village
                            if(healStatus.size() > 0) {
                                healStatus.remove(0);
                                healStat = "";
                            }
                        }
                    }
                    break;
            }
        } else if (currentMenu == 1) { //Battle - MAIN
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (X > tileSize*0.5f+widthOffset && X < tileSize*0.5f+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //fight
                            currentMenu = 5;
                            currentMove = 1;
                        }
                    }
                    if (X > tileSize*3+widthOffset && X < tileSize*3+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //item
                            currentMenu = 6;
                        }
                    }
                    if (X > tileSize*5.5f+widthOffset && X < tileSize*5.5f+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //swap
                            currentMenu = 7;
                        }
                    }
                    if (X > tileSize*8+widthOffset && X < tileSize*8+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //run
                            currentMenu = 0;
                        }
                    }
                    break;
            }
        } else if (currentMenu == 2) { //Quests
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (X > 0+widthOffset && X < 0+widthOffset+buttonBattle.getWidth()) {
                        if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 0;
                        }
                    }
                    break;
            }
        } else if (currentMenu == 3) { //My Team
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (X > 0+widthOffset && X < 0+widthOffset+buttonBattle.getWidth()) {
                        if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 0;
                        }
                    }
                    break;
            }
        } else if (currentMenu == 4) { //Village
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (X > 0+widthOffset && X < 0+widthOffset+buttonBattle.getWidth()) {
                        if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 0;
                        }
                    }
                    if (X > tileSize*3+widthOffset && X < tileSize*3+widthOffset+buttonBattle.getWidth()) {
                        if (Y > tileSize*3+heightOffset && Y < tileSize*3+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 10; // Recovery Center

                        } else if (Y > tileSize*6+heightOffset && Y < tileSize*6+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 12; //Gym
                        }
                    } else if (X > tileSize*9+widthOffset && X < tileSize*9+widthOffset+buttonBattle.getWidth()) {
                        if (Y > tileSize*3+heightOffset && Y < tileSize*3+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 11; //Mart
                        } else if (Y > tileSize*6+heightOffset && Y < tileSize*6+heightOffset+buttonBattle.getHeight()) {
                            if (munny < 50) {
                                currentMenu = 13;
                            } else {
                                monies.spend(50);
                                Random rand = new Random();
                                int randomNum = rand.nextInt(2);
                                if (randomNum < 1) {
                                    currentMenu = 1;
                                    myCurrentMonster = 0;
                                    theirCurrentMonster = 0;
                                    detailsIterator = 0;
                                    randomNum = rand.nextInt(14);
                                    theirTeam[0] = generateMonster(randomNum, myTeam[0].getLevel());

                                    for (int i=0; i<myTeam.length; i++) {
                                        if (myTeam[i] != null) {
                                            myHpGreen = Bitmap.createScaledBitmap(myHpGreen, upperA.getWidth() / 2 * 10, upperA.getWidth() / 2, false);
                                            theirHpGreen = Bitmap.createScaledBitmap(theirHpGreen, upperA.getWidth() / 2 * 10, upperA.getWidth() / 2, false);
                                            xpBlue = Bitmap.createScaledBitmap(xpBlue, upperA.getWidth() / 2 , upperA.getWidth() / 2, false);

                                            myTeam[i].setSpeed(myTeam[i].getBaseSpeed());
                                            myTeam[i].setAttack(myTeam[i].getBaseAttack());
                                            myTeam[i].setDefense(myTeam[i].getBaseDefense());
                                            myTeam[i].setSpecialAttack(myTeam[i].getBaseSpecialAttack());
                                            myTeam[i].setSpecialDefense(myTeam[i].getBaseSpecialDefense());
                                            myTeam[i].setAccuracy(100);
                                            myTeam[i].setEvasion(100);
                                        }
                                    }
                                    for (int i=0; i<theirTeam.length; i++) {
                                        if (theirTeam[i] != null) {
                                            theirTeam[i].setSpeed(theirTeam[i].getBaseSpeed());
                                            theirTeam[i].setAttack(theirTeam[i].getBaseAttack());
                                            theirTeam[i].setDefense(theirTeam[i].getBaseDefense());
                                            theirTeam[i].setSpecialAttack(theirTeam[i].getBaseSpecialAttack());
                                            theirTeam[i].setSpecialDefense(theirTeam[i].getBaseSpecialDefense());
                                            theirTeam[i].setAccuracy(100);
                                            theirTeam[i].setEvasion(100);
                                        }
                                    }
                                } else {
                                    currentMenu = 13;
                                }
                            }
                        }
                    }
                    break;
            }
        } else if (currentMenu == 5) { //Battle - Fight
            switch (eventAction) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                    if (X > tileSize*0.5f+widthOffset && X < tileSize*0.5f+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            int mySpeed = myTeam[myCurrentMonster].getSpeed();
                            int theirSpeed = theirTeam[theirCurrentMonster].getSpeed();

                            if (myTeam[myCurrentMonster].getConfused() == true) {
                                if (myTeam[myCurrentMonster].runConfusedCounter()) {
                                    myTeam[myCurrentMonster].setConfused(false);
                                    updateText.add(myTeam[myCurrentMonster].getName() + " is no longer confused!");
                                }
                            }
                            if (theirTeam[theirCurrentMonster].getConfused() == true) {
                                if (theirTeam[theirCurrentMonster].runConfusedCounter()) {
                                    theirTeam[theirCurrentMonster].setConfused(false);
                                    updateText.add(theirTeam[theirCurrentMonster].getName() + " is no longer confused!");
                                }
                            }

                            if (myTeam[myCurrentMonster].getAsleep() == true) {
                                if (myTeam[myCurrentMonster].runSleepCounter()) {
                                    myTeam[myCurrentMonster].setAsleep(false);
                                    updateText.add(myTeam[myCurrentMonster].getName() + " is no longer Asleep!");
                                }
                            }
                            if (theirTeam[theirCurrentMonster].getAsleep() == true) {
                                if (theirTeam[theirCurrentMonster].runSleepCounter()) {
                                    theirTeam[theirCurrentMonster].setAsleep(false);
                                    updateText.add(theirTeam[theirCurrentMonster].getName() + " is no longer Asleep!");
                                }
                            }

                            if (myTeam[myCurrentMonster].getParalyzed() == true) {
                                mySpeed = mySpeed/4;
                            }
                            if (theirTeam[theirCurrentMonster].getParalyzed() == true) {
                                theirSpeed = theirSpeed/4;
                            }

                            Random rand = new Random();
                            int randomNum;
                            if (myTeam[myCurrentMonster].getFrozen() == true) {
                                randomNum = rand.nextInt(10);
                                if (randomNum < 2) {
                                    updateText.add(myTeam[myCurrentMonster].getName() + " has thawed!");
                                    myTeam[myCurrentMonster].setFrozen(false);
                                }
                            }
                            if (theirTeam[theirCurrentMonster].getFrozen() == true) {
                                randomNum = rand.nextInt(10);
                                if (randomNum < 2) {
                                    updateText.add(theirTeam[theirCurrentMonster].getName() + " has thawed!");
                                    theirTeam[theirCurrentMonster].setFrozen(false);
                                }
                            }
                            if (mySpeed >= theirSpeed) {
                                detailsIterator = 0;
                                if (currentMove == 1) {
                                    updateText.addAll(myTeam[myCurrentMonster].getMove1().performMove(theirTeam[theirCurrentMonster]));
                                } else if (currentMove == 2) {
                                    updateText.addAll(myTeam[myCurrentMonster].getMove2().performMove(theirTeam[theirCurrentMonster]));
                                } else if (currentMove == 3) {
                                    updateText.addAll(myTeam[myCurrentMonster].getMove3().performMove(theirTeam[theirCurrentMonster]));
                                } else if (currentMove == 4) {
                                    updateText.addAll(myTeam[myCurrentMonster].getMove4().performMove(theirTeam[theirCurrentMonster]));
                                }
                                if (myTeam[myCurrentMonster].getBurned()) {
                                    myTeam[myCurrentMonster].setHp(myTeam[myCurrentMonster].getHp() - myTeam[myCurrentMonster].getBaseHp()/8);
                                    updateText.add(myTeam[myCurrentMonster].getName() + " took " + myTeam[myCurrentMonster].getBaseHp()/8 + " burn damge!");
                                }
                                if (myTeam[myCurrentMonster].getPoisoned()) {
                                    myTeam[myCurrentMonster].setHp(myTeam[myCurrentMonster].getHp() - myTeam[myCurrentMonster].getBaseHp()/8);
                                    updateText.add(myTeam[myCurrentMonster].getName() + " took " + myTeam[myCurrentMonster].getBaseHp()/8 + " burn damge!");
                                }
                                detailsIterator = 0;
                                if (theirTeam[theirCurrentMonster].getHp() > 0 && myTeam[myCurrentMonster].getHp() > 0) {
                                    //if current opponent is AI, also perform his move. for right now, just make it random.
                                    if (theirTeam[theirCurrentMonster].getFlinched() == false) {
                                        if (aiMatch) {
                                            randomNum = 0;
                                            randomNum = rand.nextInt(4)+1;
                                            if (randomNum == 1) {
                                                updateText.addAll(theirTeam[theirCurrentMonster].getMove1().performMove(myTeam[myCurrentMonster]));
                                            } else if (randomNum == 2) {
                                                updateText.addAll(theirTeam[theirCurrentMonster].getMove2().performMove(myTeam[myCurrentMonster]));
                                            } else if (randomNum == 3) {
                                                updateText.addAll(theirTeam[theirCurrentMonster].getMove3().performMove(myTeam[myCurrentMonster]));
                                            } else if (randomNum == 4) {
                                                updateText.addAll(theirTeam[theirCurrentMonster].getMove4().performMove(myTeam[myCurrentMonster]));
                                            }
                                        }
                                    } else {
                                        theirTeam[theirCurrentMonster].setFlinched(false);
                                    }
                                    if (theirTeam[theirCurrentMonster].getBurned()) {
                                        theirTeam[theirCurrentMonster].setHp(theirTeam[theirCurrentMonster].getHp() - theirTeam[theirCurrentMonster].getBaseHp()/8);
                                        updateText.add(theirTeam[theirCurrentMonster].getName() + " took " + theirTeam[theirCurrentMonster].getBaseHp()/8 + " burn damge!");
                                    }
                                    if (theirTeam[theirCurrentMonster].getPoisoned()) {
                                        theirTeam[theirCurrentMonster].setHp(theirTeam[theirCurrentMonster].getHp() - theirTeam[theirCurrentMonster].getBaseHp()/8);
                                        updateText.add(theirTeam[theirCurrentMonster].getName() + " took " + theirTeam[theirCurrentMonster].getBaseHp() / 8 + " burn damge!");
                                    }
                                }
                            } else {
                                //if current opponent is AI, also perform his move. for right now, just make it random.
                                if (aiMatch) {
                                    randomNum = 0;
                                    randomNum = rand.nextInt(4)+1;
                                    if (randomNum == 1) {
                                        updateText.addAll(theirTeam[theirCurrentMonster].getMove1().performMove(myTeam[myCurrentMonster]));
                                    } else if (randomNum == 2) {
                                        updateText.addAll(theirTeam[theirCurrentMonster].getMove2().performMove(myTeam[myCurrentMonster]));
                                    } else if (randomNum == 3) {
                                        updateText.addAll(theirTeam[theirCurrentMonster].getMove3().performMove(myTeam[myCurrentMonster]));
                                    } else if (randomNum == 4) {
                                        updateText.addAll(theirTeam[theirCurrentMonster].getMove4().performMove(myTeam[myCurrentMonster]));
                                    }
                                }
                                if (theirTeam[theirCurrentMonster].getBurned()) {
                                    theirTeam[theirCurrentMonster].setHp(theirTeam[theirCurrentMonster].getHp() - theirTeam[theirCurrentMonster].getBaseHp()/8);
                                    updateText.add(theirTeam[theirCurrentMonster].getName() + " took " + theirTeam[theirCurrentMonster].getBaseHp()/8 + " burn damge!");
                                }
                                if (theirTeam[theirCurrentMonster].getPoisoned()) {
                                    theirTeam[theirCurrentMonster].setHp(theirTeam[theirCurrentMonster].getHp() - theirTeam[theirCurrentMonster].getBaseHp()/8);
                                    updateText.add(theirTeam[theirCurrentMonster].getName() + " took " + theirTeam[theirCurrentMonster].getBaseHp()/8 + " burn damge!");
                                }
                                detailsIterator = 0;
                                if (myTeam[myCurrentMonster].getFlinched() == false) {
                                    if (myTeam[myCurrentMonster].getHp() > 0 && theirTeam[theirCurrentMonster].getHp() > 0) {
                                        if (currentMove == 1) {
                                            updateText.addAll(myTeam[myCurrentMonster].getMove1().performMove(theirTeam[theirCurrentMonster]));
                                        } else if (currentMove == 2) {
                                            updateText.addAll(myTeam[myCurrentMonster].getMove2().performMove(theirTeam[theirCurrentMonster]));
                                        } else if (currentMove == 3) {
                                            updateText.addAll(myTeam[myCurrentMonster].getMove3().performMove(theirTeam[theirCurrentMonster]));
                                        } else if (currentMove == 4) {
                                            updateText.addAll(myTeam[myCurrentMonster].getMove4().performMove(theirTeam[theirCurrentMonster]));
                                        }
                                    }
                                } else {
                                    myTeam[myCurrentMonster].setFlinched(false);
                                }
                                if (myTeam[myCurrentMonster].getBurned()) {
                                    myTeam[myCurrentMonster].setHp(myTeam[myCurrentMonster].getHp() - myTeam[myCurrentMonster].getBaseHp()/8);
                                    updateText.add(myTeam[myCurrentMonster].getName() + " took " + myTeam[myCurrentMonster].getBaseHp()/8 + " burn damge!");
                                }
                                if (myTeam[myCurrentMonster].getPoisoned()) {
                                    myTeam[myCurrentMonster].setHp(myTeam[myCurrentMonster].getHp() - myTeam[myCurrentMonster].getBaseHp()/8);
                                    updateText.add(myTeam[myCurrentMonster].getName() + " took " + myTeam[myCurrentMonster].getBaseHp()/8 + " burn damge!");
                                }
                            }

                            int myTeamTotalCount = 0;
                            int myTeamKOdCount = 0;
                            for (int i=0; i<myTeam.length;i++) {
                                if (myTeam[i] != null) {
                                    myTeamTotalCount++;
                                    if (myTeam[i].getHp() <= 0) {
                                        myTeamKOdCount++;
                                    }
                                }
                            }
                            //log("myTeamKOdCount: " + myTeamKOdCount);
                            //log("myTeamTotalCount: " + myTeamTotalCount);
                            if (myTeamKOdCount == myTeamTotalCount) {
                                updateText.add("You Lose...");
                            }

                            int theirTeamTotalCount = 0;
                            int theirTeamKOdCount = 0;
                            for (int i=0; i<theirTeam.length;i++) {
                                if (theirTeam[i] != null) {
                                    theirTeamTotalCount++;
                                    if (theirTeam[i].getHp() <= 0) {
                                        theirTeamKOdCount++;
                                    }
                                }
                            }
                            //log("theirTeamKOdCount: " + theirTeamKOdCount);
                            //log("theirTeamTotalCount: " + theirTeamTotalCount);
                            if (theirTeamKOdCount == theirTeamTotalCount) {
                                updateText.add("You Win!");
                                for (int i=0; i<myTeam.length; i++) {
                                    if (myTeam[i] != null) {
                                        myTeam[i].setExperience(myTeam[i].getExperience() + myTeam[i].getLevel()*2);
                                        Log.d("g13.mobmon", "xp: " + myTeam[i].getExperience());
                                        if (myTeam[i].getExperience() > myTeam[i].getLevel()*5) {
                                            myTeam[i].setLevel(myTeam[i].getLevel() + 1);
                                            myTeam[i].setBaseHp(myTeam[i].getBaseHp());
                                            myTeam[i].setBaseSpeed(myTeam[i].getBaseSpeed());
                                            myTeam[i].setBaseAttack(myTeam[i].getBaseAttack());
                                            myTeam[i].setBaseDefense(myTeam[i].getBaseDefense());
                                            myTeam[i].setBaseSpecialAttack(myTeam[i].getBaseSpecialAttack());
                                            myTeam[i].setBaseSpecialDefense(myTeam[i].getBaseSpecialDefense());
                                            //updateText.add(myTeam[i].getName() + " leveled up!");
                                            monies.setAmount(munny+200);
                                        }
                                    }
                                }
                                //get XP, level up, $
                            }
                            currentMenu = 8;
                            myHpLength = (int) ((float)((float)myTeam[myCurrentMonster].getHp()/(float)myTeam[myCurrentMonster].getBaseHp())*10);
                            if (myHpLength == 0) {
                                myHpLength =  1;
                            }
                            myHpGreen = Bitmap.createScaledBitmap(myHpGreen, upperA.getWidth() / 2 * myHpLength, upperA.getWidth() / 2, false);

                            theirHpLength =(int) ((float)((float)theirTeam[theirCurrentMonster].getHp()/(float)theirTeam[theirCurrentMonster].getBaseHp())*10);
                            if (theirHpLength == 0) {
                                theirHpLength =  1;
                            }
                            theirHpGreen = Bitmap.createScaledBitmap(theirHpGreen, upperA.getWidth() / 2 * theirHpLength, upperA.getWidth() / 2, false);

                            xpLength = (int) ((float)((float)myTeam[myCurrentMonster].getExperience()/(float)myTeam[myCurrentMonster].getLevel()*5));
                            log("myTeam[myCurrentMonster].getExperience(): " + myTeam[myCurrentMonster].getExperience());
                            log("myTeam[myCurrentMonster].getLevel()*5: " + myTeam[myCurrentMonster].getLevel()*5);
                            log("((float)((float)myTeam[myCurrentMonster].getExperience()/(float)myTeam[myCurrentMonster].getLevel()*5)*10): " + ((float)((float)myTeam[myCurrentMonster].getExperience()/(float)myTeam[myCurrentMonster].getLevel()*5)));
                            if (xpLength == 0) {
                                xpLength =  1;
                            }
                            xpBlue = Bitmap.createScaledBitmap(xpBlue, upperA.getWidth() / 2 * xpLength, upperA.getWidth() / 2, false);

                        }
                    }
                    if (X > tileSize*3+widthOffset && X < tileSize*3+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //move 1
                            currentMove = 1;
                        }
                    }
                    if (X > tileSize*5.5f+widthOffset && X < tileSize*5.5f+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //move 2
                            currentMove = 2;
                        }
                    }
                    if (X > tileSize*8+widthOffset && X < tileSize*8+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //move 3
                            currentMove = 3;
                        }
                    }
                    if (X > tileSize*10.5f+widthOffset && X < tileSize*10.5f+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //move 4
                            currentMove = 4;
                        }
                    }
                    if (X > tileSize*13+widthOffset && X < tileSize*13+widthOffset+buttonFight.getWidth()) {
                        if (Y > tileSize*7+heightOffset && Y < tileSize*7+heightOffset+buttonFight.getHeight()) {
                            //cancel
                            currentMenu = 1;
                        }
                    }
                    break;
            }
        } else if (currentMenu == 6) { //Battle - Item
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (X > 0+widthOffset && X < 0+widthOffset+buttonBack.getWidth()) {
                        if (Y > 0 + heightOffset && Y < 0 + heightOffset + buttonBack.getHeight()) {
                            currentMenu = 1;
                        }
                    }

                    if((Y > (tileSize*3)+heightOffset-900) && (Y < ((tileSize*4)+heightOffset+250))){
                        boolean canUsePotion = false;
                        for (int i=0; i<myItems.size(); i++) {
                            if (myItems.get(i).getName().equals("Monster Potion"));
                            canUsePotion = true;
                        }
                        //Code below is used for the monsterPotion
                        if(canUsePotion){
                            currentMenu=8;
                            for (int i=0; i<myItems.size(); i++) {
                                if (myItems.get(i).getName().equals("Monster Potion")) ;
                                canUsePotion = true;
                            }
                            if(myTeam[myCurrentMonster].getHp() >= 1){
                                /*
                                for(int i=0; i<myItems.size(); i++){
                                    if (myItems.get(i).getName().equals("Monster Potion")) {
                                        myItems.remove(i);
                                        break;
                                    }
                                }
                                */
                                int heal = ((myTeam[myCurrentMonster].getHp())+20);
                                if((((myTeam[myCurrentMonster].getHp())+20) > (myTeam[myCurrentMonster].getBaseHp())) && ((myTeam[myCurrentMonster].getHp()) < (myTeam[myCurrentMonster].getBaseHp()))) {
                                    myTeam[myCurrentMonster].setHp(myTeam[myCurrentMonster].getBaseHp());
                                    updateText.add("You healed your monster!");
                                    myItems.remove(monsterPotion);
                                }else if(((myTeam[myCurrentMonster].getHp())+20) < (myTeam[myCurrentMonster].getBaseHp())){
                                    myTeam[myCurrentMonster].setHp(heal);
                                    updateText.add("You healed your monster by 20!");
                                    myItems.remove(monsterPotion);
                                }else if(((myTeam[myCurrentMonster].getHp())) == (myTeam[myCurrentMonster].getBaseHp())){
                                    updateText.add("You monster has full health!");
                                }
                                else {
                                    myTeam[myCurrentMonster].setHp(heal);
                                    updateText.add("You healed your monster by 20!");
                                }
                            }
                        }
                    }

                    if (Y > (tileSize*3)+heightOffset && Y < (tileSize*4)+heightOffset) {
                        boolean canUseCharm = false;
                        for (int i=0; i<myItems.size(); i++) {
                            if (myItems.get(i).getName().equals("Monster Charm"));
                            canUseCharm = true;
                        }

                        //Code below is used for the monsterCharm
                        Random rand = new Random();
                        int randomNumber = rand.nextInt(2);
                        if (canUseCharm) {
                            currentMenu = 8;
                            if (theirTeam[theirCurrentMonster].getHp() < theirTeam[theirCurrentMonster].getBaseHp()/2) {
                                if (randomNumber < 1) {
                                    updateText.add("You successfully charmed the Monster");
                                    for (int i=0; i<myItems.size(); i++) {
                                        if (myItems.get(i).getName().equals("Monster Charm")) {
                                            myItems.remove(i);
                                            break;
                                        }
                                    }
                                    if (myTeam[1] == null) {
                                        myTeam[1] = theirTeam[0];
                                    } else if (myTeam[2] == null) {
                                        myTeam[2] = theirTeam[0];
                                    } else {
                                        savedMonsters.add(theirTeam[0]);
                                    }
                                    updateText.add("You Win!");
                                } else {
                                    updateText.add("You failed to charm the Monster");
                                    for (int i=0; i<myItems.size(); i++) {
                                        if (myItems.get(i).getName().equals("Monster Charm")) {
                                            myItems.remove(i);
                                            break;
                                        }
                                    }
                                }
                            } else {
                                updateText.add("You failed to charm the Monster");
                                for (int i=0; i<myItems.size(); i++) {
                                    if (myItems.get(i).getName().equals("Monster Charm")) {
                                        myItems.remove(i);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        } else if (currentMenu == 7) { //Battle - Swap
            if (X > 0+widthOffset && X < 0+widthOffset+buttonBack.getWidth()) {
                if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBack.getHeight()) {
                    currentMenu = 1;
                }
            }
            if (Y > (tileSize*2)+heightOffset && Y < (tileSize*3)+heightOffset) {
                currentMenu = 1;
                myCurrentMonster = 0;
            }
            if (Y > (tileSize*3)+heightOffset && Y < (tileSize*4)+heightOffset) {
                currentMenu = 1;
                myCurrentMonster = 1;
            }
            if (Y > (tileSize*4)+heightOffset && Y < (tileSize*5)+heightOffset) {
                currentMenu = 1;
                myCurrentMonster = 2;
            }
        } else if (currentMenu == 8) { //Battle - Waiting, Text
            if (detailsIterator >= details.length()) {
                detailsIterator = details.length();
                if (updateText.size() > 0) {
                    if (updateText.get(0).equals("You Win!") || updateText.get(0).equals("You Lose...")) {
                        updateText.remove(0);
                        detailsIterator = 0;
                        currentMenu = 0;
                    } else {
                        updateText.remove(0);
                        detailsIterator = 0;
                        if (updateText.size() == 0) {
                            details = "";
                            currentMenu = 1;
                        }
                    }
                } else {
                    details = "What will you do?";
                    detailsIterator = 0;
                    currentMenu = 1;
                }
            }
        } else if (currentMenu == 9) { //FIRST TIME PICK YOUR MONSTER SCREEN
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (Y > tileSize*3 && Y < tileSize*4) {
                        String FILENAME = "TEAM";
                        String string = "01";
                        try {
                            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                            fos.write(string.getBytes());
                            fos.close();
                        } catch (Exception e) {
                        }
                        myTeam[0] = generateMonster(0, 1);
                        currentMenu = 0;
                    }
                    if (Y > tileSize*5 && Y < tileSize*6) {
                        String FILENAME = "TEAM";
                        String string = "11";
                        try {
                            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                            fos.write(string.getBytes());
                            fos.close();
                        } catch (Exception e) {
                        }
                        myTeam[0] = generateMonster(1, 1);
                        currentMenu = 0;
                    }
                    if (Y > tileSize*7 && Y < tileSize*8) {
                        String FILENAME = "TEAM";
                        String string = "21";
                        try {
                            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                            fos.write(string.getBytes());
                            fos.close();
                        } catch (Exception e) {
                        }
                        myTeam[0] = generateMonster(2, 1);
                        currentMenu = 0;
                    }
                    break;
            }
        } else if (currentMenu == 10) { //Recovery Center
            if (X > 0+widthOffset && X < 0+widthOffset+buttonBattle.getWidth()) {
                if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBattle.getHeight()) {

                    currentMenu = 0;
                }
            }
            if(X > tileSize*5+widthOffset && X < tileSize*5+widthOffset+buttonBattle.getWidth()){
                if (Y > tileSize*5+heightOffset && Y < tileSize*5+heightOffset+buttonBattle.getHeight()) {
                    if (munny < 1000) {
                        healStatus.add("You do not have enough munny");
                    } else {
                        monies.setAmount(munny - 1000);
                        for (int i=0; i<myTeam.length; i++) {
                            if (myTeam[i] != null) {
                                myTeam[i].setHp(myTeam[i].getBaseHp());
                                myTeam[i].setSpeed(myTeam[i].getBaseSpeed());
                                myTeam[i].setAttack(myTeam[i].getBaseAttack());
                                myTeam[i].setDefense(myTeam[i].getBaseDefense());
                                myTeam[i].setSpecialAttack(myTeam[i].getBaseSpecialAttack());
                                myTeam[i].setSpecialDefense(myTeam[i].getBaseSpecialDefense());
                                myTeam[i].setAccuracy(100);
                                myTeam[i].setEvasion(100);
                                myTeam[i].setFlinched(false);
                                myTeam[i].setFrozen(false);
                                myTeam[i].setBurned(false);
                                myTeam[i].setParalyzed(false);
                                myTeam[i].setConfused(false);
                                myTeam[i].setAsleep(false);
                                myTeam[i].setPoisoned(false);
                            }
                        }
                        healStatus.add("All Monsters have been heals");
                    }
                    currentMenu = 10;
                }
            }
        } else if (currentMenu == 11) { //Mart
            if (X > 0+widthOffset && X < 0+widthOffset+buttonBattle.getWidth()) {
                if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBattle.getHeight()) {
                    currentMenu = 0;
                }
            }
            //Tyler was here
            if(X > tileSize*3+widthOffset && X < tileSize*3+widthOffset+buttonBattle.getWidth()) {
                if (Y > tileSize * 3 + heightOffset && Y < tileSize * 3 + heightOffset + buttonBattle.getHeight()) {
                    if(munny < 50){ //print text saying not enough munnies
                    }
                    else{
                        myItems.add(monsterPotion); //print text saying you bought one, ref to recovery section.
                        monies.setAmount(munny-50);
                    }
                }
            }

            if(X > tileSize*3+widthOffset && X < tileSize*3+widthOffset+buttonBattle.getWidth()) {
                if (Y > tileSize * 6 + heightOffset && Y < tileSize * 6 + heightOffset + buttonBattle.getHeight()) {
                    if(munny < 50){
                    }
                    else{
                        myItems.add(monsterCharm);
                        monies.setAmount(munny-50);
                    }
                }
            }
        } else if (currentMenu == 12) { //Gym
            if (X > 0+widthOffset && X < 0+widthOffset+buttonBattle.getWidth()) {
                if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBattle.getHeight()) {
                    currentMenu = 0;
                }
            }
        } else if (currentMenu == 13) { //wilderness
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (X > 0+widthOffset && X < 0+widthOffset+buttonBattle.getWidth()) {
                        if (Y > 0+heightOffset && Y < 0+heightOffset+buttonBattle.getHeight()) {
                            currentMenu = 0;
                        }
                    }
                    break;
            }
        }
        return true;
    }

    class GameView extends View {
        public GameView(Context context) {
            super(context);
        }
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            if (animationMath()) {
                //returns true 30 times a second (30 fps)
                if (detailsIterator < details.length()) {
                    detailsIterator++;
                }
            }
            if (currentMenu == 0) { //Title
                munny = monies.getCurrAmount();
                canvas.drawBitmap(mainMenu, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonBattle, tileSize*3+widthOffset, tileSize*3+heightOffset, null);
                canvas.drawBitmap(buttonMyTeam, tileSize*9+widthOffset, tileSize*3+heightOffset, null);
                canvas.drawBitmap(buttonQuests, tileSize*3+widthOffset, tileSize*6+heightOffset, null);
                canvas.drawBitmap(buttonVillage, tileSize * 9 + widthOffset, tileSize * 6 + heightOffset, null);
                if (connected) {
                    canvas.drawBitmap(connectedSign, screenWidth-connectedSign.getWidth()+widthOffset, 0+heightOffset, null);
                } else {
                    canvas.drawBitmap(disconnectedSign, screenWidth-connectedSign.getWidth()+widthOffset, 0+heightOffset, null);
                }
                String sentenceString = "Munny: " + munny;
                ArrayList<Bitmap> sentenceBitmaps = getSentence(sentenceString);
                int sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset, 0+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
            } else if (currentMenu == 1) { //Battle
                canvas.drawBitmap(battleBackground, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(battleInfo, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonFight, tileSize*0.5f+widthOffset, tileSize*7+heightOffset, null);
                canvas.drawBitmap(buttonItem, tileSize*3+widthOffset, tileSize*7+heightOffset, null);
                canvas.drawBitmap(buttonSwap, tileSize*5.5f+widthOffset, tileSize*7+heightOffset, null);
                canvas.drawBitmap(buttonRun, tileSize*8+widthOffset, tileSize*7+heightOffset, null);

                switch(myTeam[myCurrentMonster].getMonsterNumber()) {
                    case 0:
                        myCurrentMonsterBitmap = beetishBack;
                        break;
                    case 1:
                        myCurrentMonsterBitmap = pengyBack;
                        break;
                    case 2:
                        myCurrentMonsterBitmap = charmouseBack;
                        break;
                    case 3:
                        myCurrentMonsterBitmap = beaverBack;
                        break;
                    case 4:
                        myCurrentMonsterBitmap = dragonBack;
                        break;
                    case 5:
                        myCurrentMonsterBitmap = shovelknightBack;
                        break;
                    case 6:
                        myCurrentMonsterBitmap = frosqueakBack;
                        break;
                    case 7:
                        myCurrentMonsterBitmap = ghostlyBack;
                        break;
                    case 8:
                        myCurrentMonsterBitmap = medupsyBack;
                        break;
                    case 9:
                        myCurrentMonsterBitmap = redipsBack;
                        break;
                    case 10:
                        myCurrentMonsterBitmap = rockyBack;
                        break;
                    case 11:
                        myCurrentMonsterBitmap = sandslashBack;
                        break;
                    case 12:
                        myCurrentMonsterBitmap = subatBack;
                        break;
                    case 13:
                        myCurrentMonsterBitmap = venocatBack;
                        break;
                    case 14:
                        myCurrentMonsterBitmap = electrosaurBack;
                        break;
                }
                switch(theirTeam[theirCurrentMonster].getMonsterNumber()) {
                    case 0:
                        theirCurrentMonsterBitmap = beetishFront;
                        break;
                    case 1:
                        theirCurrentMonsterBitmap = pengyFront;
                        break;
                    case 2:
                        theirCurrentMonsterBitmap = charmouseFront;
                        break;
                    case 3:
                        theirCurrentMonsterBitmap = beaverFront;
                        break;
                    case 4:
                        theirCurrentMonsterBitmap = dragonFront;
                        break;
                    case 5:
                        theirCurrentMonsterBitmap = shovelknightFront;
                        break;
                    case 6:
                        theirCurrentMonsterBitmap = frosqueakFront;
                        break;
                    case 7:
                        theirCurrentMonsterBitmap = ghostlyFront;
                        break;
                    case 8:
                        theirCurrentMonsterBitmap = medupsyFront;
                        break;
                    case 9:
                        theirCurrentMonsterBitmap = redipsFront;
                        break;
                    case 10:
                        theirCurrentMonsterBitmap = rockyFront;
                        break;
                    case 11:
                        theirCurrentMonsterBitmap = sandslashFront;
                        break;
                    case 12:
                        theirCurrentMonsterBitmap = subatFront;
                        break;
                    case 13:
                        theirCurrentMonsterBitmap = venocatFront;
                        break;
                    case 14:
                        myCurrentMonsterBitmap = electrosaurFront;
                        break;
                }

                canvas.drawBitmap(myCurrentMonsterBitmap, 0+widthOffset, tileSize*2+heightOffset, null);

                ArrayList<Bitmap> sentenceBitmaps = getSentence(details);
                int sentenceOffset = 0;
                for (int i = 0; i < detailsIterator; i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset, screenHeight-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                String sentenceString = myTeam[myCurrentMonster].getName() + " Lv " + myTeam[myCurrentMonster].getLevel();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), (screenWidth/2)+sentenceOffset+widthOffset, tileSize*5+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceString = "HP " + myTeam[myCurrentMonster].getHp() + "/" + myTeam[myCurrentMonster].getBaseHp();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), (screenWidth/2)+sentenceOffset+widthOffset, tileSize*6+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                canvas.drawBitmap(theirCurrentMonsterBitmap, screenWidth-theirCurrentMonsterBitmap.getWidth()+widthOffset, 0+heightOffset, null);
                sentenceString = theirTeam[theirCurrentMonster].getName() + " Lv " + theirTeam[theirCurrentMonster].getLevel();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, 0+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceString = "HP " + theirTeam[theirCurrentMonster].getHp() + "/" + theirTeam[theirCurrentMonster].getBaseHp();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, tileSize+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                canvas.drawBitmap(hpRed,(screenWidth/2)+widthOffset, tileSize*6+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(myHpGreen,(screenWidth/2)+widthOffset, tileSize*6+upperA.getHeight()+heightOffset, null);
                //canvas.drawBitmap(xpYellow,(screenWidth/2)+widthOffset+upperA.getWidth()*6, tileSize*6+upperA.getHeight()+heightOffset, null);
                //canvas.drawBitmap(xpBlue,(screenWidth/2)+widthOffset+upperA.getWidth()*6, tileSize*6+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(hpRed,0+widthOffset, tileSize+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(theirHpGreen,0+widthOffset, tileSize+upperA.getHeight()+heightOffset, null);
            } else if (currentMenu == 2) { //Quests
                canvas.drawBitmap(mainMenu, 0 + widthOffset, 0 + heightOffset, null);
                canvas.drawBitmap(buttonBack, 0 + widthOffset, 0 + heightOffset, null);
                Paint paint = new Paint();
                paint.setColor(Color.MAGENTA);
                paint.setTextSize(100);
                boolean goalmet = false;
                if(PedometerService.getNumSteps().equals("1000")) {
                    ArrayList<Bitmap> sentenceBitmaps = getSentence("Completed!\n");
                    int sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset + 650, tileSize*3-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                else {
                    ArrayList<Bitmap> sentenceBitmaps = getSentence("Walk 1000 Steps \n");
                    int sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset + 650, tileSize*3-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                if(PedometerService.getNumSteps().equals("50000")) {
                    ArrayList<Bitmap> sentenceBitmaps = getSentence("completed \n");
                    goalmet = true;
                    int sentenceOffset = 0;
                    money.add(0,Integer.toString(Integer.parseInt(money.get(0)) + 50000));
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset + 650, tileSize*5-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                else {
                    ArrayList<Bitmap> sentenceBitmaps = getSentence("Walk 50000 steps \n");
                    int sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset + 650, tileSize*5-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                if(PedometerService.getNumSteps().equals("100000")) {
                    ArrayList<Bitmap> sentenceBitmaps = getSentence("Completed!\n");
                    goalmet=true;
                    int sentenceOffset = 0;
                    money.add(0,Integer.toString(Integer.parseInt(money.get(0)) + 100000));
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset + 650, tileSize*7-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                else {
                    ArrayList<Bitmap> sentenceBitmaps = getSentence("Walk 100000 steps \n");
                    int sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset + 650, tileSize*7-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                if(stepCount != null)canvas.drawText(Integer.toString(Integer.parseInt(stepCount.get(0))+ Integer.parseInt(PedometerService.getNumSteps())), theView.getHeight() / 2, theView.getWidth() / 2, paint);
                else{
                    canvas.drawText(PedometerService.getNumSteps(), theView.getHeight() / 2, theView.getWidth() / 2, paint);
                }

            } else if (currentMenu == 3) { //MyTeam
                //canvas.drawBitmap(mainMenu, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
                ArrayList<Bitmap> sentenceBitmaps = getSentence("My Team");
                int sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), buttonBack.getWidth()+sentenceOffset+widthOffset, (tileSize)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceBitmaps = getSentence(myTeam[0].getName());
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*2)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                if (myTeam[1] != null) {
                    sentenceBitmaps = getSentence(myTeam[1].getName());
                    sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*3)+heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                if (myTeam[2] != null) {
                    sentenceBitmaps = getSentence(myTeam[2].getName());
                    sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*4)+heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                sentenceBitmaps = getSentence("Monsters in storage");
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*5)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                for (int i=0; i<savedMonsters.size(); i++) {
                    sentenceBitmaps = getSentence(savedMonsters.get(i).getName());
                    sentenceOffset = 0;
                    for (int j = 0; j < sentenceBitmaps.size(); j++) {
                        if (sentenceBitmaps.get(j) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(j), 0+sentenceOffset+widthOffset, (tileSize*(i+5))+heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(j).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
            } else if (currentMenu == 4) { //Village
                canvas.drawBitmap(mainMenu, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonCenter, tileSize*3+widthOffset, tileSize*3+heightOffset, null);
                canvas.drawBitmap(buttonMart, tileSize*9+widthOffset, tileSize*3+heightOffset, null);
                canvas.drawBitmap(buttonGym, tileSize*3+widthOffset, tileSize*6+heightOffset, null);
                canvas.drawBitmap(buttonWilderness, tileSize*9+widthOffset, tileSize*6+heightOffset, null);
            } else if (currentMenu == 5) { //Battle - Fight
                canvas.drawBitmap(battleBackground, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(battleInfo, 0+widthOffset, 0+heightOffset, null);

                //confirm
                canvas.drawBitmap(buttonConfirm, tileSize*0.5f+widthOffset, tileSize*7+heightOffset, null);
                //Move1
                if (getMoveBitmap(1) != null) {
                    canvas.drawBitmap(getMoveBitmap(1), tileSize*3+widthOffset, tileSize*7+heightOffset, null);
                }
                //Move2
                if (getMoveBitmap(2) != null) {
                    canvas.drawBitmap(getMoveBitmap(2), tileSize*5.5f+widthOffset, tileSize*7+heightOffset, null);
                }
                //Move3
                if (getMoveBitmap(3) != null) {
                    canvas.drawBitmap(getMoveBitmap(3), tileSize*8+widthOffset, tileSize*7+heightOffset, null);
                }
                //move4
                if (getMoveBitmap(4) != null) {
                    canvas.drawBitmap(getMoveBitmap(4), tileSize*10.5f+widthOffset, tileSize*7+heightOffset, null);
                }
                //cancel
                canvas.drawBitmap(buttonCancel, tileSize*13+widthOffset, tileSize*7+heightOffset, null);

                switch(myTeam[myCurrentMonster].getMonsterNumber()) {
                    case 0:
                        myCurrentMonsterBitmap = beetishBack;
                        break;
                    case 1:
                        myCurrentMonsterBitmap = pengyBack;
                        break;
                    case 2:
                        myCurrentMonsterBitmap = charmouseBack;
                        break;
                }
                switch(theirTeam[theirCurrentMonster].getMonsterNumber()) {
                    case 0:
                        theirCurrentMonsterBitmap = beetishFront;
                        break;
                    case 1:
                        theirCurrentMonsterBitmap = pengyFront;
                        break;
                    case 2:
                        theirCurrentMonsterBitmap = charmouseFront;
                        break;
                }


                //write out what the description of move is, by default it is move 1
                canvas.drawBitmap(myCurrentMonsterBitmap, 0+widthOffset, tileSize*2+heightOffset, null);

                String sentenceString = null;
                if (currentMove == 1) {
                    sentenceString = myTeam[myCurrentMonster].getMove1().getDescription();
                    canvas.drawBitmap(selector, tileSize*3+widthOffset, tileSize*7+heightOffset, null);
                } else if (currentMove == 2) {
                    sentenceString = myTeam[myCurrentMonster].getMove2().getDescription();
                    canvas.drawBitmap(selector, tileSize*5.5f+widthOffset, tileSize*7+heightOffset, null);
                } else if (currentMove == 3) {
                    sentenceString = myTeam[myCurrentMonster].getMove3().getDescription();
                    canvas.drawBitmap(selector, tileSize*8+widthOffset, tileSize*7+heightOffset, null);
                } else if (currentMove == 4) {
                    sentenceString = myTeam[myCurrentMonster].getMove4().getDescription();
                    canvas.drawBitmap(selector, tileSize*10.5f+widthOffset, tileSize*7+heightOffset, null);
                }
                ArrayList<Bitmap> sentenceBitmaps = getSentence(sentenceString);
                int sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset, screenHeight-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceString = myTeam[myCurrentMonster].getName() + " Lv " + myTeam[myCurrentMonster].getLevel();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), (screenWidth/2)+sentenceOffset+widthOffset, tileSize*5+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceString = "HP " + myTeam[myCurrentMonster].getHp() + "/" + myTeam[myCurrentMonster].getBaseHp();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), (screenWidth/2)+sentenceOffset+widthOffset, tileSize*6+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }

                canvas.drawBitmap(theirCurrentMonsterBitmap, screenWidth-theirCurrentMonsterBitmap.getWidth()+widthOffset, 0+heightOffset, null);
                sentenceString = theirTeam[theirCurrentMonster].getName() + " Lv " + theirTeam[theirCurrentMonster].getLevel();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, 0+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceString = "HP " + theirTeam[theirCurrentMonster].getHp() + "/" + theirTeam[theirCurrentMonster].getBaseHp();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, tileSize+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                canvas.drawBitmap(hpRed,(screenWidth/2)+widthOffset, tileSize*6+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(myHpGreen,(screenWidth/2)+widthOffset, tileSize*6+upperA.getHeight()+heightOffset, null);
                //canvas.drawBitmap(xpYellow,(screenWidth/2)+widthOffset+upperA.getWidth()*6, tileSize*6+upperA.getHeight()+heightOffset, null);
                //canvas.drawBitmap(xpBlue,(screenWidth/2)+widthOffset+upperA.getWidth()*6, tileSize*6+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(hpRed,0+widthOffset, tileSize+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(theirHpGreen,0+widthOffset, tileSize+upperA.getHeight()+heightOffset, null);
            } else if (currentMenu == 6) { //Battle - Item
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
                int monsterCharm = 0;
                int monsterPotion = 0;
                for (int i=0; i<myItems.size(); i++) {
                    if (myItems.get(i).getName().equals("Monster Charm")) {
                        monsterCharm++;
                    }
                }

                for (int i=0; i<myItems.size(); i++) {
                    if (myItems.get(i).getName().equals("Monster Potion")){
                        monsterPotion++;
                    }
                }

                ArrayList<Bitmap> sentenceBitmaps = getSentence("MonsterCharm x" + monsterCharm);
                int sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*3)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceBitmaps = getSentence(" Monster Potion x" + monsterPotion);
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset-900, (tileSize*3)+heightOffset+250, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }

            } else if (currentMenu == 7) { //Battle - Swap
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
                ArrayList<Bitmap> sentenceBitmaps = getSentence(myTeam[0].getName());
                int sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*2)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                if (myTeam[1] != null) {
                    sentenceBitmaps = getSentence(myTeam[1].getName());
                    sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*3)+heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                if (myTeam[2] != null) {
                    sentenceBitmaps = getSentence(myTeam[2].getName());
                    sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*4)+heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
            } else if (currentMenu == 8) { //Battle - Waiting, Text
                canvas.drawBitmap(battleBackground, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(battleInfo, 0+widthOffset, 0+heightOffset, null);
                String sentenceString = null;
                if (updateText.size() > 0) {
                    details = updateText.get(0);
                }
                canvas.drawBitmap(myCurrentMonsterBitmap, 0+widthOffset, tileSize*2+heightOffset, null);
                ArrayList<Bitmap> sentenceBitmaps = getSentence(details);
                int sentenceOffset = 0;
                for (int i = 0; i < detailsIterator; i++) {
                    if (sentenceOffset > screenWidth) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset-screenWidth, screenHeight-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    } else {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset, screenHeight-sentenceBitmaps.get(i).getHeight()-tileSize-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
                }
                sentenceString = myTeam[myCurrentMonster].getName() + " Lv " + myTeam[myCurrentMonster].getLevel();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), (screenWidth/2)+sentenceOffset+widthOffset, tileSize*5+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceString = "HP " + myTeam[myCurrentMonster].getHp() + "/" + myTeam[myCurrentMonster].getBaseHp();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), (screenWidth/2)+sentenceOffset+widthOffset, tileSize*6+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }

                canvas.drawBitmap(theirCurrentMonsterBitmap, screenWidth-theirCurrentMonsterBitmap.getWidth()+widthOffset, 0+heightOffset, null);
                sentenceString = theirTeam[theirCurrentMonster].getName() + " Lv " + theirTeam[theirCurrentMonster].getLevel();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, 0+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceString = "HP " + theirTeam[theirCurrentMonster].getHp() + "/" + theirTeam[theirCurrentMonster].getBaseHp();
                sentenceBitmaps = getSentence(sentenceString);
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, tileSize+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                canvas.drawBitmap(hpRed,(screenWidth/2)+widthOffset, tileSize*6+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(myHpGreen,(screenWidth/2)+widthOffset, tileSize*6+upperA.getHeight()+heightOffset, null);
                //canvas.drawBitmap(xpYellow,(screenWidth/2)+widthOffset+upperA.getWidth()*6, tileSize*6+upperA.getHeight()+heightOffset, null);
                //canvas.drawBitmap(xpBlue,(screenWidth/2)+widthOffset+upperA.getWidth()*6, tileSize*6+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(hpRed,0+widthOffset, tileSize+upperA.getHeight()+heightOffset, null);
                canvas.drawBitmap(theirHpGreen,0+widthOffset, tileSize+upperA.getHeight()+heightOffset, null);
            } else if (currentMenu == 9) { //FIRST TIME PICK YOUR MONSTER SCREEN
                ArrayList<Bitmap> sentenceBitmaps = getSentence("Choose your starting Monster: ");
                int sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, tileSize+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceBitmaps = getSentence("BEETISH");
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*3)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceBitmaps = getSentence("PENGY");
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*5)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
                sentenceBitmaps = getSentence("CHARMOUSE");
                sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*7)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
            }else if(currentMenu == 10){ //recovery center
                canvas.drawBitmap(fitmonCenter, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(heal,tileSize*5+widthOffset, tileSize*5+heightOffset, null);
                healStat = "";
                if(healStatus.size() > 0){
                    healStat = healStatus.get(0);
                }
                    ArrayList<Bitmap> sentenceBitmaps = getSentence(healStat);
                    int sentenceOffset = 0;
                    for (int i = 0; i < sentenceBitmaps.size(); i++) {
                        if (sentenceBitmaps.get(i) != null) {
                            canvas.drawBitmap(sentenceBitmaps.get(i), sentenceOffset+widthOffset, screenHeight-sentenceBitmaps.get(i).getHeight()-heightOffset, null);
                            sentenceOffset += sentenceBitmaps.get(i).getWidth();
                        } else {
                            sentenceOffset += lowerA.getWidth();
                        }
                    }
            }else if(currentMenu == 11){ //mart
                canvas.drawBitmap(mart, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
                //Tyler place buttons
                canvas.drawBitmap(martMonsterPotion,tileSize*3+widthOffset, tileSize*3+heightOffset, null);
                canvas.drawBitmap(martMonsterCharm,tileSize*3+widthOffset, tileSize*6+heightOffset, null);
            }else if(currentMenu == 12){ //gym
                canvas.drawBitmap(gym, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
            } else if(currentMenu == 13){ //wilderness
                canvas.drawBitmap(wilderness, 0+widthOffset, 0+heightOffset, null);
                canvas.drawBitmap(buttonBack, 0+widthOffset, 0+heightOffset, null);
                ArrayList<Bitmap> sentenceBitmaps = getSentence("");
                if (munny < 50) {
                    sentenceBitmaps = getSentence("You need more munny.");
                } else {
                    sentenceBitmaps = getSentence("You did not find any Monsters.");
                }
                int sentenceOffset = 0;
                for (int i = 0; i < sentenceBitmaps.size(); i++) {
                    if (sentenceBitmaps.get(i) != null) {
                        canvas.drawBitmap(sentenceBitmaps.get(i), 0+sentenceOffset+widthOffset, (tileSize*5)+heightOffset, null);
                        sentenceOffset += sentenceBitmaps.get(i).getWidth();
                    } else {
                        sentenceOffset += lowerA.getWidth();
                    }
                }
            }
            invalidate();
        }
    }

    public void loadImages() {
        Resources r = this.getResources();
        Matrix matrix = new Matrix();

        dollar = loadBitmap(r.getDrawable(R.drawable.dollar), false);
        fitmonCenter = loadBitmap(r.getDrawable(R.drawable.fitmoncenter), false);
        heal = loadBitmap(r.getDrawable(R.drawable.button_heal), false);
        gym = loadBitmap(r.getDrawable(R.drawable.gym), false);
        wilderness = loadBitmap(r.getDrawable(R.drawable.wilderness), false);
        mart = loadBitmap(r.getDrawable(R.drawable.mart), false);
        connectedSign = loadBitmap(r.getDrawable(R.drawable.button_connected), false);
        disconnectedSign = loadBitmap(r.getDrawable(R.drawable.button_disconnected), false);
        battleInfo = loadBitmap(r.getDrawable(R.drawable.battleinfo), true);
        battleBackground = loadBitmap(r.getDrawable(R.drawable.battlebackground), true);
        beetishFront = loadBitmap(r.getDrawable(R.drawable.beetish), true);
        beetishBack = loadBitmap(r.getDrawable(R.drawable.beetishbehind), true);
        pengyFront = loadBitmap(r.getDrawable(R.drawable.pengy), true);
        pengyBack = loadBitmap(r.getDrawable(R.drawable.pengybehind), true);
        charmouseFront = loadBitmap(r.getDrawable(R.drawable.charmouse), true);
        charmouseBack = loadBitmap(r.getDrawable(R.drawable.charmousebehind), true);
        beaverFront = loadBitmap(r.getDrawable(R.drawable.beaver), true);
        beaverBack = loadBitmap(r.getDrawable(R.drawable.beaverbehind), true);
        dragonFront = loadBitmap(r.getDrawable(R.drawable.dragon), true);
        dragonBack = loadBitmap(r.getDrawable(R.drawable.dragonbehind), true);
        frosqueakFront = loadBitmap(r.getDrawable(R.drawable.frosqueak), true);
        frosqueakBack = loadBitmap(r.getDrawable(R.drawable.frosqueakbehind), true);
        ghostlyFront = loadBitmap(r.getDrawable(R.drawable.ghostly), true);
        ghostlyBack = loadBitmap(r.getDrawable(R.drawable.ghostlybehind), true);
        medupsyFront = loadBitmap(r.getDrawable(R.drawable.medupsy), true);
        medupsyBack = loadBitmap(r.getDrawable(R.drawable.medupsybehind), true);
        redipsFront = loadBitmap(r.getDrawable(R.drawable.redips), true);
        redipsBack = loadBitmap(r.getDrawable(R.drawable.redipsbehind), true);
        rockyFront = loadBitmap(r.getDrawable(R.drawable.rocky), true);
        rockyBack = loadBitmap(r.getDrawable(R.drawable.rockybehind), true);
        sandslashFront = loadBitmap(r.getDrawable(R.drawable.sandslash), true);
        sandslashBack = loadBitmap(r.getDrawable(R.drawable.sandslashbehind), true);
        shovelknightFront = loadBitmap(r.getDrawable(R.drawable.shovelknight), true);
        shovelknightBack = loadBitmap(r.getDrawable(R.drawable.shovelknightbehind), true);
        subatFront = loadBitmap(r.getDrawable(R.drawable.subat), true);
        subatBack = loadBitmap(r.getDrawable(R.drawable.subatbehind), true);
        venocatFront = loadBitmap(r.getDrawable(R.drawable.venocat), true);
        venocatBack = loadBitmap(r.getDrawable(R.drawable.venocatbehind), true);

        myHpGreen = loadBitmap(r.getDrawable(R.drawable.green), false);
        theirHpGreen = loadBitmap(r.getDrawable(R.drawable.green), false);
        hpRed = loadBitmap(r.getDrawable(R.drawable.red), false);
        xpBlue = loadBitmap(r.getDrawable(R.drawable.blue), false);
        xpYellow = loadBitmap(r.getDrawable(R.drawable.yellow), false);
        moveVineWhip = loadBitmap(r.getDrawable(R.drawable.grass_vinewhip), false);
        moveScratch = loadBitmap(r.getDrawable(R.drawable.normal_scratch), false);
        moveSmokeScreen = loadBitmap(r.getDrawable(R.drawable.normal_smokescreen), false);
        moveBubble = loadBitmap(r.getDrawable(R.drawable.water_bubble), false);
        moveCut = loadBitmap(r.getDrawable(R.drawable.normal_cut), false);
        moveEmber = loadBitmap(r.getDrawable(R.drawable.fire_ember), false);
        moveGrowl = loadBitmap(r.getDrawable(R.drawable.normal_growl), false);
        moveLeer = loadBitmap(r.getDrawable(R.drawable.normal_leer), false);
        movePound = loadBitmap(r.getDrawable(R.drawable.normal_pound), false);
        moveSurf = loadBitmap(r.getDrawable(R.drawable.water_surf), false);
        //Tyler Added the moves from here to the next comment
        moveBite = loadBitmap(r.getDrawable(R.drawable.dark_bite), false);
        moveScreech = loadBitmap(r.getDrawable(R.drawable.normal_screech), false);
        moveFlash = loadBitmap(r.getDrawable(R.drawable.normal_flash), false);
        moveGrowth = loadBitmap(r.getDrawable(R.drawable.normal_growth), false);
        moveRockSlide = loadBitmap(r.getDrawable(R.drawable.rock_rockslide), false);
        moveRockThrow = loadBitmap(r.getDrawable(R.drawable.rock_rockthrow), false);
        moveBodySlam = loadBitmap(r.getDrawable(R.drawable.normal_bodyslam), false);
        moveDefenseCurl = loadBitmap(r.getDrawable(R.drawable.normal_defensecurl), false);
        moveConfuseRay = loadBitmap(r.getDrawable(R.drawable.ghost_confuseray), false);
        moveLick = loadBitmap(r.getDrawable(R.drawable.ghost_lick), false);
        moveNightShade = loadBitmap(r.getDrawable(R.drawable.ghost_nightshade), false);
        moveSing = loadBitmap(r.getDrawable(R.drawable.normal_sing), false);
        moveAcid = loadBitmap(r.getDrawable(R.drawable.poison_acid), false);
        movePoisonGas = loadBitmap(r.getDrawable(R.drawable.poison_poisongas), false);
        moveSmog = loadBitmap(r.getDrawable(R.drawable.poison_smog), false);
        moveSludge = loadBitmap(r.getDrawable(R.drawable.poison_sludge), false);
        moveLeechLife = loadBitmap(r.getDrawable(R.drawable.bug_leechlife), false);
        movePinMissle = loadBitmap(r.getDrawable(R.drawable.bug_pinmissle), false);
        moveStringShot = loadBitmap(r.getDrawable(R.drawable.bug_stringshot), false);
        moveTwineedle = loadBitmap(r.getDrawable(R.drawable.bug_twineedle), false);
        moveAuroraBeam = loadBitmap(r.getDrawable(R.drawable.ice_aurorabeam), false);
        moveBlizzard = loadBitmap(r.getDrawable(R.drawable.ice_blizzard), false);
        moveIceBeam = loadBitmap(r.getDrawable(R.drawable.ice_icebeam), false);
        moveHeadButt = loadBitmap(r.getDrawable(R.drawable.normal_headbutt), false);
        //My move contribution ends here
        //Tyler Mart Buttons
        martMonsterCharm = loadBitmap(r.getDrawable(R.drawable.monstercharm), false);
        martMonsterPotion = loadBitmap(r.getDrawable(R.drawable.monsterpotion), false);
        moveTackle = loadBitmap(r.getDrawable(R.drawable.normal_tackle), false);
        moveTailWhip = loadBitmap(r.getDrawable(R.drawable.normal_tailwhip), false);
        selector = loadBitmap(r.getDrawable(R.drawable.redselector), false);
        mainMenu = loadBitmap(r.getDrawable(R.drawable.backdrop_main_activity), false);
        buttonFight = loadBitmap(r.getDrawable(R.drawable.button_fight), false);
        buttonItem = loadBitmap(r.getDrawable(R.drawable.button_item), false);
        buttonSwap = loadBitmap(r.getDrawable(R.drawable.button_swap), false);
        buttonRun = loadBitmap(r.getDrawable(R.drawable.button_run), false);
        buttonBattle = loadBitmap(r.getDrawable(R.drawable.button_battle), false);
        buttonMyTeam = loadBitmap(r.getDrawable(R.drawable.button_myteam), false);
        buttonQuests = loadBitmap(r.getDrawable(R.drawable.button_quests), false);
        buttonVillage = loadBitmap(r.getDrawable(R.drawable.button_village), false);
        buttonWilderness = loadBitmap(r.getDrawable(R.drawable.button_wilderness), false);
        buttonCenter = loadBitmap(r.getDrawable(R.drawable.button_center), false);
        buttonMart = loadBitmap(r.getDrawable(R.drawable.button_mart), false);
        buttonGym = loadBitmap(r.getDrawable(R.drawable.button_gym), false);
        buttonSearch = loadBitmap(r.getDrawable(R.drawable.button_search),false);
        buttonBack = loadBitmap(r.getDrawable(R.drawable.button_back), false);
        buttonConfirm = loadBitmap(r.getDrawable(R.drawable.button_confirm), false);
        buttonCancel = loadBitmap(r.getDrawable(R.drawable.button_cancel), false);
        zero = loadBitmap(r.getDrawable(R.drawable.zero), false);
        one = loadBitmap(r.getDrawable(R.drawable.one), false);
        two = loadBitmap(r.getDrawable(R.drawable.two), false);
        three = loadBitmap(r.getDrawable(R.drawable.three), false);
        four = loadBitmap(r.getDrawable(R.drawable.four), false);
        five = loadBitmap(r.getDrawable(R.drawable.five), false);
        six = loadBitmap(r.getDrawable(R.drawable.six), false);
        seven = loadBitmap(r.getDrawable(R.drawable.seven), false);
        eight = loadBitmap(r.getDrawable(R.drawable.eight), false);
        nine = loadBitmap(r.getDrawable(R.drawable.nine), false);
        lowerA = loadBitmap(r.getDrawable(R.drawable.lowera), false);
        lowerB = loadBitmap(r.getDrawable(R.drawable.lowerb), false);
        lowerC = loadBitmap(r.getDrawable(R.drawable.lowerc), false);
        lowerD = loadBitmap(r.getDrawable(R.drawable.lowerd), false);
        lowerE = loadBitmap(r.getDrawable(R.drawable.lowere), false);
        lowerF = loadBitmap(r.getDrawable(R.drawable.lowerf), false);
        lowerG = loadBitmap(r.getDrawable(R.drawable.lowerg), false);
        lowerH = loadBitmap(r.getDrawable(R.drawable.lowerh), false);
        lowerI = loadBitmap(r.getDrawable(R.drawable.loweri), false);
        lowerJ = loadBitmap(r.getDrawable(R.drawable.lowerj), false);
        lowerK = loadBitmap(r.getDrawable(R.drawable.lowerk), false);
        lowerL = loadBitmap(r.getDrawable(R.drawable.lowerl), false);
        lowerM = loadBitmap(r.getDrawable(R.drawable.lowerm), false);
        lowerN = loadBitmap(r.getDrawable(R.drawable.lowern), false);
        lowerO = loadBitmap(r.getDrawable(R.drawable.lowero), false);
        lowerP = loadBitmap(r.getDrawable(R.drawable.lowerp), false);
        lowerQ = loadBitmap(r.getDrawable(R.drawable.lowerq), false);
        lowerR = loadBitmap(r.getDrawable(R.drawable.lowerr), false);
        lowerS = loadBitmap(r.getDrawable(R.drawable.lowers), false);
        lowerT = loadBitmap(r.getDrawable(R.drawable.lowert), false);
        lowerU = loadBitmap(r.getDrawable(R.drawable.loweru), false);
        lowerV = loadBitmap(r.getDrawable(R.drawable.lowerv), false);
        lowerW = loadBitmap(r.getDrawable(R.drawable.lowerw), false);
        lowerX = loadBitmap(r.getDrawable(R.drawable.lowerx), false);
        lowerY = loadBitmap(r.getDrawable(R.drawable.lowery), false);
        lowerZ = loadBitmap(r.getDrawable(R.drawable.lowerz), false);
        upperA = loadBitmap(r.getDrawable(R.drawable.uppera), false);
        upperB = loadBitmap(r.getDrawable(R.drawable.upperb), false);
        upperC = loadBitmap(r.getDrawable(R.drawable.upperc), false);
        upperD = loadBitmap(r.getDrawable(R.drawable.upperd), false);
        upperE = loadBitmap(r.getDrawable(R.drawable.uppere), false);
        upperF = loadBitmap(r.getDrawable(R.drawable.upperf), false);
        upperG = loadBitmap(r.getDrawable(R.drawable.upperg), false);
        upperH = loadBitmap(r.getDrawable(R.drawable.upperh), false);
        upperI = loadBitmap(r.getDrawable(R.drawable.upperi), false);
        upperJ = loadBitmap(r.getDrawable(R.drawable.upperj), false);
        upperK = loadBitmap(r.getDrawable(R.drawable.upperk), false);
        upperL = loadBitmap(r.getDrawable(R.drawable.upperl), false);
        upperM = loadBitmap(r.getDrawable(R.drawable.upperm), false);
        upperN = loadBitmap(r.getDrawable(R.drawable.uppern), false);
        upperO = loadBitmap(r.getDrawable(R.drawable.uppero), false);
        upperP = loadBitmap(r.getDrawable(R.drawable.upperp), false);
        upperQ = loadBitmap(r.getDrawable(R.drawable.upperq), false);
        upperR = loadBitmap(r.getDrawable(R.drawable.upperr), false);
        upperS = loadBitmap(r.getDrawable(R.drawable.uppers), false);
        upperT = loadBitmap(r.getDrawable(R.drawable.uppert), false);
        upperU = loadBitmap(r.getDrawable(R.drawable.upperu), false);
        upperV = loadBitmap(r.getDrawable(R.drawable.upperv), false);
        upperW = loadBitmap(r.getDrawable(R.drawable.upperw), false);
        upperX = loadBitmap(r.getDrawable(R.drawable.upperx), false);
        upperY = loadBitmap(r.getDrawable(R.drawable.uppery), false);
        upperZ = loadBitmap(r.getDrawable(R.drawable.upperz), false);

        fitmonCenter = Bitmap.createScaledBitmap(fitmonCenter, screenWidth, screenHeight, false);
        gym = Bitmap.createScaledBitmap(gym, screenWidth, screenHeight, false);
        wilderness = Bitmap.createScaledBitmap(wilderness, screenWidth, screenHeight, false);
        mart = Bitmap.createScaledBitmap(mart, screenWidth, screenHeight, false);

        matrix.setRectToRect(new RectF(0, 0, heal.getWidth(), heal.getHeight()), new RectF(0, 0, tileSize*4, tileSize*2), Matrix.ScaleToFit.CENTER);
        heal = Bitmap.createBitmap(heal, 0, 0, heal.getWidth(), heal.getHeight(), matrix, true);

        //Tyler was here
        matrix.setRectToRect(new RectF(0, 0, heal.getWidth(), heal.getHeight()), new RectF(0, 0, tileSize*4, tileSize*2), Matrix.ScaleToFit.CENTER);
        martMonsterPotion = Bitmap.createBitmap(martMonsterPotion, 0, 0, martMonsterPotion.getWidth(), martMonsterPotion.getHeight(), matrix, true);
        martMonsterCharm = Bitmap.createBitmap(martMonsterCharm, 0, 0, martMonsterCharm.getWidth(), martMonsterCharm.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, connectedSign.getWidth(), connectedSign.getHeight()), new RectF(0, 0, tileSize*2, tileSize), Matrix.ScaleToFit.CENTER);
        connectedSign = Bitmap.createBitmap(connectedSign, 0, 0, connectedSign.getWidth(), connectedSign.getHeight(), matrix, true);
        disconnectedSign = Bitmap.createBitmap(disconnectedSign, 0, 0, disconnectedSign.getWidth(), disconnectedSign.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, battleBackground.getWidth(), battleBackground.getHeight()), new RectF(0, 0, tileSize*16, tileSize*9), Matrix.ScaleToFit.CENTER);
        battleBackground = Bitmap.createBitmap(battleBackground, 0, 0, battleBackground.getWidth(), battleBackground.getHeight(), matrix, true);
        battleInfo = Bitmap.createBitmap(battleInfo, 0, 0, battleInfo.getWidth(), battleInfo.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, moveVineWhip.getWidth(), moveVineWhip.getHeight()), new RectF(0, 0, tileSize*2, tileSize), Matrix.ScaleToFit.CENTER);
        moveVineWhip = Bitmap.createBitmap(moveVineWhip, 0, 0, moveVineWhip.getWidth(), moveVineWhip.getHeight(), matrix, true);
        moveScratch = Bitmap.createBitmap(moveScratch, 0, 0, moveScratch.getWidth(), moveScratch.getHeight(), matrix, true);
        moveSmokeScreen = Bitmap.createBitmap(moveSmokeScreen, 0, 0, moveSmokeScreen.getWidth(), moveSmokeScreen.getHeight(), matrix, true);
        moveBubble = Bitmap.createBitmap(moveBubble, 0, 0, moveBubble.getWidth(), moveBubble.getHeight(), matrix, true);
        moveCut = Bitmap.createBitmap(moveCut, 0, 0, moveCut.getWidth(), moveCut.getHeight(), matrix, true);
        moveEmber = Bitmap.createBitmap(moveEmber, 0, 0, moveEmber.getWidth(), moveEmber.getHeight(), matrix, true);
        moveGrowl = Bitmap.createBitmap(moveGrowl, 0, 0, moveGrowl.getWidth(), moveGrowl.getHeight(), matrix, true);
        moveLeer = Bitmap.createBitmap(moveLeer, 0, 0, moveLeer.getWidth(), moveLeer.getHeight(), matrix, true);
        movePound = Bitmap.createBitmap(movePound, 0, 0, movePound.getWidth(), movePound.getHeight(), matrix, true);
        moveSurf = Bitmap.createBitmap(moveSurf, 0, 0, moveSurf.getWidth(), moveSurf.getHeight(), matrix, true);
        //More additions made by Tyler, again anything broken point the blaim my way! :D
        moveBite = Bitmap.createBitmap(moveBite, 0, 0, moveBite.getWidth(), moveBite.getHeight(), matrix, true);
        moveScreech = Bitmap.createBitmap(moveScreech, 0, 0, moveScreech.getWidth(), moveScreech.getHeight(), matrix, true);
        moveFlash = Bitmap.createBitmap(moveFlash, 0, 0, moveFlash.getWidth(), moveFlash.getHeight(), matrix, true);
        moveGrowth = Bitmap.createBitmap(moveGrowth, 0, 0, moveGrowth.getWidth(), moveGrowth.getHeight(), matrix, true);
        moveRockSlide = Bitmap.createBitmap(moveRockSlide, 0, 0, moveRockSlide.getWidth(), moveRockSlide.getHeight(), matrix, true);
        moveRockThrow = Bitmap.createBitmap(moveRockThrow, 0, 0, moveRockThrow.getWidth(), moveRockThrow.getHeight(), matrix, true);
        moveBodySlam = Bitmap.createBitmap(moveBodySlam, 0, 0, moveBodySlam.getWidth(), moveBodySlam.getHeight(), matrix, true);
        moveDefenseCurl = Bitmap.createBitmap(moveDefenseCurl, 0, 0, moveDefenseCurl.getWidth(), moveDefenseCurl.getHeight(), matrix, true);
        moveConfuseRay = Bitmap.createBitmap(moveConfuseRay, 0, 0, moveConfuseRay.getWidth(), moveConfuseRay.getHeight(), matrix, true);
        moveLick = Bitmap.createBitmap(moveLick, 0, 0, moveLick.getWidth(), moveLick.getHeight(), matrix, true);
        moveNightShade = Bitmap.createBitmap(moveNightShade, 0, 0, moveNightShade.getWidth(), moveNightShade.getHeight(), matrix, true);
        moveSing = Bitmap.createBitmap(moveSing, 0, 0, moveSing.getWidth(), moveSing.getHeight(), matrix, true);
        moveAcid = Bitmap.createBitmap(moveAcid, 0, 0, moveAcid.getWidth(), moveAcid.getHeight(), matrix, true);
        movePoisonGas = Bitmap.createBitmap(movePoisonGas, 0, 0, movePoisonGas.getWidth(), movePoisonGas.getHeight(), matrix, true);
        moveSmog = Bitmap.createBitmap(moveSmog, 0, 0, moveSmog.getWidth(), moveSmog.getHeight(), matrix, true);
        moveSludge = Bitmap.createBitmap(moveSludge, 0, 0, moveSludge.getWidth(), moveSludge.getHeight(), matrix, true);
        moveLeechLife = Bitmap.createBitmap(moveLeechLife, 0, 0, moveLeechLife.getWidth(), moveLeechLife.getHeight(), matrix, true);
        movePinMissle = Bitmap.createBitmap(movePinMissle, 0, 0, movePinMissle.getWidth(), movePinMissle.getHeight(), matrix, true);
        moveStringShot = Bitmap.createBitmap(moveStringShot, 0, 0, moveStringShot.getWidth(), moveStringShot.getHeight(), matrix, true);
        moveTwineedle = Bitmap.createBitmap(moveTwineedle, 0, 0, moveTwineedle.getWidth(), moveTwineedle.getHeight(), matrix, true);
        moveAuroraBeam = Bitmap.createBitmap(moveAuroraBeam, 0, 0, moveAuroraBeam.getWidth(), moveAuroraBeam.getHeight(), matrix, true);
        moveBlizzard = Bitmap.createBitmap(moveBlizzard, 0, 0, moveBlizzard.getWidth(), moveBlizzard.getHeight(), matrix, true);
        moveIceBeam = Bitmap.createBitmap(moveIceBeam, 0, 0, moveIceBeam.getWidth(), moveIceBeam.getHeight(), matrix, true);
        moveHeadButt = Bitmap.createBitmap(moveHeadButt, 0, 0, moveHeadButt.getWidth(), moveHeadButt.getHeight(), matrix, true);
        //End Blame
        moveTackle = Bitmap.createBitmap(moveTackle, 0, 0, moveTackle.getWidth(), moveTackle.getHeight(), matrix, true);
        moveTailWhip = Bitmap.createBitmap(moveTailWhip, 0, 0, moveTailWhip.getWidth(), moveTailWhip.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, beetishFront.getWidth(), beetishFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        beetishFront = Bitmap.createBitmap(beetishFront, 0, 0, beetishFront.getWidth(), beetishFront.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, beetishBack.getWidth(), beetishBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        beetishBack = Bitmap.createBitmap(beetishBack, 0, 0, beetishBack.getWidth(), beetishBack.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, pengyFront.getWidth(), pengyFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        pengyFront = Bitmap.createBitmap(pengyFront, 0, 0, pengyFront.getWidth(), pengyFront.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, pengyBack.getWidth(), pengyBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        pengyBack = Bitmap.createBitmap(pengyBack, 0, 0, pengyBack.getWidth(), pengyBack.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, charmouseFront.getWidth(), charmouseFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        charmouseFront = Bitmap.createBitmap(charmouseFront, 0, 0, charmouseFront.getWidth(), charmouseFront.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, charmouseBack.getWidth(), charmouseBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        charmouseBack = Bitmap.createBitmap(charmouseBack, 0, 0, charmouseBack.getWidth(), charmouseBack.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, beaverFront.getWidth(), beaverFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        beaverFront = Bitmap.createBitmap(beaverFront, 0, 0, beaverFront.getWidth(), beaverFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, beaverBack.getWidth(), beaverBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        beaverBack = Bitmap.createBitmap(beaverBack, 0, 0, beaverBack.getWidth(), beaverBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, dragonFront.getWidth(), dragonFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        dragonFront = Bitmap.createBitmap(dragonFront, 0, 0, dragonFront.getWidth(), dragonFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, dragonBack.getWidth(), dragonBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        dragonBack = Bitmap.createBitmap(dragonBack, 0, 0, dragonBack.getWidth(), dragonBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, frosqueakFront.getWidth(), frosqueakFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        frosqueakFront = Bitmap.createBitmap(frosqueakFront, 0, 0, frosqueakFront.getWidth(), frosqueakFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, frosqueakBack.getWidth(), frosqueakBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        frosqueakBack = Bitmap.createBitmap(frosqueakBack, 0, 0, frosqueakBack.getWidth(), frosqueakBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, ghostlyFront.getWidth(), ghostlyFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        ghostlyFront = Bitmap.createBitmap(ghostlyFront, 0, 0, ghostlyFront.getWidth(), ghostlyFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, ghostlyBack.getWidth(), ghostlyBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        ghostlyBack = Bitmap.createBitmap(ghostlyBack, 0, 0, ghostlyBack.getWidth(), ghostlyBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, medupsyFront.getWidth(), medupsyFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        medupsyFront = Bitmap.createBitmap(medupsyFront, 0, 0, medupsyFront.getWidth(), medupsyFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, medupsyBack.getWidth(), medupsyBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        medupsyBack = Bitmap.createBitmap(medupsyBack, 0, 0, medupsyBack.getWidth(), medupsyBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, redipsFront.getWidth(), redipsFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        redipsFront = Bitmap.createBitmap(redipsFront, 0, 0, redipsFront.getWidth(), redipsFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, redipsBack.getWidth(), redipsBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        redipsBack = Bitmap.createBitmap(redipsBack, 0, 0, redipsBack.getWidth(), redipsBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, rockyFront.getWidth(), rockyFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        rockyFront = Bitmap.createBitmap(rockyFront, 0, 0, rockyFront.getWidth(), rockyFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, rockyBack.getWidth(), rockyBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        rockyBack = Bitmap.createBitmap(rockyBack, 0, 0, rockyBack.getWidth(), rockyBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, sandslashFront.getWidth(), sandslashFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        sandslashFront = Bitmap.createBitmap(sandslashFront, 0, 0, sandslashFront.getWidth(), sandslashFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, sandslashBack.getWidth(), sandslashBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        sandslashBack = Bitmap.createBitmap(sandslashBack, 0, 0, sandslashBack.getWidth(), sandslashBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, shovelknightFront.getWidth(), shovelknightFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        shovelknightFront = Bitmap.createBitmap(shovelknightFront, 0, 0, shovelknightFront.getWidth(), shovelknightFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, shovelknightBack.getWidth(), shovelknightBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        shovelknightBack = Bitmap.createBitmap(shovelknightBack, 0, 0, shovelknightBack.getWidth(), shovelknightBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, subatFront.getWidth(), subatFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        subatFront = Bitmap.createBitmap(subatFront, 0, 0, subatFront.getWidth(), subatFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, subatBack.getWidth(), subatBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        subatBack = Bitmap.createBitmap(subatBack, 0, 0, subatBack.getWidth(), subatBack.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, venocatFront.getWidth(), venocatFront.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        venocatFront = Bitmap.createBitmap(venocatFront, 0, 0, venocatFront.getWidth(), venocatFront.getHeight(), matrix, true);
        matrix.setRectToRect(new RectF(0, 0, venocatBack.getWidth(), venocatBack.getHeight()), new RectF(0, 0, tileSize*5, tileSize*5), Matrix.ScaleToFit.CENTER);
        venocatBack = Bitmap.createBitmap(venocatBack, 0, 0, venocatBack.getWidth(), venocatBack.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, mainMenu.getWidth(), mainMenu.getHeight()), new RectF(0, 0, screenWidth, screenHeight), Matrix.ScaleToFit.CENTER);
        mainMenu = Bitmap.createBitmap(mainMenu, 0, 0, mainMenu.getWidth(), mainMenu.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, buttonBattle.getWidth(), buttonBattle.getHeight()), new RectF(0, 0, tileSize*4, tileSize*2), Matrix.ScaleToFit.CENTER);
        buttonBattle = Bitmap.createBitmap(buttonBattle, 0, 0, buttonBattle.getWidth(), buttonBattle.getHeight(), matrix, true);
        buttonMyTeam = Bitmap.createBitmap(buttonMyTeam, 0, 0, buttonMyTeam.getWidth(), buttonMyTeam.getHeight(), matrix, true);
        buttonQuests = Bitmap.createBitmap(buttonQuests, 0, 0, buttonQuests.getWidth(), buttonQuests.getHeight(), matrix, true);
        buttonVillage = Bitmap.createBitmap(buttonVillage, 0, 0, buttonVillage.getWidth(), buttonVillage.getHeight(), matrix, true);
        buttonBack = Bitmap.createBitmap(buttonBack, 0, 0, buttonBack.getWidth(), buttonBack.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, buttonFight.getWidth(), buttonFight.getHeight()), new RectF(0, 0, tileSize*2, tileSize), Matrix.ScaleToFit.CENTER);
        buttonFight = Bitmap.createBitmap(buttonFight, 0, 0, buttonFight.getWidth(), buttonFight.getHeight(), matrix, true);
        buttonItem = Bitmap.createBitmap(buttonItem, 0, 0, buttonItem.getWidth(), buttonItem.getHeight(), matrix, true);
        buttonSwap = Bitmap.createBitmap(buttonSwap, 0, 0, buttonSwap.getWidth(), buttonSwap.getHeight(), matrix, true);
        buttonRun = Bitmap.createBitmap(buttonRun, 0, 0, buttonRun.getWidth(), buttonRun.getHeight(), matrix, true);
        buttonConfirm = Bitmap.createBitmap(buttonConfirm, 0, 0, buttonConfirm.getWidth(), buttonConfirm.getHeight(), matrix, true);
        buttonCancel = Bitmap.createBitmap(buttonCancel, 0, 0, buttonCancel.getWidth(), buttonCancel.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, buttonWilderness.getWidth(), buttonWilderness.getHeight()), new RectF(0, 0, tileSize*4, tileSize*2), Matrix.ScaleToFit.CENTER);
        buttonCenter = Bitmap.createBitmap(buttonCenter, 0, 0, buttonCenter.getWidth(), buttonCenter.getHeight(), matrix, true);
        buttonMart = Bitmap.createBitmap(buttonMart, 0, 0, buttonMart.getWidth(), buttonMart.getHeight(), matrix, true);
        buttonGym = Bitmap.createBitmap(buttonGym, 0, 0, buttonGym.getWidth(), buttonGym.getHeight(), matrix, true);
        buttonWilderness = Bitmap.createBitmap(buttonWilderness,0,0,buttonWilderness.getWidth(),buttonWilderness.getHeight(), matrix, true);


        matrix.setRectToRect(new RectF(0, 0, selector.getWidth(), selector.getHeight()), new RectF(0, 0, tileSize*2, tileSize), Matrix.ScaleToFit.CENTER);
        selector = Bitmap.createBitmap(selector, 0, 0, selector.getWidth(), selector.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, lowerA.getWidth(), lowerA.getHeight()), new RectF(0, 0, tileSize*10, tileSize/2), Matrix.ScaleToFit.CENTER);
        lowerA = Bitmap.createBitmap(lowerA, 0, 0, lowerA.getWidth(), lowerA.getHeight(), matrix, true);
        lowerB = Bitmap.createBitmap(lowerB, 0, 0, lowerB.getWidth(), lowerB.getHeight(), matrix, true);
        lowerC = Bitmap.createBitmap(lowerC, 0, 0, lowerC.getWidth(), lowerC.getHeight(), matrix, true);
        lowerD = Bitmap.createBitmap(lowerD, 0, 0, lowerD.getWidth(), lowerD.getHeight(), matrix, true);
        lowerE = Bitmap.createBitmap(lowerE, 0, 0, lowerE.getWidth(), lowerE.getHeight(), matrix, true);
        lowerF = Bitmap.createBitmap(lowerF, 0, 0, lowerF.getWidth(), lowerF.getHeight(), matrix, true);
        lowerG = Bitmap.createBitmap(lowerG, 0, 0, lowerG.getWidth(), lowerG.getHeight(), matrix, true);
        lowerH = Bitmap.createBitmap(lowerH, 0, 0, lowerH.getWidth(), lowerH.getHeight(), matrix, true);
        lowerI = Bitmap.createBitmap(lowerI, 0, 0, lowerI.getWidth(), lowerI.getHeight(), matrix, true);
        lowerJ = Bitmap.createBitmap(lowerJ, 0, 0, lowerJ.getWidth(), lowerJ.getHeight(), matrix, true);
        lowerK = Bitmap.createBitmap(lowerK, 0, 0, lowerK.getWidth(), lowerK.getHeight(), matrix, true);
        lowerL = Bitmap.createBitmap(lowerL, 0, 0, lowerL.getWidth(), lowerL.getHeight(), matrix, true);
        lowerM = Bitmap.createBitmap(lowerM, 0, 0, lowerM.getWidth(), lowerM.getHeight(), matrix, true);
        lowerN = Bitmap.createBitmap(lowerN, 0, 0, lowerN.getWidth(), lowerN.getHeight(), matrix, true);
        lowerO = Bitmap.createBitmap(lowerO, 0, 0, lowerO.getWidth(), lowerO.getHeight(), matrix, true);
        lowerP = Bitmap.createBitmap(lowerP, 0, 0, lowerP.getWidth(), lowerP.getHeight(), matrix, true);
        lowerQ = Bitmap.createBitmap(lowerQ, 0, 0, lowerQ.getWidth(), lowerQ.getHeight(), matrix, true);
        lowerR = Bitmap.createBitmap(lowerR, 0, 0, lowerR.getWidth(), lowerR.getHeight(), matrix, true);
        lowerS = Bitmap.createBitmap(lowerS, 0, 0, lowerS.getWidth(), lowerS.getHeight(), matrix, true);
        lowerT = Bitmap.createBitmap(lowerT, 0, 0, lowerT.getWidth(), lowerT.getHeight(), matrix, true);
        lowerU = Bitmap.createBitmap(lowerU, 0, 0, lowerU.getWidth(), lowerU.getHeight(), matrix, true);
        lowerV = Bitmap.createBitmap(lowerV, 0, 0, lowerV.getWidth(), lowerV.getHeight(), matrix, true);
        lowerW = Bitmap.createBitmap(lowerW, 0, 0, lowerW.getWidth(), lowerW.getHeight(), matrix, true);
        lowerX = Bitmap.createBitmap(lowerX, 0, 0, lowerX.getWidth(), lowerX.getHeight(), matrix, true);
        lowerY = Bitmap.createBitmap(lowerY, 0, 0, lowerY.getWidth(), lowerY.getHeight(), matrix, true);
        lowerZ = Bitmap.createBitmap(lowerZ, 0, 0, lowerZ.getWidth(), lowerZ.getHeight(), matrix, true);
        upperA = Bitmap.createBitmap(upperA, 0, 0, upperA.getWidth(), upperA.getHeight(), matrix, true);
        upperB = Bitmap.createBitmap(upperB, 0, 0, upperB.getWidth(), upperB.getHeight(), matrix, true);
        upperC = Bitmap.createBitmap(upperC, 0, 0, upperC.getWidth(), upperC.getHeight(), matrix, true);
        upperD = Bitmap.createBitmap(upperD, 0, 0, upperD.getWidth(), upperD.getHeight(), matrix, true);
        upperE = Bitmap.createBitmap(upperE, 0, 0, upperE.getWidth(), upperE.getHeight(), matrix, true);
        upperF = Bitmap.createBitmap(upperF, 0, 0, upperF.getWidth(), upperF.getHeight(), matrix, true);
        upperG = Bitmap.createBitmap(upperG, 0, 0, upperG.getWidth(), upperG.getHeight(), matrix, true);
        upperH = Bitmap.createBitmap(upperH, 0, 0, upperH.getWidth(), upperH.getHeight(), matrix, true);
        upperI = Bitmap.createBitmap(upperI, 0, 0, upperI.getWidth(), upperI.getHeight(), matrix, true);
        upperJ = Bitmap.createBitmap(upperJ, 0, 0, upperJ.getWidth(), upperJ.getHeight(), matrix, true);
        upperK = Bitmap.createBitmap(upperK, 0, 0, upperK.getWidth(), upperK.getHeight(), matrix, true);
        upperL = Bitmap.createBitmap(upperL, 0, 0, upperL.getWidth(), upperL.getHeight(), matrix, true);
        upperM = Bitmap.createBitmap(upperM, 0, 0, upperM.getWidth(), upperM.getHeight(), matrix, true);
        upperN = Bitmap.createBitmap(upperN, 0, 0, upperN.getWidth(), upperN.getHeight(), matrix, true);
        upperO = Bitmap.createBitmap(upperO, 0, 0, upperO.getWidth(), upperO.getHeight(), matrix, true);
        upperP = Bitmap.createBitmap(upperP, 0, 0, upperP.getWidth(), upperP.getHeight(), matrix, true);
        upperQ = Bitmap.createBitmap(upperQ, 0, 0, upperQ.getWidth(), upperQ.getHeight(), matrix, true);
        upperR = Bitmap.createBitmap(upperR, 0, 0, upperR.getWidth(), upperR.getHeight(), matrix, true);
        upperS = Bitmap.createBitmap(upperS, 0, 0, upperS.getWidth(), upperS.getHeight(), matrix, true);
        upperT = Bitmap.createBitmap(upperT, 0, 0, upperT.getWidth(), upperT.getHeight(), matrix, true);
        upperU = Bitmap.createBitmap(upperU, 0, 0, upperU.getWidth(), upperU.getHeight(), matrix, true);
        upperV = Bitmap.createBitmap(upperV, 0, 0, upperV.getWidth(), upperV.getHeight(), matrix, true);
        upperW = Bitmap.createBitmap(upperW, 0, 0, upperW.getWidth(), upperW.getHeight(), matrix, true);
        upperX = Bitmap.createBitmap(upperX, 0, 0, upperX.getWidth(), upperX.getHeight(), matrix, true);
        upperY = Bitmap.createBitmap(upperY, 0, 0, upperY.getWidth(), upperY.getHeight(), matrix, true);
        upperZ = Bitmap.createBitmap(upperZ, 0, 0, upperZ.getWidth(), upperZ.getHeight(), matrix, true);

        matrix.setRectToRect(new RectF(0, 0, zero.getWidth(), zero.getHeight()), new RectF(0, 0, tileSize*10, tileSize/2), Matrix.ScaleToFit.CENTER);
        zero = Bitmap.createBitmap(zero, 0, 0, zero.getWidth(), zero.getHeight(), matrix, true);
        one = Bitmap.createBitmap(one, 0, 0, one.getWidth(), one.getHeight(), matrix, true);
        two = Bitmap.createBitmap(two, 0, 0, two.getWidth(), two.getHeight(), matrix, true);
        three = Bitmap.createBitmap(three, 0, 0, three.getWidth(), three.getHeight(), matrix, true);
        four = Bitmap.createBitmap(four, 0, 0, four.getWidth(), four.getHeight(), matrix, true);
        five = Bitmap.createBitmap(five, 0, 0, five.getWidth(), five.getHeight(), matrix, true);
        six = Bitmap.createBitmap(six, 0, 0, six.getWidth(), six.getHeight(), matrix, true);
        seven = Bitmap.createBitmap(seven, 0, 0, seven.getWidth(), seven.getHeight(), matrix, true);
        eight = Bitmap.createBitmap(eight, 0, 0, eight.getWidth(), eight.getHeight(), matrix, true);
        nine = Bitmap.createBitmap(nine, 0, 0, nine.getWidth(), nine.getHeight(), matrix, true);
        dollar = Bitmap.createBitmap(dollar, 0, 0, dollar.getWidth(), dollar.getHeight(), matrix, true);

        hpRed = Bitmap.createScaledBitmap(hpRed, upperA.getWidth() / 2 * 10, upperA.getWidth() / 2, false);
        xpYellow = Bitmap.createScaledBitmap(xpYellow, upperA.getWidth() / 2 * 10, upperA.getWidth() / 2, false);
    }

    public static Bitmap loadBitmap(Drawable sprite, Bitmap.Config bitmapConfig) {
        int width = sprite.getIntrinsicWidth();
        int height = sprite.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, bitmapConfig);
        Canvas canvas = new Canvas(bitmap);
        sprite.setBounds(0, 0, width, height);
        sprite.draw(canvas);
        return bitmap;
    }

    //saves memory by loading as a smaller format if they are not transparent
    public static Bitmap loadBitmap(Drawable sprite, Boolean transparent) {
        if (transparent) {
            return loadBitmap(sprite, Bitmap.Config.ARGB_4444);
        } else {
            return loadBitmap(sprite, Bitmap.Config.RGB_565);
        }
    }

    //this method allows you to easily draw sentences
    public ArrayList<Bitmap> getSentence(String sentence) {
        ArrayList<Bitmap> bitmaps = new ArrayList();
        if (sentence != null) {
            for(int j=0; j<sentence.length(); j++) {
                switch (sentence.charAt(j)) {
                    case 'A' :
                        bitmaps.add(upperA);
                        break;
                    case 'B' :
                        bitmaps.add(upperB);
                        break;
                    case 'C' :
                        bitmaps.add(upperC);
                        break;
                    case 'D' :
                        bitmaps.add(upperD);
                        break;
                    case 'E' :
                        bitmaps.add(upperE);
                        break;
                    case 'F' :
                        bitmaps.add(upperF);
                        break;
                    case 'G' :
                        bitmaps.add(upperG);
                        break;
                    case 'H' :
                        bitmaps.add(upperH);
                        break;
                    case 'I' :
                        bitmaps.add(upperI);
                        break;
                    case 'J' :
                        bitmaps.add(upperJ);
                        break;
                    case 'K' :
                        bitmaps.add(upperK);
                        break;
                    case 'L' :
                        bitmaps.add(upperL);
                        break;
                    case 'M' :
                        bitmaps.add(upperM);
                        break;
                    case 'N' :
                        bitmaps.add(upperN);
                        break;
                    case 'O' :
                        bitmaps.add(upperO);
                        break;
                    case 'P' :
                        bitmaps.add(upperP);
                        break;
                    case 'Q' :
                        bitmaps.add(upperQ);
                        break;
                    case 'R' :
                        bitmaps.add(upperR);
                        break;
                    case 'S' :
                        bitmaps.add(upperS);
                        break;
                    case 'T' :
                        bitmaps.add(upperT);
                        break;
                    case 'U' :
                        bitmaps.add(upperU);
                        break;
                    case 'V' :
                        bitmaps.add(upperV);
                        break;
                    case 'W' :
                        bitmaps.add(upperW);
                        break;
                    case 'X' :
                        bitmaps.add(upperX);
                        break;
                    case 'Y' :
                        bitmaps.add(upperY);
                        break;
                    case 'Z' :
                        bitmaps.add(upperZ);
                        break;
                    case 'a' :
                        bitmaps.add(lowerA);
                        break;
                    case 'b' :
                        bitmaps.add(lowerB);
                        break;
                    case 'c' :
                        bitmaps.add(lowerC);
                        break;
                    case 'd' :
                        bitmaps.add(lowerD);
                        break;
                    case 'e' :
                        bitmaps.add(lowerE);
                        break;
                    case 'f' :
                        bitmaps.add(lowerF);
                        break;
                    case 'g' :
                        bitmaps.add(lowerG);
                        break;
                    case 'h' :
                        bitmaps.add(lowerH);
                        break;
                    case 'i' :
                        bitmaps.add(lowerI);
                        break;
                    case 'j' :
                        bitmaps.add(lowerJ);
                        break;
                    case 'k' :
                        bitmaps.add(lowerK);
                        break;
                    case 'l' :
                        bitmaps.add(lowerL);
                        break;
                    case 'm' :
                        bitmaps.add(lowerM);
                        break;
                    case 'n' :
                        bitmaps.add(lowerN);
                        break;
                    case 'o' :
                        bitmaps.add(lowerO);
                        break;
                    case 'p' :
                        bitmaps.add(lowerP);
                        break;
                    case 'q' :
                        bitmaps.add(lowerQ);
                        break;
                    case 'r' :
                        bitmaps.add(lowerR);
                        break;
                    case 's' :
                        bitmaps.add(lowerS);
                        break;
                    case 't' :
                        bitmaps.add(lowerT);
                        break;
                    case 'u' :
                        bitmaps.add(lowerU);
                        break;
                    case 'v' :
                        bitmaps.add(lowerV);
                        break;
                    case 'w' :
                        bitmaps.add(lowerW);
                        break;
                    case 'x' :
                        bitmaps.add(lowerX);
                        break;
                    case 'y' :
                        bitmaps.add(lowerY);
                        break;
                    case 'z' :
                        bitmaps.add(lowerZ);
                        break;
                    case '0' :
                        bitmaps.add(zero);
                        break;
                    case '1' :
                        bitmaps.add(one);
                        break;
                    case '2' :
                        bitmaps.add(two);
                        break;
                    case '3' :
                        bitmaps.add(three);
                        break;
                    case '4' :
                        bitmaps.add(four);
                        break;
                    case '5' :
                        bitmaps.add(five);
                        break;
                    case '6' :
                        bitmaps.add(six);
                        break;
                    case '7' :
                        bitmaps.add(seven);
                        break;
                    case '8' :
                        bitmaps.add(eight);
                        break;
                    case '9' :
                        bitmaps.add(nine);
                        break;
                    case '$' :
                        bitmaps.add(dollar);
                    default :
                        bitmaps.add(null);
                        break;
                }
            }
        } else {
            bitmaps.add(null);
        }
        return bitmaps;
    }

    //this method is basically to make it so that you can know what to draw in the combat menu
    public Bitmap getMoveBitmap(int whichMove) {
        Bitmap b = null;
        String name = "";
        if (whichMove == 1) {
            name = myTeam[myCurrentMonster].getMove1().getName();
        } else if (whichMove == 2) {
            name = myTeam[myCurrentMonster].getMove2().getName();
        } else if (whichMove == 3) {
            name = myTeam[myCurrentMonster].getMove3().getName();
        } else if (whichMove == 4) {
            name = myTeam[myCurrentMonster].getMove4().getName();
        }

        switch (name) {
            case "Ember" :
                b = moveEmber;
                break;
            case "Vine Whip" :
                b = moveVineWhip;
                break;
            case "Cut" :
                b = moveCut;
                break;
            case "Growl" :
                b = moveGrowl;
                break;
            case "Leer" :
                b = moveLeer;
                break;
            case "Pound" :
                b = movePound;
                break;
            case "Scratch" :
                b = moveScratch;
                break;
            case "Smokescreen" :
                b = moveSmokeScreen;
                break;
            case "Tackle" :
                b = moveTackle;
                break;
            case "Tail Whip" :
                b = moveTailWhip;
                break;
            case "Bubble" :
                b = moveBubble;
                break;
            case "Surf" :
                b = moveSurf;
                break;
            case "Bite" : //NEW MOVES FROM HERE DOWN WERE ADDED BY TYLER, BLAME ME FOR ANYTHING BREAKING. :P
                break;
            case "Screech" :
                break;
            case "Flash" :
                break;
            case "Growth" :
                break;
            case "Rock Slide" :
                break;
            case "Rock Throw" :
                break;
            case "Body Slam" :
                break;
            case "Defense Curl" :
                break;
            case "Confuse Ray" :
                break;
            case "Lick" :
                break;
            case "Night Shade" :
                break;
            case "Sing" :
                break;
            case "Acid" :
                break;
            case "Poison Gas" :
                break;
            case "Smog" :
                break;
            case "Sludge" :
                break;
            case "Leech Life" :
                break;
            case "Pin Missile" :
                break;
            case "String Shot" :
                break;
            case "Twineedle" :
                break;
            case "Aurora Beam" :
                break;
            case "Blizzard" :
                break;
            case "Ice Beam" :
                break;
            case "Headbutt" :
                break;
            case "Sand Attack" :
                break;
            case "Bonemerang" :
                break;
            case "Fissure" :
                break;
            case "Bone Club" :
                break;
            case "Hypnosis" :
                break;
            case "Psybeam" :
                break;
            case "Psychic" :
                break;
            case "Barrier" :
                break;
            case "Comet Punch" :
                break;
            case "Dizzy Punch" :
                break;
            case "Ice Break" :
                break;
            case "Thunder Punch" :
                break;
            case "Dragon Rage" :
                break;
            case "Fire Blast" :
                break;
            case "Flamethrower" :
                break;
            case "Recover" :
                break;
            case "Fury Swipes" :
                break;
            case "Hyper Fang" :
                break;
            case "Sharpen" :
                break;
            case "Splash" :
                break;
            default :
                break;
        }
        return b;
    }

    //This is where you create a new npc monster. Player monsters will have non standard creation templates
    public Monster generateMonster(int monsterNumber, int level) {
        Monster m = new Monster();
        m.setEvasion(100);
        m.setAccuracy(100);
        m.setLevel(level);
        m.setMonsterNumber(monsterNumber);
        //the reason types are ints is because i use them as an index in a 2d array to determine damage modifiers
        //types: 0:normal, 1:fire, 2:water, 3:electric, 4:grass, 5:ice, 6:fighting, 7:poison,
        //8:ground, 9:flying, 10:psychic, 11:bug, 12:rock, 13:ghost, 14:dragon, 15:dark, 16:steel
        //-1:N/A
        switch (monsterNumber) {
            case 0:
                m.setName("Beetish");
                m.setType1(4);
                m.setType2(-1);
                m.setBaseHp((80));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((82));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((83));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((100));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((100));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((80));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Vine Whip", 1);
                m.setMove("Growl", 2);
                m.setMove("Cut", 3);
                m.setMove("Tackle", 4);
                break;
            case 1:
                m.setName("Pengy");
                m.setType1(2);
                m.setType2(-1);
                m.setBaseHp((79));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((83));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((100));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((85));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((105));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((78));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Bubble", 1);
                m.setMove("Surf", 2);
                m.setMove("Pound", 3);
                m.setMove("Tail Whip", 4);
                break;
            case 2:
                m.setName("Charmouse");
                m.setType1(1);
                m.setType2(-1);
                m.setBaseHp((78));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((84));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((78));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((109));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((85));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((100));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Ember", 1);
                m.setMove("Leer", 2);
                m.setMove("Smokescreen", 3);
                m.setMove("Scratch", 4);
                break;
            case 3:
                //beaver
                m.setName("Beaver");
                m.setType1(0);
                m.setType2(-1);
                m.setBaseHp((55));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((81));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((60));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((50));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((70));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((97));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Fury Swipes", 1);
                m.setMove("Hyper Fang", 2);
                m.setMove("Sharpen", 3);
                m.setMove("Splash", 4);
                break;
            case 4:
                //dragon
                m.setName("Dragon");
                m.setType1(14);
                m.setType2(-1);
                m.setBaseHp((91));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((134));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((95));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((100));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((100));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((80));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Dragon Rage", 1);
                m.setMove("Fire Blast", 2);
                m.setMove("Flamethrower", 3);
                m.setMove("Recover", 4);
                break;
            case 5:
                //shovel knight
                m.setName("Shovel Knight");
                m.setType1(6);
                m.setType2(-1);
                m.setBaseHp((50));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((105));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((79));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((35));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((110));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((76));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Comet Punch", 1);
                m.setMove("Dizzy Punch", 2);
                m.setMove("Ice Punch", 3);
                m.setMove("Thunder Punch", 4);
                break;
            case 6:
                //frosqueak
                m.setName("Frosqueak");
                m.setType1(5);
                m.setType2(-1);
                m.setBaseHp((90));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((85));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((100));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((95));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((125));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((85));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Aurora Beam", 1);
                m.setMove("Blizzard", 2);
                m.setMove("Ice Beam", 3);
                m.setMove("Headbutt", 4);
                break;
            case 7:
                //ghostly
                m.setName("Ghostly");
                m.setType1(13);
                m.setType2(-1);
                m.setBaseHp((60));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((65));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((60));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((130));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((75));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((110));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Confuse Ray", 1);
                m.setMove("Lick", 2);
                m.setMove("Night Shade", 3);
                m.setMove("Sing", 4);
                break;
            case 8:
                //medupsy
                m.setName("Medupsy");
                m.setType1(10);
                m.setType2(-1);
                m.setBaseHp((55));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((50));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((45));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((135));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((95));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((120));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Hypnosis", 1);
                m.setMove("Psybeam", 2);
                m.setMove("Psychic", 3);
                m.setMove("Barrier", 4);
                break;
            case 9:
                //redips
                m.setName("Redips");
                m.setType1(11);
                m.setType2(-1);
                m.setBaseHp((65));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((125));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((100));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((55));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((70));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((85));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Leech Life", 1);
                m.setMove("Pin Missile", 2);
                m.setMove("String Shot", 3);
                m.setMove("Twineedle", 4);
                break;
            case 10:
                //rocky
                m.setName("Rocky");
                m.setType1(12);
                m.setType2(-1);
                m.setBaseHp((80));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((120));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((130));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((55));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((65));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((45));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Rock Slide", 1);
                m.setMove("Rock Throw", 2);
                m.setMove("Body Slam", 3);
                m.setMove("Defense Curl", 4);
                break;
            case 11:
                //sandslash
                m.setName("Sandy");
                m.setType1(8);
                m.setType2(-1);
                m.setBaseHp((75));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((100));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((110));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((45));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((55));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((65));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Sand Attack", 1);
                m.setMove("Bonemerang", 2);
                m.setMove("Fissure", 3);
                m.setMove("Bone Club", 4);
                break;
            case 12:
                //subat
                m.setName("Subat");
                m.setType1(9);
                m.setType2(-1);
                m.setBaseHp((75));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((80));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((70));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((65));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((75));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((90));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Bite", 1);
                m.setMove("Screech", 2);
                m.setMove("Flash", 3);
                m.setMove("Growth", 4);
                break;
            case 13:
                //venocat
                m.setName("Venocat");
                m.setType1(7);
                m.setType2(-1);
                m.setBaseHp((90));
                m.setHp(m.getBaseHp());
                m.setBaseAttack((92));
                m.setAttack(m.getBaseAttack());
                m.setBaseDefense((87));
                m.setDefense(m.getBaseDefense());
                m.setBaseSpecialAttack((75));
                m.setSpecialAttack(m.getBaseSpecialAttack());
                m.setBaseSpecialDefense((85));
                m.setSpecialDefense(m.getBaseSpecialDefense());
                m.setBaseSpeed((76));
                m.setSpeed(m.getBaseSpeed());
                m.setMove("Acid", 1);
                m.setMove("Poison Gas", 2);
                m.setMove("Smog", 3);
                m.setMove("Sludge", 4);
                break;

        }
        return m;
    }

    //allows you to update 30 fps, returns true when it's updated
    public boolean animationMath() {
        prevTime = curTime;
        curTime = System.nanoTime();
        deltaTime = (curTime-prevTime)/1000000;
        deltaSum += deltaTime;

        //30 times a second (or 33 milliseconds), update the animations
        if (deltaSum >= 33) {
            deltaSum = 0;
            return true;
        }
        return false;
    }

    public class Nuggeta extends NSample {
        public NGame game;
        public String gameId;
        public Nuggeta(String url) {
            super(url);
        }

        @Override
        public void run() {
            nuggetaPlug.setStartResponseHandler(new StartResponseHandler() {
                @Override
                public void onStartResponse(StartResponse startresponse) {
                    if (startresponse.getStartStatus() == StartStatus.READY) {
                        sampleIO.connected();
                        log("Hello NUGGETA!");
                        connected = true;
                        log("gameApi.getPlayerProfile(): " + gameApi.getPlayerProfile());
                        NuggetaQuery nquery = new NuggetaQuery();
                        log("gameApi.getPlayers(nquery): " + gameApi.getPlayers(nquery));
                        log("nuggetaPlug.loadConnectionId(): " + nuggetaPlug.loadConnectionId());

                    } else {
                        log("Nuggeta start failed");
                    }
                }
            });

            nuggetaPlug.setConnectionInterruptedListener(new ConnectionInterruptedListener() {
                @Override
                public void onConnectionInterrupted() {
                    connected = false;
                    log("onConnectionInterrupted");
                }
            });

            nuggetaPlug.setConnectionLostListener(new ConnectionLostListener() {
                @Override
                public void onConnectionLost() {
                    connected = false;
                    log("onConnectionLost");
                }
            });

            gameApi = nuggetaPlug.gameApi();

            gameApi.addJoinGameResponseHandler(new JoinGameResponseHandler() {
                @Override
                public void onJoinGameResponse(JoinGameResponse joinGameResponse) {
                    log("joinGameResponse: " + joinGameResponse.toString());
                }
            });

            gameApi.addMatchAndJoinGameResponseHandler(new MatchAndJoinGameResponseHandler() {
                @Override
                public void onMatchAndJoinGameResponse(MatchAndJoinGameResponse matchAndJoinGameResponse) {
                    log("matchAndJoinGameResponse: " + matchAndJoinGameResponse.toString());
                }
            });

            gameApi.addNMatchAndJoinGameExpiredNotificationHandler(new NMatchAndJoinGameExpiredNotificationHandler() {
                @Override
                public void onNMatchAndJoinGameExpiredNotification(NMatchAndJoinGameExpiredNotification matchAndJoinGameExpiredNotification) {
                    log("matchAndJoinGameExpiredNotification: " + matchAndJoinGameExpiredNotification.getGameId());
                }
            });

            gameApi.addNDisconnectedNotificationHandler(new NDisconnectedNotificationHandler() {
                @Override
                public void onNDisconnectedNotification(NDisconnectedNotification ndisconnectednotification) {
                    log("ndisconnectednotification: " + ndisconnectednotification);
                }
            });

            gameApi.addNRawGameMessageHandler(new NRawGameMessageHandler() {
                @Override
                public void onNRawGameMessage(NRawGameMessage rawMessage) {
                    log("rawMessage: " + rawMessage.getContent().toString());
                    //unpackGameState(rawMessage.getContent().toString());
                }
            });

            gameApi.addPlayerEnterGameHandler(new PlayerEnterGameHandler() {
                @Override
                public void onPlayerEnterGame(PlayerEnterGame playerEnterGame) {
                    String name = playerEnterGame.getPlayer().getName();
                    log("Player " + name + " joined the game");
                }
            });

            gameApi.addPlayerUnjoinGameHandler(new PlayerUnjoinGameHandler() {
                @Override
                public void onPlayerUnjoinGame(PlayerUnjoinGame playerUnjoinGame) {
                    String name = playerUnjoinGame.getPlayer().getName();
                    log("Player " + name + " left the game");
                }
            });

            gameApi.addSendMessageResponseHandler(new SendMessageResponseHandler() {
                @Override
                public void onSendMessageResponse(SendMessageResponse sendMessageResponse) {
                    log("sendMessageResponse: " + sendMessageResponse.toString());
                }
            });

            gameApi.addSendMessageToGameResponseHandler(new SendMessageToGameResponseHandler() {
                @Override
                public void onSendMessageToGameResponse(SendMessageToGameResponse sendMessageToGameResponse) {
                    log("sendMessageToGameResponse: " + sendMessageToGameResponse.toString());
                }
            });

            gameApi.addSendMessageToPlayerResponseHandler(new SendMessageToPlayerResponseHandler() {
                @Override
                public void onSendMessageToPlayerResponse(SendMessageToPlayerResponse sendMessageToPlayerResponse) {
                    log("sendMessageToPlayerResponse: " + sendMessageToPlayerResponse.toString());
                }
            });

            gameApi.addCreateGameResponseHandler(new CreateGameResponseHandler() {
                @Override
                public void onCreateGameResponse(CreateGameResponse createGameResponse) {
                    log("createGameResponse: " + createGameResponse.toString());
                }
            });

            gameApi.addNextPlayerTurnResponseHandler(new NextPlayerTurnResponseHandler() {
                @Override
                public void onNextPlayerTurnResponse(NextPlayerTurnResponse nextPlayerTurnResponse) {
                    log("nextPlayerTurnResponse: " + nextPlayerTurnResponse.toString());
                }
            });

            gameApi.addPlayerTurnNotificationHandler(new PlayerTurnNotificationHandler() {
                @Override
                public void onPlayerTurnNotification(PlayerTurnNotification playerTurnNotification) {
                    log("playerTurnNotification: " + playerTurnNotification.toString());
                }
            });

            nuggetaPlug.start();
        }

        @Override
        public void saveMultiplayerGame() {
            String FILENAME = "multiplayer_game";
            sampleIO.log("game: " + game);
            if (game != null) {
                StringBuilder sb = new StringBuilder();
                String content = sb.toString();
                //content += turn;
                try {
                    FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                    fos.write(content.getBytes());
                    fos.close();
                } catch (Exception e) {
                }
            }
        }

        @Override
        protected void createGame(int minPlayers, int maxPlayers, final String name) {
            game = new NGame();
            game.setName(name);
            NGameCharacteristics gameCharacteristics = new NGameCharacteristics();
            gameCharacteristics.setMinPlayer(minPlayers);
            gameCharacteristics.setMaxPlayer(maxPlayers);
            gameCharacteristics.setAutoStop(false);
            gameCharacteristics.setAutoStart(true);
            game.setGameCharacteristics(gameCharacteristics);
            log("Create Game ");
            gameApi.createGameRequest(game, new CreateGameResponseHandler() {
                @Override
                public void onCreateGameResponse(CreateGameResponse response) {
                    if (response.getCreateGameStatus() == CreateGameStatus.SUCCESS) {
                        gameId = response.getGameId();
                        log("Create Game successful / game Id : " + gameId);
                        gameApi.joinGameRequest(gameId, new JoinGameResponseHandler() {
                            @Override
                            public void onJoinGameResponse(JoinGameResponse response) {
                                if (response.getJoinGameStatus() == JoinGameStatus.GAME_NOT_FOUND) {
                                    log("Game not found, provide a valid game id");
                                } else if (response.getJoinGameStatus() == JoinGameStatus.ACCEPTED) {
                                    log("Join Game successful");
                                    game = response.getGame();
                                    //game.setCustomData(buildGameState());
                                    //log("build game state: " + buildGameState());
                                    log("set Game Data: " + game.getCustomData());
                                    log("game.gameCharacteristics.customData: " + game.getGameCharacteristics().getCustomData());
                                    gameApi.saveGame(game);
                                } else {
                                    log("Failed to join Game ");
                                }
                            }
                        });
                    } else {
                        log("Failed to create Game ");
                    }
                }
            });
        }

        @Override
        protected void findGames() {
            log("Find games");
            NuggetaQuery nuggetaQuery = new NuggetaQuery();
            gameApi.getGamesRequest(nuggetaQuery, new GetGamesResponseHandler() {
                @Override
                public void onGetGamesResponse(GetGamesResponse getgamesresponse) {
                    List<NGame> games = getgamesresponse.getGames();
                    int gamesCount = games.size();
                    if (gamesCount > 0) {
                        log("Found " + gamesCount + " games.");
                    } else {
                        log("No game Found.");
                    }
                }
            });
        }

        @Override
        protected void joinGame() {
            log("joinGame");
            gameApi.joinGameRequest(gameId, new JoinGameResponseHandler() {
                @Override
                public void onJoinGameResponse(JoinGameResponse response) {
                    log("joinGameResponse");
                    if (response.getJoinGameStatus() == JoinGameStatus.GAME_NOT_FOUND) {
                        log("Game not found, provide a valid game id");
                    } else if (response.getJoinGameStatus() == JoinGameStatus.ACCEPTED) {
                        log("Join Game successful");
                        game = response.getGame();
                        gameId = game.getId();
                        gameApi.loadGame(gameId);
                        log("CustomData: " + game.getCustomData());
                        gameApi.saveGame(game);
                    } else {
                        log("Failed to join Game ");
                    }
                }
            });
        }

        @Override
        protected void sendGameMessage(String message) {
            log("sendGameMessage");
            NRawGameMessage rawGameMessage = new NRawGameMessage();
            rawGameMessage.setContent(message);
            gameApi.sendMessageToGame(game.getId(), rawGameMessage, false);
            //gameApi.save
        }

        @Override
        protected void sendPlayerMessage(String playerId, String message) {
            log("sendPlayerMessage");
            //game.setCustomData(buildGameState());
            gameApi.saveGame(game);
            NRawGameMessage rawMessage = new NRawGameMessage();
            rawMessage.setContent(message);
            gameApi.sendMessageToPlayer(playerId, rawMessage);
        }

        @Override
        protected void matchAndJoinGame() {
            gameApi.matchAndJoinGameRequest(null, null, 5000, new MatchAndJoinGameResponseHandler() {
                @Override
                public void onMatchAndJoinGameResponse(MatchAndJoinGameResponse matchAndJoinGameResponse) {
                    MatchAndJoinGameStatus matchAndJoinGameStatus = matchAndJoinGameResponse.getMatchAndJoinGameStatus();
                    if (matchAndJoinGameStatus == MatchAndJoinGameStatus.ACCEPTED) {
                        NGame game = matchAndJoinGameResponse.getGame();
                        log("You have joined the Game " + game.getId());
                        gameApi.loadGame(game.getId());
                        log("CustomData: " + game.getCustomData());
                    } else {
                        log("Failed to find a game " + matchAndJoinGameStatus.toString());
                        customMatchmaking();
                    }
                }
            });
        }

        @Override
        public void customMatchmaking() {
            game = new NGame();
            game.setName("My Game");
            NGameCharacteristics gameCharacteristics = new NGameCharacteristics();
            gameCharacteristics.setAutoStop(true);
            game.setGameCharacteristics(gameCharacteristics);
            log("Find games / Useful to build a games lobby.");
            NuggetaQuery nuggetaQuery = new NuggetaQuery();
            gameApi.getGamesRequest(nuggetaQuery, new GetGamesResponseHandler() {
                @Override
                public void onGetGamesResponse(GetGamesResponse getgamesresponse) {
                    List<NGame> games = getgamesresponse.getGames();
                    log("Found " + games.size() + " games.");
                    if (games.size() > 0) {
                        for (int i = 0; i < games.size(); i++) {
                            game = games.get(i);
                            gameId = game.getId();
                        }
                        log("Game id :  " + gameId);
                        joinGame();
                    } else {
                        log("No game Found. Hosting a game");
                    }
                }
            });
        }
    }
    public Goal getGoal() {
        ArrayList<Goal> goals = new ArrayList<Goal>();
        goals.add(new Goal("Walk 10 steps", 400));
        goals.add(new Goal("Walk 20 steps", 400));
        goals.add(new Goal("Walk 30 steps", 400));
        goals.add(new Goal("Walk 40 steps", 400));
        return goals.get(new Random().nextInt(goals.size() - 1));
    }

    @Override
    protected void onPause() {
        super.onPause();
        String FILENAME = "TEAM";
        String string = "";
        string += myTeam[0].getMonsterNumber();
        string += myTeam[0].getLevel();
        string += myTeam[0].getMove1().getName().length();
        string += myTeam[0].getMove1().getName();
        string += myTeam[0].getMove2().getName().length();
        string += myTeam[0].getMove2().getName();
        string += myTeam[0].getMove3().getName().length();
        string += myTeam[0].getMove3().getName();
        string += myTeam[0].getMove4().getName().length();
        string += myTeam[0].getMove4().getName();
        if (myTeam[1] != null) {
            string += myTeam[1].getMonsterNumber();
            string += myTeam[1].getLevel();
            string += myTeam[1].getMove1().getName().length();
            string += myTeam[1].getMove1().getName();
            string += myTeam[1].getMove2().getName().length();
            string += myTeam[1].getMove2().getName();
            string += myTeam[1].getMove3().getName().length();
            string += myTeam[1].getMove3().getName();
            string += myTeam[1].getMove4().getName().length();
            string += myTeam[1].getMove4().getName();
        }
        if (myTeam[2] != null) {
            string += myTeam[2].getMonsterNumber();
            string += myTeam[2].getLevel();
            string += myTeam[2].getMove1().getName().length();
            string += myTeam[2].getMove1().getName();
            string += myTeam[2].getMove2().getName().length();
            string += myTeam[2].getMove2().getName();
            string += myTeam[2].getMove3().getName().length();
            string += myTeam[2].getMove3().getName();
            string += myTeam[2].getMove4().getName().length();
            string += myTeam[2].getMove4().getName();
        }

        try {
            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(string.getBytes());
            fos.close();
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(stepCount != null) {
            stepCount.add(0, Integer.toString(Integer.parseInt(stepCount.get(0)) + Integer.parseInt(PedometerService.getNumSteps())));
        }else{
            stepCount = new ArrayList<String>();
            stepCount.add(PedometerService.getNumSteps());
        }
        stepsArray.storeFile(stepsFile, stepCount, this.getApplicationContext());
        moneyArray.storeFile(moneyfile,money,this.getApplicationContext());
        stopService(new Intent(this, PedometerService.class));
    }
}

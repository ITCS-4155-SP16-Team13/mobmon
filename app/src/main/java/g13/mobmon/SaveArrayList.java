package g13.mobmon;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jfon4 on 4/11/2016.
 */
public class SaveArrayList implements Serializable{

    private ArrayList<String> retrieved;

    public SaveArrayList(){
    }

    public void storeFile(String filename, ArrayList<String> toStore, Context context){
        try {
            FileOutputStream fos = context.openFileOutput(filename,Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(toStore);
            oos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> retrieveFile(String filename , Context context){
        try {
            FileInputStream fis = context.openFileInput(filename);
            ObjectInputStream oos = new ObjectInputStream(fis);
            retrieved = (ArrayList) oos.readObject();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return retrieved;
    }
}

package g13.mobmon;

/**
 * Created by jfon4 on 5/9/2016.
 */
public class Money {

    private int amount, currentSteps, currAmount;

    public Money(){
        amount= Integer.parseInt(PedometerService.getNumSteps());
        currentSteps = amount;
    }
    public int getCurrAmount() {
        updateAmount();
        return amount;
    }
    private void updateAmount(){
       if(Integer.parseInt(PedometerService.getNumSteps()) - currentSteps > 1){
           amount += Integer.parseInt(PedometerService.getNumSteps()) - currentSteps;
           currentSteps = Integer.parseInt(PedometerService.getNumSteps());
       }
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void spend(int amount){this.amount = this.amount - amount;}
    public void earn(int amount){this.amount = this.amount + amount;}
}
